// ===================================================================
//
// assimpparse.h
//     Header file for CParserOBJ class.
//
// Class: CParserOBJ
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, 2016.

#ifndef __ASSIMPPARSER_H__
#define __ASSIMPPARSER_H__

// nanoGOLEM headers
#include "configh.h"
#include "color.h"
#include "light.h"
#include "triangle.h"

__BEGIN_GOLEM_HEADER

// forward declarations
class CEnvironment;
class CScene;

// Format OBJ from WaveFront
class CParserASSIMP
{
private:
	// Possibly, add some point lights from a file
	void AddPointLights(const string &filename);

	/**
	* Parses and obj file using the ASSIMP. Can execute triangulation and other procedures after parsing.
	* Used when the binary file containing the object data is not present.
	* Creates the binary file after the parsing and eventual post-processing.
	*/
	bool ParseOBJ(const string &filename, CEnvironment &env, CScene &scene);

	/**
	* Parses the binary file with object data, does not use the ASSIMP. Does not execute any post-processing (e.g. triangulation).
	* Used when the binary file is present. If not, ParseOBJ method is used instead.
	*/
	bool ParseBinary(const string &filename, CEnvironment &env, CScene &scene);
public:
	CParserASSIMP() { }
	virtual ~CParserASSIMP() {}

	virtual bool Parse(const string &filename, CEnvironment &env, CScene &scene);
};

__END_GOLEM_HEADER

#endif // __ASSIMPPARSER_H__
