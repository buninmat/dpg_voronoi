// ============================================================================
//
// hminfo.cpp
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, January 2000

// nanoGOLEM headers
#include "configg.h"
#include "asds.h"
#include "hminfo.h"
#include "triangle.h"
#include "world.h"

__BEGIN_GOLEM_SOURCE

// copy constructor
CHitPointInfo::CHitPointInfo(const CHitPointInfo &info)
{
  // copy the values, no deep copy is needed for most of parameters
  memcpy((void*)this, (void*)&info, sizeof(CHitPointInfo));
}

// assignment operator
CHitPointInfo&
CHitPointInfo::operator=(const CHitPointInfo &info)
{
  if (this == &info)
    return *this;

  // copy the values, no deep copy is needed for most of parameters
  memcpy((void*)this, (void*)&info, sizeof(CHitPointInfo));

  return *this;
}

CVector3D
CHitPointInfo::GetNormal()
{
  if (!normalValid) {
    // compute the normal and the bump map if necessary
    assert(object);
    assert(ray);
    // casting to non-const
    object->FindNormal( *((CHitPointInfo*)this) );
  }

  return normal;
}

void
CHitPointInfo::GetDifferentials(CVector3D &u, CVector3D &v) const
{
  if (object) {
    object->GetUVvectors((CHitPointInfo&)(*this));
  }
  // copy the vectors
  u = diffU;
  v = diffV;

  return;
}

ostream&
operator<<(ostream &s, const CHitPointInfo &h)
{
  if (h.object) {
    s << "objectType = " << (int)(h.object->ID()) << endl;
    s << "pointer = " << h.object << endl;
  }
  else {
    s << "No object intersected" << endl;
  }
  return s;
}

__END_GOLEM_SOURCE

