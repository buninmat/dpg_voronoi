// ================================================================
//
// environ.cpp
//     Implementation of the environment operations, ie. reading
//     application environment file, reading command line parameters etc.
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran and Tomas Kopal, 1997

// standard headers 
#include <fstream>
#include <cstdio>

// nanoGOLEM headers
#include "configg.h"
#include "errorg.h"
#include "vector3.h"
#include "environ.h"

__BEGIN_GOLEM_SOURCE

// constructor
CEnvironment::CEnvironment()
{
  // this is maximal nuber of options in the constructor
#define MAX_OPTIONS   1200

  // allocate data for environment
  AllocateData(MAX_OPTIONS);

  // now it's time to register all used options

  // ---------------------------------------------------------------------
  // Misc parameters without the name space
  RegisterOption("InputFileName", optString, NULL);
  RegisterOption("OutputFileType", optString, "T", "hdr");
  RegisterOption("OutputFileName", optString, "O");
  // The initial setting of random generators -- seed -- is given here.
  RegisterOption("SeedValue", optInt, NULL, "123");

  // When running the "application" in the world, which stages to run
  RegisterOption("Run", optBool, "r", "ON");
  RegisterOption("Init", optBool, "i", "ON");
  RegisterOption("CleanUp", optBool, NULL, "ON");

  // ---------------------------------------------------------------------
  // Scene parameters
  RegisterOption("Scene.backgroundColor", optVector, NULL,
		 // sky blue is ("0.196078, 0.6, 0.8");
		 // black color is ("0.0, 0.0, 0.0"); - conforms to VRML'97
		 "0.0, 0.0, 0.0");
  RegisterOption("Scene.envFileName", optString, "m", "ENV/stpeters.hdr");
  RegisterOption("Scene.twoSidedPlanars", optBool, NULL, "ON");

  // ---------------------------------------------------------------------
  // Setting the image size in pixels and the borders to start at/finish to
  // whole image size that corresponds to the screen.
  RegisterOption("SetCamera.imageHeight", optInt, "H", "513");
  RegisterOption("SetCamera.imageWidth", optInt, "W", "513");
  // subimage definition
  RegisterOption("SetCamera.automaticView", optBool, NULL, "ON");
  RegisterOption("SetCamera.scanLine.start", optInt, "s", "0");
  RegisterOption("SetCamera.scanLine.end", optInt, "e");
  RegisterOption("SetCamera.scanColumn.start", optInt, "f", "0");
  RegisterOption("SetCamera.scanColumn.end", optInt, "l");
  RegisterOption("SetCamera.originLeftBottom", optBool, NULL, "ON");
  RegisterOption("SetCamera.samplesPerPixel", optInt, NULL, "1");
  RegisterOption("SetCamera.dotCounterMethod", optInt, NULL, "0");
  // CLimits class parameters
  RegisterOption("Limits.threshold", optFloat, NULL, "0.01");
  RegisterOption("Limits.small", optFloat, NULL, "1e-6");
  RegisterOption("Limits.infinity", optFloat, NULL, "1e6");

  // ---------------------------------------------------------------------
  // Application settings:

  // Which application should be run in the world  
  RegisterOption("Application.type", optString, "a", "RT1");
  // Ray-tracing parameters of general ray tracing algorithm
  RegisterOption("SetRayTracing.depthLimit", optInt, "q", "4");
  RegisterOption("SetRayTracing.cntSamples", optInt, "N", "1000");
  RegisterOption("SetRayTracing.envCntSamples", optInt, "N", "800000");
  RegisterOption("SetRayTracing.visModeVariable", optInt, NULL, "0");
  // The setting of parameters that use ray shooting as a basic tool
  RegisterOption("RTCore.dataStruct", optString, "t", "TDBVH");
  RegisterOption("RTCore.buildASDS", optBool, NULL, "ON");
  RegisterOption("RTCore.initImage", optBool, NULL, "ON");
  RegisterOption("RTCore.showBackfaceSurfaces", optBool, NULL, "ON");

  // ---------------------------------------------------------------------
  // BRDF settings:

  // Which application should be run in the world  
  RegisterOption("BRDF.type", optString, "b", "phong");
  RegisterOption("BRDF.Phong.kd", optFloat, NULL, "0.5");
  RegisterOption("BRDF.Phong.ks", optFloat, NULL, "0.5");
  RegisterOption("BRDF.Phong.ns", optFloat, NULL, "1.0");

  // ---------------------------------------------------------------------
  // Path(s) to be used when search the images on a file systems for reading
  // textures, etc.
  char normaldir[4] = { '.', DIRCAT, ';', 0x00 };
  RegisterOption("Image.filePath", optString, NULL, normaldir);
  // if to save the image into a file for image synthesis applications
  RegisterOption("Image.saveToFile", optBool, NULL, "ON");
  // The setting of saving High-Dynamic-Range images (without tonemapping)
  RegisterOption("Image.HDR.saveToFile", optBool, "B", "OFF");
  // The prefix put before the extension when saving HDR image
  RegisterOption("Image.HDR.suffix", optString, NULL, ".hdr");
  // The prefix put before the extension when saving HDR image
  RegisterOption("Image.HDR.fileType", optString, NULL, "hdr");
  // The image multiplier
  RegisterOption("Image.HDR.multiplier", optFloat, NULL, "1.0");

  return;
}

void
CEnvironment::AllocateData(int maxOptions)
{
  _maxOptions = maxOptions;
  optionalParams = NULL;
  paramRows = 0;
  numParams = 0;
  params = NULL;

  numOptions = 0;
  options = new char**[4];
  optTypes = new EOptType[_maxOptions];
  if (options == NULL || optTypes == NULL) {
    FATAL << "Memory allocation failed.\n";
    FATAL_EXIT;
  }
  options[0] = new char*[_maxOptions];
  options[1] = new char*[_maxOptions];
  options[2] = new char*[_maxOptions];
  options[3] = new char*[_maxOptions];
  if (options[0] == NULL || options[1] == NULL ||
      options[2] == NULL || options[3] == NULL) {
    FATAL << "Memory allocation failed.\n";
    FATAL_EXIT;
  }
  for (int i = 0; i < _maxOptions; i++) {
    options[0][i] = NULL;
    options[1][i] = NULL;
    options[2][i] = NULL;
    options[3][i] = NULL;
  }
}

// returns the number of non-optional params on the commandline
// .. static function
int
CEnvironment::GetParamNum(const int argc, const char *argv[])
{
  int numberOfParams = 0;

  for (int i = 1; i < argc; i++) {
    if (argv[i][0] != '-')
      numberOfParams++; // without - it should be scene filename 
  }
  return numberOfParams;
}

CEnvironment::~CEnvironment()
{
  int i, j;

  // delete the params structure
  for (i = 0; i < numParams; i++) {
    for (j = 0; j < paramRows; j++)
      if (params[i][j] != NULL) {
	delete[] params[i][j];
	params[i][j] = 0;
      }
    if (params[i] != NULL) {
      delete[] params[i];
      params[i] = 0;
    }
  }
  if (params != NULL) delete[] params;
  // delete the options structure
  for (i = 0; i < 4; i++) {
    for (j = 0; j < _maxOptions; j++)
      if (options[i][j] != NULL) delete[] options[i][j];
    if (options[i] != NULL) delete[] options[i];
  }
  if (options != NULL) delete[] options;
  if (optTypes != NULL)  delete[] optTypes;
  if (optionalParams != NULL)  delete[] optionalParams;
}

bool
CEnvironment::CheckForSwitch(const int argc, const char *argv[],
			     const char swtch) const
{
  for (int i = 1; i < argc; i++)
    if ((argv[i][0] == '-') && (argv[i][1] == swtch))
      return true;
  return false;
}

bool
CEnvironment::CheckForHelpSwitch(const int argc, const char *argv[],
				 string &str) const
{
  for (int i = 1; i < argc; i++)
    if ((argv[i][0] == '-') && (argv[i][1] == 'h')) {
      str.assign(&(argv[i][2]));
      return true; // help switch found
    }
  str.assign("");
  return false; // help switch not found
}

bool
CEnvironment::CheckType(const char *value, const EOptType type) const
{
  char *s, *t, *u;

  switch (type) {
  case optInt: {
    strtol(value, &t, 10);
    if (value + strlen(value) != t)
      return false;
    else
      return true;
  }
  case optFloat: {
    strtod(value, &t);
    if (value + strlen(value) != t)
      return false;
    else
      return true;
  }
  case optBool: {
    if (!strcmp(value, "true") ||
	!strcmp(value, "false") ||
	!strcmp(value, "YES") ||
	!strcmp(value, "NO") ||
	!strcmp(value, "+") ||
	!strcmp(value, "-") ||
	!strcmp(value, "ON") ||
	!strcmp(value, "OFF"))
      return true;
    return false;
  }
  case optVector: {
    strtod(value, &s);
    if (*s == ' ' || *s == '\t') {
      while (*s == ' ' || *s == '\t')
	s++;
      if (*s != ',')
	s--;
    }
    if ((*s != ',' && *s != ' ' && *s != '\t') || value == s)
      return false;
    t = s;
    strtod(s + 1, &u);
    if (*u == ' ' || *u == '\t') {
      while (*u == ' ' || *u == '\t')
	u++;
      if (*u != ',')
	u--;
    }
    if ((*u != ',' && *s != ' ' && *s != '\t') || t == u)
      return false;
    t = u;
    strtod(u + 1, &s);
    if (t == s || value + strlen(value) != s)
      return false;
    return true;
  }
  case optString: {
    return true;
  }
  default: {
    FATAL << "Internal error: Unknown type of option.\n" << flush;
    FATAL_ABORT;
  }
  }
  return false;
}

void
CEnvironment::ReadCmdlineParams(const int argc, const char *argv[],
				const char *optParams)
{
  int i;

  // Make sure we are called for the first time
  if (optionalParams != NULL)
    return;

  numParams = (int)(strlen(optParams) + 1);
  optionalParams = new char[numParams];
  strcpy(optionalParams, optParams);

  // First, count all non-optional parameters on the command line
  for (i = 1; i < argc; i++)
    if (argv[i][0] != '-')
      paramRows++;

  // allocate and initialize the table for parameters
  params = new char **[numParams];
  for (i = 0; i < numParams; i++) {
    params[i] = new char *[paramRows];
    for (int j = 0; j < paramRows; j++)
      params[i][j] = NULL;
  }
  // Now read all non-optional and optional parameters into the table
  curRow = -1;
  for (i = 1; i < argc; i++) {
    if (argv[i][0] != '-') {
      // non-optional parameter encountered
      curRow++;
      params[0][curRow] = new char[strlen(argv[i]) + 1];
      strcpy(params[0][curRow], argv[i]);
    }
    else {
      // option encountered
      char *t = strchr(optionalParams, argv[i][1]);
      if (t != NULL) {
	// this option is optional parameter
	int index = static_cast<int>(t - optionalParams + 1);
	if (curRow < 0) {
	  // it's a global parameter
	  for (int j = 0; j < paramRows; j++) {
	    params[index][j] = new char[strlen(argv[i] + 2) + 1];
	    strcpy(params[index][j], argv[i] + 2);
	  }
	}
	else {
	  // it's a scene parameter
	  if (params[index][curRow] != NULL) {
	    delete[] params[index][curRow];
	  }
	  params[index][curRow] = new char[strlen(argv[i] + 2) + 1];
	  strcpy(params[index][curRow], argv[i] + 2);
	}
      }
    }
  }
  curRow = 0;
}

bool
CEnvironment::GetParam(const char name, const int index, string &value) const
{
  int column;

  if (index >= paramRows || index < 0)
    return false;
  if (name == ' ')
    column = 0;
  else {
    char *t = strchr(optionalParams, name);
    if (t == NULL)
      return false;
    column = static_cast<int>(t - optionalParams + 1);
  }
  if (params[column][index] == NULL)
    return false;
  value.assign(params[column][index]);
  return true;
}

void
CEnvironment::RegisterOption(const char *name, const EOptType type,
			     const char *abbrev, const char *defValue)
{
  int i;

  // make sure this option was not yet registered
  for (i = 0; i < numOptions; i++)
    if (!strcmp(name, options[0][i])) {
      FATAL << " option " << name << " registered twice.\n";
      FATAL_ABORT;
    }
  // make sure we have enough room in memory
  if (numOptions >= _maxOptions) {
    FATAL << " too many options. Try enlarge the MAX_OPTIONS "
	  << "definition in file " << __FILE__ << ".\n";
    FATAL_EXIT;
  }
  // make sure the abbreviation doesn't start with 'D'.
  // 'D' is reserved for variables definitions.
  if (abbrev != NULL && (abbrev[0] == 'D')) {
    FATAL << "Internal error: reserved switch " << abbrev
	  << " used as an abbreviation.\n";
    FATAL_EXIT;
  }
  // new option
  options[0][numOptions] = new char[strlen(name) + 1];
  if (options[0][numOptions] == NULL) {
    FATAL << "Memory allocation failed.\n";
    FATAL_EXIT;
  }
  strcpy(options[0][numOptions], name);
  optTypes[numOptions] = type;
  // assign abbreviation, if requested
  if (abbrev != NULL) {
    options[2][numOptions] = new char[strlen(abbrev) + 1];
    if (options[2][numOptions] == NULL) {
      FATAL << "Memory allocation failed.\n";
      FATAL_EXIT;
    }
    strcpy(options[2][numOptions], abbrev);
  }
  // assign default value, if requested
  if (defValue != NULL) {
    options[3][numOptions] = new char[strlen(defValue) + 1];
    if (options[3][numOptions] == NULL) {
      FATAL << "Memory allocation failed.\n";
      FATAL_EXIT;
    }
    strcpy(options[3][numOptions], defValue);
    if (!CheckType(defValue, type)) {
      FATAL << "Internal error: Inconsistent type and default value in option "
	    << name << ".\n";
      FATAL_EXIT;
    }
  }
  // new option registered
  numOptions++;
}

bool
CEnvironment::OptionPresent(const char *name) const
{
  bool found = false;
  int i;

  for (i = 0; i < numOptions; i++)
    if (!strcmp(options[0][i], name)) {
      found = true;
      break;
    }
  if (!found) {
    FATAL << "Internal error: Option " << name << " not registered." << endl;
    FATAL_ABORT;
  }
  if (options[1][i] != NULL || options[3][i] != NULL)
    return true;
  else
    return false;
}

bool
CEnvironment::GetInt(const char *name, int &value,
		     const bool isFatal) const
{
  bool found = false;
  int i;

  // is this option registered ?
  for (i = 0; i < numOptions; i++)
    if (!strcmp(options[0][i], name)) {
      found = true;
      break;
    }
  if (!found) {
    // no registration found
    FATAL << "Internal error: Required option " << name
	  << " not registered.\n" << flush;
    FATAL_ABORT;
  }
  if ((options[1][i] == NULL) &&
      (options[3][i] == NULL)) {
    // this option was not initialised to some value
    if (isFatal) {
      FATAL << "Error: Required option " << name << " not found.\n" << flush;
      FATAL_EXIT;
    }
    else {
      FATAL << "Error: Required option " << name << " not found.\n" << flush;
      return false;
    }
  }

  if (options[1][i] == NULL) {
    // option was not read, so use the default
    value = strtol(options[3][i], NULL, 10);
  }
  else {
    // option was explicitly specified
    value = strtol(options[1][i], NULL, 10);
  }
  return true;
}

bool
CEnvironment::GetFloat(const char *name, double &value,
		       const bool isFatal) const
{
  bool found = false;
  int i;

  // is this option registered ?
  for (i = 0; i < numOptions; i++)
    if (!strcmp(options[0][i], name)) {
      found = true;
      break;
    }
  if (!found) {
    // no registration found
    FATAL << "Internal error: Required option " << name
	  << " not registered.\n" << flush;
    FATAL_EXIT;
  }
  if ((options[1][i] == NULL) &&
      (options[3][i] == NULL)) {
    // this option was not initialised to some value
    if (isFatal) {
      FATAL << "Error: Required option " << name << " not found.\n" << flush;
      FATAL_EXIT;
    }
    else {
      FATAL << "Error: Required option " << name << " not found.\n" << flush;
      return false;
    }
  }

  if (options[1][i] == NULL) {
    // option was not read, so use the default
    value = strtod(options[3][i], NULL);
  }
  else {
    // option was explicitly specified
    value = strtod(options[1][i], NULL);
  }
  return true;
}

bool
CEnvironment::GetBool(const char *name, bool &value,
		      const bool isFatal) const
{
  bool found = false;
  int i;

  // is this option registered ?
  for (i = 0; i < numOptions; i++)
    if (!strcmp(options[0][i], name)) {
      found = true;
      break;
    }
  if (!found) {
    // no registration found
    FATAL << "Internal error: Required option " << name
	  << " not registered.\n" << flush;
    FATAL_EXIT;
  }
  if ((options[1][i] == NULL) &&
      (options[3][i] == NULL)) {
    // this option was not initialised to some value
    if (isFatal) {
      FATAL << "Error: Required option " << name << " not found.\n" << flush;
      FATAL_EXIT;
    }
    else {
      FATAL << "Error: Required option " << name << " not found.\n" << flush;
      return false;
    }
  }

  value = false;
  if (options[1][i] == NULL) {
    // option was not read, so use the default
    if (!strcmp(options[3][i], "true") ||
	!strcmp(options[3][i], "YES") ||
	!strcmp(options[3][i], "+") ||
	!strcmp(options[3][i], "ON"))
      value = true;
  }
  else {
    // option was explicitly specified
    if (!strcmp(options[1][i], "true") ||
	!strcmp(options[1][i], "YES") ||
	!strcmp(options[1][i], "+") ||
	!strcmp(options[1][i], "ON"))
      value = true;
  }
  return true;
}

bool
CEnvironment::GetVector(const char *name, float &x, float &y, float &z,
			const bool isFatal) const
{
  bool found = false;
  int i;

  // is this option registered ?
  for (i = 0; i < numOptions; i++)
    if (!strcmp(options[0][i], name)) {
      found = true;
      break;
    }
  if (!found) {
    // no registration found
    FATAL << "Internal error: Required option " << name
	  << " not registered.\n" << flush;
    FATAL_EXIT;
  }
  if (options[1][i] == NULL && options[3][i] == NULL) {
    // this option was not initialised to some value
    if (isFatal) {
      FATAL << "Error: Required option " << name << " not found.\n" << flush;
      FATAL_EXIT;
    }
    else {
      FATAL << "Error: Required option " << name << " not found.\n" << flush;
      return false;
    }
  }

  if (options[1][i] == NULL) {
    // option was not read, so use the default
    char *s, *t;

    x = (float)strtod(options[3][i], &s);
    y = (float)strtod(s + 1, &t);
    z = (float)strtod(t + 1, NULL);
  }
  else {
    // option was explicitly specified
    char *s, *t;

    x = (float)strtod(options[1][i], &s);
    y = (float)strtod(s + 1, &t);
    z = (float)strtod(t + 1, NULL);
  }
  return true;
}

bool
CEnvironment::GetString(const char *name, string &value,
			const bool isFatal) const
{
  bool found = false;
  int i;

  // is this option registered ?
  for (i = 0; i < numOptions; i++)
    if (!strcmp(options[0][i], name)) {
      found = true;
      break;
    }
  if (!found) {
    // no registration found
    FATAL << "Internal error: Required option " << name
	  << " not registered.\n" << flush;
    FATAL_EXIT;
  }
  if (options[1][i] == NULL && options[3][i] == NULL) {
    // this option was not initialised to some value
    if (isFatal) {
      FATAL << "Error: Required option " << name << " not found.\n" << flush;
      FATAL_ABORT;
    }
    else {
      FATAL << "Error: Required option " << name << " not found.\n" << flush;
      return false;
    }
  }

  if (options[1][i] == NULL) {
    // option was not read, so use the default
    value.assign(options[3][i]);
  }
  else {
    // option was explicitly specified
    value.assign(options[1][i]);
  }
  return true;
}


void
CEnvironment::SetInt(const char *name, const int value)
{
  bool found = false;
  int i;

  // is this option registered ?
  for (i = 0; i < numOptions; i++)
    if (!strcmp(options[0][i], name)) {
      found = true;
      break;
    }
  if (!found) {
    // no registration found
    FATAL << "Internal error: Required option " << name
	  << " not registered.\n" << flush;
    FATAL_EXIT;
  }
  if (optTypes[i] == optInt) {
    delete[]options[1][i];
    options[1][i] = new char[16];
    sprintf(options[1][i], "%.15d", value);
  }
  else {
    FATAL << "Internal error: Trying to set non-integer option " << name
	  << " to integral value.\n" << flush;
    FATAL_EXIT;
  }
}

void
CEnvironment::SetFloat(const char *name, const float value)
{
  bool found = false;
  int i;

  // is this option registered ?
  for (i = 0; i < numOptions; i++)
    if (!strcmp(options[0][i], name)) {
      found = true;
      break;
    }
  if (!found) {
    // no registration found
    FATAL << "Internal error: Required option " << name
	  << " not registered.\n" << flush;
    FATAL_EXIT;
  }
  if (optTypes[i] == optFloat) {
    delete[]options[1][i];
    options[1][i] = new char[25];
    sprintf(options[1][i], "%.15e", value);
  }
  else {
    FATAL << "Internal error: Trying to set non-float option " << name
	  << " to float value.\n" << flush;
    FATAL_EXIT;
  }
}

void
CEnvironment::SetBool(const char *name, const bool value)
{
  bool found = false;
  int i;

  // is this option registered ?
  for (i = 0; i < numOptions; i++)
    if (!strcmp(options[0][i], name)) {
      found = true;
      break;
    }
  if (!found) {
    // no registration found
    FATAL << "Internal error: Required option " << name
	  << " not registered.\n" << flush;
    FATAL_EXIT;
  }
  if (optTypes[i] == optBool) {
    delete[]options[1][i];
    options[1][i] = new char[6];
    if (value)
      sprintf(options[1][i], "true");
    else
      sprintf(options[1][i], "false");
  }
  else {
    FATAL << "Internal error: Trying to set non-bool option " << name
	  << " to boolean value.\n" << flush;
    FATAL_EXIT;
  }
}

void
CEnvironment::SetVector(const char *name,
			const float x, const float y, const float z)
{
  bool found = false;
  int i;

  // is this option registered ?
  for (i = 0; i < numOptions; i++)
    if (!strcmp(options[0][i], name)) {
      found = true;
      break;
    }
  if (!found) {
    // no registration found
    FATAL << "Internal error: Required option " << name
	  << " not registered.\n" << flush;
    FATAL_EXIT;
  }
  if (optTypes[i] == optVector) {
    delete[]options[1][i];
    options[1][i] = new char[200];
    sprintf(options[1][i], "%.15e,%.15e,%.15e", x, y, z);
  }
  else {
    FATAL << "Internal error: Trying to set non-vector option " << name
	  << " to vector value.\n" << flush;
    FATAL_EXIT;
  }
}

void
CEnvironment::SetString(const char *name, const string &value)
{
  SetString(name, value.c_str());
}

void
CEnvironment::SetString(const char *name, const char *value)
{
  bool found = false;
  int i;

  // is this option registered ?
  for (i = 0; i < numOptions; i++)
    if (!strcmp(options[0][i], name)) {
      found = true;
      break;
    }
  if (!found) {
    // no registration found
    FATAL << "Internal error: Required option " << name
	  << " not registered.\n" << flush;
    FATAL_EXIT;
  }
  if (optTypes[i] == optString) {
    delete[]options[1][i];
    options[1][i] = new char[strlen(value) + 1];
    strcpy(options[1][i], value);
  }
  else {
    FATAL << "Internal error: Trying to set non-string option " << name
	  << " to string value.\n" << flush;
    FATAL_EXIT;
  }
}

void
CEnvironment::ParseCmdline(const int argc, const char *argv[], const int index)
{
  int curIndex = -1;

  for (int i = 1; i < argc; i++) {
    // if this parameter is non-optional, skip it and increment the counter
    if (argv[i][0] != '-') {
      curIndex++;
      continue;
    }
    // make sure to skip all non-optional parameters
    char *t = strchr(optionalParams, argv[i][1]);
    if (t != NULL)
      continue;

    // if we are in the scope of the current parameter, parse it
    if (curIndex == -1 || curIndex == index) {
      if (argv[i][1] == 'D') {
	// it's a full name definition
	bool found = false;
	int j;

	const char *t = strchr(argv[i] + 2, '=');
	if (t == NULL) {
	  FATAL << "Error: Missing '=' in option. "
		<< "Syntax is -D<name>=<value>.\n" << flush;
	  FATAL_EXIT;
	}
	for (j = 0; j < numOptions; j++)
	  if (!strncmp(options[0][j], argv[i] + 2, t - argv[i] - 2) &&
	      (unsigned)(t - argv[i] - 2) == strlen(options[0][j])) {
	    found = true;
	    break;
	  }
	if (!found) {
	  FATAL << "Error: Invalid option " << argv[i] << ".\n" << flush;
	  FATAL_EXIT;
	}
	if (!CheckType(t + 1, optTypes[j])) {
	  FATAL << "Error: invalid type of value " << t + 1 << " in option "
		<< options[0][j] << ".\n";
	  FATAL_EXIT;
	}
	if (options[1][j] != NULL)
	  delete[] options[1][j];
	options[1][j] = new char[strlen(t + 1) + 1];
	strcpy(options[1][j], t + 1);
      }
      else {
	// it's an abbreviation
	bool found = false;
	int j;

	for (j = 0; j < numOptions; j++)
	  if (options[2][j] != NULL &&
	      !strncmp(options[2][j], argv[i] + 1, strlen(options[2][j]))) {
	    found = true;
	    break;
	  }
	if (!found) {
	  FATAL << "Error: Invalid option " << argv[i] << ".\n" << flush;
	  FATAL_EXIT;
	}
	if (!CheckType(argv[i] + 1 + strlen(options[2][j]), optTypes[j])) {
	  FATAL << "Error: invalid type of value "
		<< argv[i] + 1 + strlen(options[2][j]) << "in option "
		<< options[0][j] << ".\n";
	  FATAL_EXIT;
	}
	if (options[1][j] != NULL)
	  delete[] options[1][j];
	options[1][j] = new char[strlen(argv[i] + 1 + strlen(options[2][j]))
				 + 1];
	strcpy(options[1][j], argv[i] + 1 + strlen(options[2][j]));
      }
    }
  }
#ifdef _DEBUG_PARAMS
  // write out the options table
  DEBUG << "Options table for " << numOptions << " options:\n";
  for (int j = 0; j < numOptions; j++) {
    for (int i = 0; i < 4; i++) {
      if (options[i][j] != NULL)
	DEBUGW << options[i][j];
      else
	DEBUGW << "NULL";
      DEBUGW << "\t";
    }
    DEBUGW << optTypes[j] << "\n";
  }
  DEBUGW << "Options done." << endl;
#endif // _DEBUG_PARAMS
}

void
CEnvironment::ClearEnvironment()
{
  // clears all non-default values
  for (int j = 0; j < MAX_OPTIONS; j++)
    if (options[1][j] != NULL) {
      delete[] options[1][j];
      options[1][j] = NULL;
    }
}

char *
CEnvironment::ParseString(char *buffer, char *string) const
{
  char *s = buffer;
  char *t = string + strlen(string);

  // skip leading whitespaces
  while (*s == ' ' || *s == '\t')
    s++;
  if (*s == '\0')
    return NULL;
  while ((*s >= 'a' && *s <= 'z') ||
	 (*s >= 'A' && *s <= 'Z') ||
	 (*s >= '0' && *s <= '9') ||
	 *s == '_')
    *t++ = *s++;
  *t = '\0';
  // skip trailing whitespaces
  while (*s == ' ' || *s == '\t')
    s++;
  return s;
}

bool
CEnvironment::ReadEnvFile(const char *envFilename)
{
  const int MaxStringLength = 256;
  char buff[MaxStringLength], name[MaxStringLength];
  char *s, *t;
  int i, line = 0;
  bool found;
  ifstream envStream(envFilename);

  // some error had occured
  if (envStream.fail()) {
    FATAL << " can't open file " << envFilename << " for reading (err. "
	  << envStream.rdstate() << ").\n";
    return false;
  }

  name[0] = '\0';

  // main loop
  for (;;) {
    // read in one line
    envStream.getline(buff, 255);
    if (!envStream) break;
    line++;
    // get rid of comments
    s = strchr(buff, '#');
    if (s != NULL)
      *s = '\0';

    // get one identifier
    s = ParseString(buff, name);
    // parse line
    while (s != NULL) {
      // it's a group name - make the full name
      if (*s == '{') {
	strcat(name, ".");
	s++;
	s = ParseString(s, name);
	continue;
      }
      // end of group
      if (*s == '}') {
	if (strlen(name) == 0) {
	  FATAL << " unpaired } in " << envFilename << " (line "
		<< line << ").\n";
	  envStream.close();
	  return false;
	}
	name[strlen(name) - 1] = '\0';
	t = strrchr(name, '.');
	if (t == NULL)
	  name[0] = '\0';
	else
	  *(t + 1) = '\0';
	s++;
	s = ParseString(s, name);
	continue;
      }
      // find variable name in the table
      found = false;
      for (i = 0; i < numOptions; i++) {
	if (!strcmp(name, options[0][i])) {
	  found = true;
	  break;
	}
      }
      if (!found) {
	FATAL << " unknown option " << name << " in environment file "
	      << envFilename << " (line " << line << ").\n";

	const char *l = strrchr(name, '.');
	int len = static_cast<int>((l == 0) ? strlen(name) : l - name);
	int found = 0;
	for (i = 0; i < numOptions; i++) {
	  if (strncmp(name, options[0][i], len) == 0) {
	    if (found == 0)
	      STATUS << "Similar registered options are:\n";
	    STATUS << options[0][i] << std::endl;
	    found++;
	  }
	}
	if (found == 0)
	  STATUS << "No similar registered options found.\n";

	envStream.close();
	return false;
      }
      switch (optTypes[i]) {
      case optInt: {
	//long int ret =
	strtol(s, &t, 10);
	if (t == s || (*t != ' ' && *t != '\t' &&
		       *t != '\0' && *t != '}')) {
	  FATAL << " mismatch in int variable " << name << " in "
		<< "environment file " << envFilename << " (line "
		<< line << ").\n";
	  envStream.close();
	  return false;
	}
	if (options[1][i] != NULL)
	  delete[] options[1][i];
	options[1][i] = new char[t - s + 1];
	strncpy(options[1][i], s, t - s);
	options[1][i][t - s] = '\0';
	s = t;
	break;
      }
      case optFloat: {
	strtod(s, &t);
	if (t == s || (*t != ' ' && *t != '\t' &&
		       *t != '\0' && *t != '}')) {
	  FATAL << " mismatch in float variable " << name << " in "
		<< "environment file " << envFilename << " (line "
		<< line << ").\n";
	  envStream.close();
	  return false;
	}
	if (options[1][i] != NULL)
	  delete[] options[1][i];
	options[1][i] = new char[t - s + 1];
	strncpy(options[1][i], s, t - s);
	options[1][i][t - s] = '\0';
	s = t;
	break;
      }
      case optBool: {
	t = s;
	while ((*t >= 'a' && *t <= 'z') ||
	       (*t >= 'A' && *t <= 'Z') ||
	       *t == '+' || *t == '-')
	  t++;
	if (((!strncmp(s, "true", t - s) && t - s == 4) ||
	     (!strncmp(s, "false", t - s) && t - s == 5) ||
	     (!strncmp(s, "YES", t - s) && t - s == 3) ||
	     (!strncmp(s, "NO", t - s) && t - s == 2) ||
	     (!strncmp(s, "ON", t - s) && t - s == 2) ||
	     (!strncmp(s, "OFF", t - s) && t - s == 3) ||
	     (t - s == 1 && (*s == '+' || *s == '-'))) &&
	    (*t == ' ' || *t == '\t' || *t == '\0' || *t == '}')) {
	  if (options[1][i] != NULL)
	    delete[] options[1][i];
	  options[1][i] = new char[t - s + 1];
	  strncpy(options[1][i], s, t - s);
	  options[1][i][t - s] = '\0';
	  s = t;
	}
	else {
	  FATAL << " mismatch in bool variable " << name << " in "
		<< "environment file " << envFilename << " (line "
		<< line << ").\n";
	  envStream.close();
	  return false;
	}
	break;
      }
      case optVector: {
	strtod(s, &t);
	if (*t == ' ' || *t == '\t') {
	  while (*t == ' ' || *t == '\t')
	    t++;
	  if (*t != ',')
	    t--;
	}
	if (t == s || (*t != ' ' && *t != '\t' && *t != ',')) {
	  FATAL << " mismatch in vector variable " << name << " in "
		<< "environment file " << envFilename << " (line "
		<< line << ").\n";
	  envStream.close();
	  return false;
	}
	char *u;
	strtod(t, &u);
	t = u;
	if (*t == ' ' || *t == '\t') {
	  while (*t == ' ' || *t == '\t')
	    t++;
	  if (*t != ',')
	    t--;
	}
	if (t == s || (*t != ' ' && *t != '\t' && *t != ',')) {
	  FATAL << " mismatch in vector variable " << name << " in "
		<< "environment file " << envFilename << " (line "
		<< line << ").\n";
	  envStream.close();
	  return false;
	}
	strtod(t, &u);
	t = u;
	if (t == s || (*t != ' ' && *t != '\t' &&
		       *t != '\0' && *t != '}')) {
	  FATAL << " mismatch in vector variable " << name << " in "
		<< "environment file " << envFilename << " (line "
		<< line << ").\n";
	  envStream.close();
	  return false;
	}
	if (options[1][i] != NULL)
	  delete[] options[1][i];
	options[1][i] = new char[t - s + 1];
	strncpy(options[1][i], s, t - s);
	options[1][i][t - s] = '\0';
	s = t;
	break;
      }
      case optString: {
	if (options[1][i] != NULL)
	  delete[] options[1][i];
	options[1][i] = new char[strlen(s) + 1];
	strcpy(options[1][i], s);
	s += strlen(s);
	break;
      }
      default: {
	FATAL << "Internal error: Unknown type of option.\n" << flush;
	FATAL_EXIT;
      }
      }
      // prepare the variable name for next pass
      t = strrchr(name, '.');
      if (t == NULL)
	name[0] = '\0';
      else
	*(t + 1) = '\0';
      // get next identifier
      s = ParseString(s, name);
    }
  }
  envStream.close();
  return true;
}

void
CEnvironment::PrintUsage(const char *hlpstring) const
{
  BANNER << "\nnanoGOLEM usage:\n";
  BANNER << "> golem [global-options] SceneFile [options] [SceneFile"
	 << "[options]] ...\n\n";
  BANNER << ", where options are as follows:\n";
  BANNER << "\t-h\t\tThis basic help screen\n";
  BANNER << "\t-h<+>\t\tHelpSetting: describe all the parameters possible.\n";

  // Other executive options
  BANNER << "\t-E<file>\tAlternative environment file\n";
  BANNER << "\t-O<file>\tAlternative output image file (-O disables output)\n";
  BANNER << "\t-T<type>\tAlternative output file type\n";
  BANNER << "\t-S<file>\tAppend statistics to given file\n";
  BANNER << "\t-R<file|->\tReference file name or reference forbidden\n";

  BANNER << "\t-a<file>\tThe application type(RT1, RT2)\n";
  BANNER << "\t-C<X>\t\tSelect X-th camera (default is 0)\n";
  BANNER << "\t-t<name>\tSelect type of main spatial data-structure\n";

  BANNER << "\t-W<width>\tSet width of the output image\n";
  BANNER << "\t-H<height>\tSet height of the output image\n";
  BANNER << "\t-s<start>\tStart at given scanline (range [0, height-1])\n";
  BANNER << "\t-e<end>\t\tEnd at given scanline (range [0, height])\n";
  BANNER << "\t-f<first>\tFirst column to be rendered (range [0, width-1])\n";
  BANNER << "\t-l<last>\tLast column to be rendered (range [0, width])\n";
  BANNER << "\t-w<+|->\t\tSave to file the whole frame defined (default on)\n";

  BANNER << "\n";

  BANNER << "\t-D<name>=<val>\tSet environment variable name 'name' to "
	 << "value 'val'.\n";
  BANNER << "\n";

  string helpstring(hlpstring);

  if (helpstring.compare("+") == 0) {
    // Print out all environment variable names
    BANNER << "Registered variable names for '-D' options are:\n";
    // delete the options structure
    for (int j = 0; j < MAX_OPTIONS; j++)
      if (options[0][j] != NULL)
	BANNER << "\t" << options[0][j] << "\n";
    BANNER << flush;
  }
}

__END_GOLEM_SOURCE

