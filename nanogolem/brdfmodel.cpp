// ===================================================================
//
// brdfmodel.cpp
//     BRDF model inlcuding Phong model
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, 1999.

// standard headers
#include <cstdio>

// nanoGOLEM headers
#include "configg.h"
#include "basmath.h"
#include "basstr.h"
#include "basrnd.h"
#include "coords3d.h"
#include "brdfmodel.h"

__BEGIN_GOLEM_SOURCE

// -------------------------------------------------------------------
// Implementation using Phong model - white color only

BRDF_Phong::BRDF_Phong(float rho_diffuse, float rho_specular, float spec_indexV)
{
  rho_diff = rho_diffuse;
  rho_spec = rho_specular;
  spec_index = spec_indexV;
}

void
BRDF_Phong::GetString(string &outstr) const
{
  char str[200];
  sprintf(str, "Phong rhod=%3.2f rhos=%3.2f si=%3.3f", rho_diff, rho_spec, spec_index);
  outstr.assign(str);
}

// returns cosine corrected value of BRDF given the illumination
// and viewer angle in RGB, returns false on success
bool
BRDF_Phong::GetBRDFValue(float /*u*/, float /*v*/, float theta_v,
			 float phi_v, float theta_i, float phi_i,
			 CColor &RGB) const
{
  // illumination is this way reflected different way
  // Phi-angles
  const float rp_i = (phi_v + float(M_PI));
  const float rp_v = phi_i;
  // Theta-angles
  const float rt_i = theta_v;
  const float rt_v = theta_i;

  // The vectors of illumination and view vector
  CVector3D refl_illum_vec, view_vec;
  ConvertThetaPhiToXYZ(rt_i, rp_i, refl_illum_vec); // ideally reflected ray
  ConvertThetaPhiToXYZ(rt_v, rp_v, view_vec); // viewing vector
  // cosine angle between reflected ray and viewing direction
  float cosangle = DotProd(refl_illum_vec, view_vec);
  if (cosangle < 0.f) cosangle = 0.f;
  float pexp = powf(cosangle, fabs(spec_index));
  float brdf = rho_diff/float(M_PI) + rho_spec * (spec_index + 2.f)/(2.f*float(M_PI)) * pexp;
  assert(!isnan(brdf));
  // the clamp of negative values due to the specular lobe
  if (brdf < 1e-25f)
    brdf = 1e-25f;
  // final value is cosine corrected for theta_i or theta_v ?
  RGB[0] = RGB[1] = RGB[2] = brdf * cos(rt_v);
  //RGB[0] = RGB[1] = RGB[2] = brdf * cos(rt_i);
  return false; // OK
}

bool 
BRDF_Phong::SpawnStochasticRay(float u, float v, // position in the texture
			       float rnd1, float rnd2, // input RND numbers
			       float theta_view, float phi_view, // the direction of a viewer
			       CVector3D &XYZ, // output direction
			       CColor &RGB, // corresponding color
			       float &prob) const // and probability
{
  u = u; v = v;
  // z-axis is the normal of a surface (0,0,1)
  float rho_total = rho_diff + rho_spec;
  // value to be used
  //  float value = 2.f*rho_diff+(spec_index+2.f)/(spec_index+1.f)*rho_spec;
  if (rnd1 < rho_diff/rho_total) {
    // sample diffuse par as rho_d*cos(theta)
    rnd1 = rnd1 / rho_diff * rho_total; // normalize the random variable to <0,1>
    assert(rnd1 <= 1.000001f);
    assert(0.f <= rnd1);
    float phi = 2.0f*float(M_PI)*rnd1; // tmp2
    float sinTheta = sqrt(1.0f - rnd2); // sin(theta)
    XYZ[0] = cos(phi) * sinTheta;
    XYZ[1] = sin(phi) * sinTheta;
    XYZ[2] = sqrt(rnd2); // cos(theta)
    // prob given by PDF is cos(theta)/M_PI, see tech report and GI compendium
    prob = XYZ[2] / float(M_PI); 
    prob = rho_diff/rho_total * prob;
    //prob = rho_total/rho_diff * prob;
    RGB[0] = RGB[1] = RGB[2] = rho_diff * XYZ[2] / float(M_PI) / prob; // so it is constant
    // Note - multiplication by rho_total above is necessary, as in fact
    // we compute cos(theta)*rho_d/M_PI *(rho_spec+rho_d)/rho_d
    // The results were werified numerically
    return false; // OK
  }

  // sample specular lobe in local coordinate system x,y,z  
  if (rho_spec < 1e-10f) {
    RGB[0] = RGB[1] = RGB[2] = 0.0f;
    prob = float(M_PI * M_PI); // prob given by PDF of diffuse component
    return true; // no ray does exist here
  }
  // alpha ... angle between perfect mirror reflection and the outgoing direction
  rnd1 = (rnd1*rho_total - rho_diff)/rho_spec; // normalize the random variable to <0,1>
  assert(rnd1 >= 0.f);
  assert(rnd1 <= 1.00001f);
  // compute the angle alpha, it is enough its cos(alpha) and sin(alpha)
  float cosAlpha = powf(rnd1, 1.0f/(spec_index+1.0f)); // our sampling distribution
  assert(cosAlpha <= 1.0f);
  assert(cosAlpha >= 0.f);
  float sinAlpha = sqrt(1.0f - cosAlpha*cosAlpha);
  float XYZL[3]; // the result in local coordinate system given by reflected ray
  float phi = 2.0f*float(M_PI)*rnd2;
  // we compute the direction in coordinate system (1,0,0)x(0,1,0)x(0,0,1)
  XYZL[0] = cos(phi) * sinAlpha;
  XYZL[1] = sin(phi) * sinAlpha;
  XYZL[2] = cosAlpha;
  // we transform now the vector to the coordinate system given
  // by ideally reflected ray R,U,V
  CVector3D R, U, V;
  ConvertThetaPhiToXYZ(theta_view, phi_view + float(M_PI), R);
  U = ArbitraryNormal(R); // generate normal to R
  V = CrossProd(R,U);// and the V to create the coordinate system R,U,V
  
  // transform XYZL in the coordinate system R,U,V to X,Y,Z coordinate system
  // first compute Z-value
  XYZ[2] = XYZL[0] * U[2] + XYZL[1] * V[2] + XYZL[2] * R[2];
  // now we check, if the samples goes below the surface
  if ((XYZ[2] <= 1e-10f)||(cosAlpha<1e-10)) {
    // generated direction is below the surface specified by normal
    RGB[0] = RGB[1] = RGB[2] = 0.0f; // black colour
    XYZ[0] = 0; XYZ[0] = 1; XYZ[2] = 0;
    prob = XYZ[2] / float(M_PI);
    return true; // no ray can be shot for given two random values
  }
  // Then compute two other coordinates X an Y
  XYZ[0] = XYZL[0] * U[0] + XYZL[1] * V[0] + XYZL[2] * R[0];
  XYZ[1] = XYZL[0] * U[1] + XYZL[1] * V[1] + XYZL[2] * R[1];
  assert(cosAlpha > 0.f);
  // Let us evaluate the probability of sampling of the specular lobe
  float cosAlphaPoweredFactor = pow(cosAlpha, spec_index);
  // This formula contains cosine(theta_illum) in the result by XYZ[2]
  // In fact it is reevaluating BRDF for the lighing and viewer direction
  assert(XYZL[2] >= 0.f);
  float brdf = (spec_index + 2.f) * powf(XYZL[2], spec_index)/(2.f*float(M_PI)) * rho_spec;
  prob = (spec_index + 1.f) * cosAlphaPoweredFactor / float(2.0f * M_PI);
  prob = rho_spec/rho_total * prob;
  //prob = rho_total/rho_spec * prob;
  RGB[0] = RGB[1] = RGB[2] = brdf * XYZ[2] / prob; // return brdf
  assert(prob > 0.f);
  // Note - multiplication by rho_total above is necessary, as in fact we
  // compute cos(theta)*rho_spec*(spec_index+2)/2/M_PI *cos^spec_index(alpha)*(rho_spec+rho_d)/rho_d
  // The results were werified numerically on various ranges of rho_spec, rho_diff, spec_index
  // Vlastimil Havran, 2013/09/10
  return false; // OK
}

// returns probability of sampling in a given viewer direction
// The RGB value has to be provided from fomer call of function GetBRDFValue.
// It shall not be used in combination of importanceSampling - it returns different
// value - since importance sampling is probabilistic and here we compute some average.
// It shall be used only if we sample from another distribution and aske what it is
// probabibility of chosing this direction, such as in MIS.
float
BRDF_Phong::GetProbOfSampling(float /*u*/, float /*v*/,
			      float theta_v, float phi_v,
			      float theta_i, float phi_i,
			      const CColor &/*RGB*/) const
{
  // Compute BRDF given the directions
  // Phi-angles
  // illumination is this way reflected different way
  const float rp_i = (phi_v + float(M_PI));
  const float rp_v = phi_i;
  // Theta-angles
  const float rt_i = theta_v;
  const float rt_v = theta_i;

  // The vectors of illumination and view vector
  CVector3D refl_illum_vec, view_vec;
  ConvertThetaPhiToXYZ(rt_i, rp_i, refl_illum_vec); // ideally reflected ray
  ConvertThetaPhiToXYZ(rt_v, rp_v, view_vec); // viewing vector
  // cosine angle between reflected ray and viewing direction
  float cosangle = DotProd(refl_illum_vec, view_vec);
  if (cosangle < 0.f) cosangle = 0.f;
  float pexp = powf(cosangle, fabs(spec_index));
  float brdf = rho_diff/float(M_PI) + rho_spec * (spec_index + 2.f)/(2.f*float(M_PI)) * pexp;
  assert(!isnan(brdf));
  // the clamp of negative values due to the specular lobe
  if (brdf < 1e-25f)
    brdf = 1e-25f;
  return brdf * cos(rt_v)/(rho_diff + rho_spec);
}

float
BRDF_Phong::GetAlbedo(float /*u*/, float /*v*/, // texture position
		      // viewer direction as two angles in radians
		      float /*theta_view*/, float /*phi_view*/) const
{ 
  // analytic formula for albedo
  return rho_diff + rho_spec;
}

__END_GOLEM_SOURCE

