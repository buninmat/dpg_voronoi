// ================================================================
//
//  The design of these streams definition was motivated when
//    reading PovRay manual.
//
// Class: CMsgStreams
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, February 1997

// nanoGOLEM headers
#include "configg.h"
#include "errorg.h"

__BEGIN_GOLEM_SOURCE

// The static variables of class CMsgStreams for reporting messages
ostream*
CMsgStreams::sbanner = NULL;
ostream*
CMsgStreams::sdebug = NULL;
ostream*
CMsgStreams::sfatal = NULL;
ostream*
CMsgStreams::sappopts = NULL;
ostream*
CMsgStreams::sstats = NULL;
ostream*
CMsgStreams::srefstats = NULL;
ostream*
CMsgStreams::sstatus = NULL;
ostream*
CMsgStreams::swarning = NULL;

// -------------------------------------------------------------------------
// the definition of streams, must be run at the main() as first command
void
CMsgStreams::Init()
{
  sbanner = &cout;
  sdebug = &cerr;
  sfatal = &cerr;
  sappopts = &cout;
  sstats = &cout;
  srefstats = &cout;
  sstatus = &cout;
  swarning = &cerr;
}

// the redirection of concrete stream - which stream is one of the static
// stream above, where is address of cout, cerr or any ofstream
void
CMsgStreams::Redirect(CMsgStreams::EType whichStream, ostream* where)
{
  ostream *rwhere = NULL;

  if (where == NULL) {
    // completely disregard the output of the stream - redirect to null
    // or something else like that !!!
  }
  else {
    rwhere = where;
  }

  switch (whichStream) {
    case EE_BANNER: {
      if (sbanner)
	*sbanner << flush;
      sbanner = rwhere;
      break;
    }
    case EE_DEBUG: {
      if (sdebug)
	*sdebug << flush;
      sdebug = rwhere;
      break;
    }
    case EE_FATAL: {
      if (sfatal)
	*sfatal << flush;
      sfatal = rwhere;
      break;
    }
    case EE_APPOPTS: {
      if (sappopts)
	*sappopts << flush;
      sappopts = rwhere;
      break;
    }
    case EE_STATS: {
      if (sstats)
	*sstats << flush;
      sstats = rwhere;
      break;
    }
    case EE_REFSTATS: {
      if (srefstats)
	*srefstats << flush;
      srefstats = rwhere;
      break;
    }
    case EE_STATUS: {
      if (sstats)
	*sstats << flush;
      sstatus = rwhere;
      break;
    }
    case EE_WARNING: {
      if (swarning)
	*swarning << flush;
      swarning = rwhere;
      break;
    }
  } // switch

  *rwhere << flush;
}

// -------------------------------------------------------------------------- 
// This is the class for debugging purposes of strange errors

// the suspicious address 
void*
CDebug::addr = NULL;

// if the address was set
bool
CDebug::wasSet = false;

// the function to set the suspicious addres
void
CDebug::SetAddr(void *newAddr)
{
  addr = newAddr;
  wasSet = true;
}

// the function to be called to check the address content - the error can be
// found by binary search in the source files. Its content depend on the
// situation.
void
CDebug::DoCheckAddr(char *file, int line)
{
  if (wasSet) {
    float value = *((float*)addr);
    cerr << " doCheck in file:" << file << " line:" << line
	 << " value is " << value << endl;
  }
}


void
CDebug::DoCheckAddr()
{
  if (wasSet) {
    float value = *((float*)addr);
    cerr << " DoingCheck. "
	 << " Value is " << value << endl;
  }
}

__END_GOLEM_SOURCE

