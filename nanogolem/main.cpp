// ===================================================================
//
// main.cpp
//
//     The main program, which implements rendering algorithm in nanoGOLEM
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, 2000

// standard headers
#include <fstream>
#ifdef __UNIX__
#include <sys/time.h>
#include <sys/resource.h>
#endif // __UNIX__
#include <iostream>
#include <cmath>

// nanoGOLEM headers
#include "configg.h"
#include "appcore.h"
#include "basmath.h"
#include "basmacr.h"
#include "basstr.h"
#include "errorg.h"
#include "hdrimg.h"
#include "scene.h"
#include "world.h"

__BEGIN_GOLEM_SOURCE

// ----------------------------------------
// GLOBAL variables
// global world pointers defined
CWorld *world = NULL;

int
RunApplication(int argc, const char *argv[])
{
  // the names of file
  string sceneFilename;
  string envFilename;

  // Scene to process exists, create the world
  world = new CWorld(argc, argv);
  int numberOfScenes;
  
  // No scene file names - error
  if ( (numberOfScenes = CEnvironment::GetParamNum(argc, argv)) <= 0) {
    FATAL << " no scene filename specified. Try 'golem -h or golem -h+' for help.\n"
	  << flush;
    exit(3);
  }

  // Process all scene files
  for (int sceneNum = 0; sceneNum < numberOfScenes; sceneNum++) {
    // the name of the whole scene that can contain several scene names
    string wholeSceneName("");
    
    // Get the scene file name
    if (!world->_environment.GetParam(' ', sceneNum, sceneFilename)) {
      FATAL << "Internal error: Invalid scene number.\n" << flush;
      delete world;
      world = 0;
      return 1;
    }    

    wholeSceneName.assign(sceneFilename);
    
    string envFN;
    // Get the environment file name for scene being processed
    if (!world->_environment.GetParam('E', sceneNum, envFN)) {
      // user didn't specified environment file explicitly, so
      // it's name will be derived from scene file name
      string extension("env");
      string sceneFN(sceneFilename);
      ChangeFilenameExtension(sceneFN, extension, envFilename);
    }
    else {
      envFilename = envFN;
    }

    // Now it's time to read in environment file.
    if (!world->_environment.ReadEnvFile(envFilename.c_str())) {
      // error - bad input file name specified ?
      world->CleanUp();
      continue;
    }

    // Parse the command line first for given scene number.
    // options given on the command line subsume
    // stuff specified in the input environment file.
    world->_environment.ParseCmdline(argc, argv, sceneNum);
    
    // now create the application
    if( world->CreateApplication() )
      return 5; // an error occured

    // and set the preferences of the application before parsing etc.
    world->_application->SetPreferences(world->_environment);
    
    // set some attributes to the world before parse
    CScene *scene = new CScene();
    // set the scene
    world->SetScene(scene);
    
    // flag if the scene was read into the memory
    bool sceneRead = false;
    // now assign the statistics
    
    // Parse the scene, returns true on error
    sceneRead = world->ParseScene(sceneFilename);

    if (sceneRead) { // some error occured when parsing
      WARNING <<"Error: scene " << sceneFilename << " was not successfuly "
	      << "parsed." << endl;
      world->CleanUp();
      continue; // scene was not read into the memory
    }

    // Now read the environment file again, since some parameters can
    // overrride the setting made when scene was read. Environment setting
    // is thus always preferred over the setting read from the scene
    if (!world->_environment.ReadEnvFile(envFilename.c_str())) {
      // error - bad input file name specified ?
      world->CleanUp();
      continue;
    }
    
    // Parse the command line  again; options given on the command line
    // subsume stuff specified in the input environment file and in
    // the scene file.
    world->_environment.ParseCmdline(argc, argv, sceneNum);

    // set the input scene name that can be combined from more names
    world->_environment.SetString("InputFileName", wholeSceneName.c_str());
    
    // initialiazes anything in the world before runing any application inside
    // the world.
    if (world->Init(sceneFilename))
      return 10; // some error occured

    // makes world into the run, anything can happen in the world
    // from now on !!!
    if (world->Run())
      return 20; // some error occured

    // Prepare all for further scene
    world->CleanUp();
  } // for all scenes

  // This will end the last universe created :-)
  delete world;
  world = 0;

  // OK
  return 0;
}

// --------------------------------------------------------------------------
//   main()
// --------------------------------------------------------------------------
int
main(const int argc, const char *argv[])
{  
  // init the stream - where the report should be passed
  CMsgStreams::Init();  
  BANNER << "You are running " << argv[0] << endl;
  BANNER << "nanoGOLEM Ray Tracer, compiled on " << __DATE__ << "\n";
  BANNER << "Can be used only for DPG subject at Czech Technical University\n";
  BANNER <<  "  Contact person: Vlastimil Havran - havran@fel.cvut.cz\n" << flush;
  BANNER << "Running with arguments" << endl;
  for (int i = 0; i < argc; i++) {
    BANNER << argv[i] << " ";
  }
  BANNER << endl;
  // run the application over the input arguments
  int res = RunApplication(argc, argv);

  BANNER << "nanoGOLEM finished" << endl;
  // Success or failure
  return res;
}

__END_GOLEM_SOURCE

