// ===================================================================
//
// sbbox.cpp
//     Simply bounding box for ray tracing only, 24 Bytes in size.
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlasta Havran, December 2005

// nanoGOLEM headers
#include "configg.h"
#include "basstr.h"
#include "sbbox.h"

__BEGIN_GOLEM_SOURCE

// Overload << operator for C++-style output
ostream&
operator<< (ostream &s, const SBBox &A)
{
  return s << '[' << A.pp[0].x << ", " << A.pp[0].y << ", " << A.pp[0].z << "]["
	   << A.pp[1].x << ", " << A.pp[1].y << ", " << A.pp[1].z << ']';
}

// Overload >> operator for C++-style input
istream&
operator>> (istream &s, SBBox &A)
{
  char a;
  // read "[min.x, min.y, min.z][max.x, max.y, max.z]"
  return s >> a >> A.pp[0].x >> a >> A.pp[0].y >> a >> A.pp[0].z >> a >> a
	   >> A.pp[1].x >> a >> A.pp[1].y >> a >> A.pp[1].z >> a;
}

void SBBox::operator=(const SBBox & box) {
  pp[0].x = box.pp[0].x;
  pp[0].y = box.pp[0].y;
  pp[0].z = box.pp[0].z;
  pp[1].x = box.pp[1].x;
  pp[1].y = box.pp[1].y;
  pp[1].z = box.pp[1].z;
}

void
Describe(const SBBox &b, ostream &app, int ind)
{
  Indent(app, ind);
  app << "SBBox: min at(" << b.Min() << "), max at(" << b.Max() << ")\n";
}

// Function describing the box
void
DescribeXYZ(const SBBox &b, ostream &app, int ind)
{
  Indent(app, ind);
  app << " box = ( "
      << b.Min().x << " , " << b.Max().x << " )( "
      << b.Min().y << " , " << b.Max().y << " )( "
      << b.Min().z << " , " << b.Max().z << " )\n";
}


bool
SBBox::Includes(const SBBox &b) const
{
  if (b.pp[0].x >= pp[0].x &&
      b.pp[0].y >= pp[0].y &&
      b.pp[0].z >= pp[0].z &&
      b.pp[1].x <= pp[1].x &&
      b.pp[1].y <= pp[1].y &&
      b.pp[1].z <= pp[1].z)
    return true;
  return false;
}

bool
SBBox::Includes(const SBBox &b, float eps) const
{
  if  (b.pp[0].x >= pp[0].x - eps &&
       b.pp[0].y >= pp[0].y - eps &&
       b.pp[0].z >= pp[0].z - eps &&
       b.pp[1].x <= pp[1].x + eps &&
       b.pp[1].y <= pp[1].y + eps &&
       b.pp[1].z <= pp[1].z + eps)
    return true;
  return false;  
}
 
__END_GOLEM_SOURCE

