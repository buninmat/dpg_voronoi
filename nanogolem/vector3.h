// ===================================================================
//
// vector3.h
//     Header file for CVector3D class in nanoGOLEM.
//
// Class: CVector3D
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, 2000

#ifndef __VECTOR3_H__
#define __VECTOR3_H__

// nanoGOLEM headers
#include "configh.h"
#include "basmath.h"

//standard headers
#include <cmath>

__BEGIN_GOLEM_HEADER

// Forward-declare some other classes.
class CMatrix4;
class CVector2D;

class CVector3D
{
public:
  float x, y, z;

  // for compatibility with pascal's code
  void  setX(float q) { x=q; }
  void  setY(float q) { y=q; }
  void  setZ(float q) { z=q; }
  float getX() const { return x; }
  float getY() const { return y; }
  float getZ() const { return z; }

  // constructors
  CVector3D() { }

  CVector3D(float X, float Y, float Z) { x = X; y = Y; z = Z; }
  CVector3D(float X) { x = y = z = X; }
  CVector3D(const CVector3D &v) { x = v.x; y = v.y; z = v.z; }

  // Functions to get at the vector components
  float& operator[] (int inx) {
    return (&x)[inx];
    
  }

  operator const float*() const { return (const float*) this; }

  const float& operator[] (int inx) const {
    return *(&x+inx);
  }

  void ExtractVerts(float *px, float *py, int which) const;
  void ExtractVerts(CVector2D &v, int which) const;

  void Set(const float &a, const float &b, const float &c)
  {  x=a; y=b; z=c; }

  void Set(const float a) { x = y = z = a; }
  
  // returns the axis, where the vector has the largest value
  int DrivingAxis(void) const;

  // returns the axis, where the vector has the smallest value
  int TinyAxis(void) const;

  inline float MaxComponent(void) const {
     // return (x > y && x > z) ? x : ((y > z) ? y : z);
     return (x > y) ? ( (x > z) ? x : z) : ( (y > z) ? y : z);
   }
 
   inline CVector3D Abs(void) const {
     return CVector3D(fabs(x), fabs(y), fabs(z));
   }

  // normalizes the vector of unit size corresponding to given vector
  inline void Normalize();
  
  /**
    ===> Using ArbitraryNormal() for constructing coord systems 
    ===> is obsoleted by RightHandedBase() method (<JK> 12/20/03).   

     Return an arbitrary normal to `v'.
     In fact it tries v x (0,0,1) an if the result is too small,
     it definitely does v x (0,1,0). It will always work for
     non-degenareted vector and is much faster than to use
     TangentVectors.

     @param v(in) The vector we want to find normal for.
     @return The normal vector to v.
  */
  friend inline CVector3D ArbitraryNormal(const CVector3D &v); 

  /**
    Find a right handed coordinate system with (*this) being
    the z-axis. For a right-handed system, U x V = (*this) holds.
    This implementation is here to avoid inconsistence and confusion
    when construction coordinate systems using ArbitraryNormal():
    In fact:
      V = ArbitraryNormal(N);
      U = CrossProd(V,N);
    constructs a right-handed coordinate system as well, BUT:
    1) bugs can be introduced if one mistakenly constructs a 
       left handed sytems e.g. by doing
       U = ArbitraryNormal(N);
       V = CrossProd(U,N);
    2) this implementation gives non-negative base vectors
       for (*this)==(0,0,1) |  (0,1,0) | (1,0,0), which is
       good for debugging and is not the case with the implementation
       using ArbitraryNormal()

    ===> Using ArbitraryNormal() for constructing coord systems 
	     is obsoleted by this method (<JK> 12/20/03).   
  */
  void RightHandedBase(CVector3D& U, CVector3D& V) const;

  /// Transforms a vector to the global coordinate frame.
  /**
    Given a local coordinate frame (U,V,N) (i.e. U,V,N are 
    the x,y,z axes of the local coordinate system) and
    a vector 'loc' in the local coordiante system, this
    function returns a the coordinates of the same vector
    in global frame (i.e. frame (1,0,0), (0,1,0), (0,0,1).
  */
  friend inline CVector3D ToGlobalFrame(const CVector3D& loc,
	  const CVector3D& U,
	  const CVector3D& V,
	  const CVector3D& N);
  
  /// Transforms a vector to a local coordinate frame.
  /**
    Given a local coordinate frame (U,V,N) (i.e. U,V,N are 
    the x,y,z axes of the local coordinate system) and
    a vector 'loc' in the global coordiante system, this
    function returns a the coordinates of the same vector
    in the local frame.
  */
  friend inline CVector3D ToLocalFrame(const CVector3D& loc,
	  const CVector3D& U,
	  const CVector3D& V,
	  const CVector3D& N);

  /// the magnitude=size of the vector
  friend inline float Magnitude(const CVector3D &v);
  /// the squared magnitude of the vector .. for efficiency in some cases
  friend inline float SqrMagnitude(const CVector3D &v);
  /// Magnitude(v1-v2)
  friend inline float Distance(const CVector3D &v1, const CVector3D &v2);
  /// SqrMagnitude(v1-v2)
  friend inline float SqrDistance(const CVector3D &v1, const CVector3D &v2);

  // creates the vector of unit size corresponding to given vector
  friend inline CVector3D Normalize(const CVector3D &A);

  // Rotate a normal vector.
  friend CVector3D PlaneRotate(const CMatrix4 &, const CVector3D &);

  // construct view vectors .. DirAt is the main viewing direction
  // Viewer is the coordinates of viewer location, UpL is the vector.
  friend void ViewVectors(const CVector3D &DirAt, const CVector3D &Viewer,
	                  const CVector3D &UpL, CVector3D &ViewV,
			  CVector3D &ViewU, CVector3D &ViewN );

  // Given the intersection point `P', you have available normal `N'
  // of unit length. Let us suppose the incoming ray has direction `D'.
  // Then we can construct such two vectors `U' and `V' that
  // `U',`N', and `D' are coplanar, and `V' is perpendicular
  // to the vectors `N','D', and `V'. Then 'N', 'U', and 'V' create
  // the orthonormal base in space R3.
  friend void TangentVectors(CVector3D &U, CVector3D &V, // output
			     const CVector3D &normal, // input
			     const CVector3D &dirIncoming);
  // Unary operators
  CVector3D operator+ () const;
  CVector3D operator- () const;

  // Assignment operators
  CVector3D& operator+= (const CVector3D &A);
  CVector3D& operator-= (const CVector3D &A);
  CVector3D& operator*= (const CVector3D &A);
  CVector3D& operator*= (float A);
  CVector3D& operator/= (float A);

  // Binary operators
  friend inline CVector3D operator+ (const CVector3D &A, const CVector3D &B);
  friend inline CVector3D operator- (const CVector3D &A, const CVector3D &B);
  friend inline CVector3D operator* (const CVector3D &A, const CVector3D &B);
  friend inline CVector3D operator* (const CVector3D &A, float B);
  friend inline CVector3D operator* (float A, const CVector3D &B);
  friend CVector3D operator* (const CMatrix4 &, const CVector3D &);
  friend inline CVector3D operator/ (const CVector3D &A, const CVector3D &B);

  friend inline int operator< (const CVector3D &A, const CVector3D &B);
  friend inline int operator<= (const CVector3D &A, const CVector3D &B);

  friend inline CVector3D operator/ (const CVector3D &A, float B);
  friend inline int operator== (const CVector3D &A, const CVector3D &B);
  friend inline float DotProd(const CVector3D &A, const CVector3D &B);
  friend inline CVector3D CrossProd (const CVector3D &A, const CVector3D &B);

  friend ostream& operator<< (ostream &s, const CVector3D &A);
  friend istream& operator>> (istream &s, CVector3D &A);
    
  friend void Minimize(CVector3D &min, const CVector3D &candidate);
  friend void Maximize(CVector3D &max, const CVector3D &candidate);

  friend inline int EpsilonEqualV3(const CVector3D &v1, const CVector3D &v2, float thr);
  friend inline int EpsilonEqualV3(const CVector3D &v1, const CVector3D &v2);
};

inline CVector3D
ArbitraryNormal(const CVector3D &N)
{
  float dist2 = N.x * N.x + N.y * N.y;
  if (dist2 > 0.0001) {
    float inv_size = 1.0f/sqrt(dist2);
    return CVector3D(N.y * inv_size, -N.x * inv_size, 0); // N x (0,0,1)
  }
  float inv_size = 1.0f/sqrt(N.z * N.z + N.x * N.x);
  return CVector3D(-N.z * inv_size, 0, N.x * inv_size); // N x (0,1,0)
}


inline CVector3D
ToGlobalFrame(const CVector3D &loc,
	      const CVector3D &U,
	      const CVector3D &V,
	      const CVector3D &N)
{
  return loc.x * U + loc.y * V + loc.z * N;
}

inline CVector3D
ToLocalFrame(const CVector3D &loc,
	     const CVector3D &U,
	     const CVector3D &V,
	     const CVector3D &N)
{
  return CVector3D( loc.x * U.x + loc.y * U.y + loc.z * U.z,
		    loc.x * V.x + loc.y * V.y + loc.z * V.z,
		    loc.x * N.x + loc.y * N.y + loc.z * N.z);
}

inline float
Magnitude(const CVector3D &v) 
{
  return sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
}

inline float
SqrMagnitude(const CVector3D &v) 
{
  return v.x * v.x + v.y * v.y + v.z * v.z;
}

inline float
Distance(const CVector3D &v1, const CVector3D &v2)
{
  return sqrt(sqr(v1.x - v2.x) + sqr(v1.y - v2.y) + sqr(v1.z - v2.z));
}

inline float
SqrDistance(const CVector3D &v1, const CVector3D &v2)
{
  return sqr(v1.x-v2.x)+sqr(v1.y-v2.y)+sqr(v1.z-v2.z);
}

inline CVector3D
Normalize(const CVector3D &A)
{
  return A * (1.0f/Magnitude(A));
}

inline float
DotProd(const CVector3D &A, const CVector3D &B)
{
  return A.x * B.x + A.y * B.y + A.z * B.z;
}

inline CVector3D
CVector3D::operator+() const
{
  return *this;
}

inline CVector3D
CVector3D::operator-() const
{
  return CVector3D(-x, -y, -z);
}

inline CVector3D&
CVector3D::operator+=(const CVector3D &A)
{
  x += A.x;  y += A.y;  z += A.z;
  return *this;
}

inline CVector3D&
CVector3D::operator-=(const CVector3D &A)
{
  x -= A.x;  y -= A.y;  z -= A.z;
  return *this;
}

inline CVector3D&
CVector3D::operator*= (float A)
{
  x *= A;  y *= A;  z *= A;
  return *this;
}

inline CVector3D&
CVector3D::operator/=(float A)
{
  float a = 1.0f/A;
  x *= a;  y *= a;  z *= a;
  return *this;
}

inline CVector3D&
CVector3D::operator*= (const CVector3D &A)
{
  x *= A.x;  y *= A.y;  z *= A.z;
  return *this;
}

inline CVector3D
operator+ (const CVector3D &A, const CVector3D &B)
{
  return CVector3D(A.x + B.x, A.y + B.y, A.z + B.z);
}

inline CVector3D
operator- (const CVector3D &A, const CVector3D &B)
{
  return CVector3D(A.x - B.x, A.y - B.y, A.z - B.z);
}

inline CVector3D
operator* (const CVector3D &A, const CVector3D &B)
{
  return CVector3D(A.x * B.x, A.y * B.y, A.z * B.z);
}

inline CVector3D
operator* (const CVector3D &A, float B)
{
  return CVector3D(A.x * B, A.y * B, A.z * B);
}

inline CVector3D
operator* (float A, const CVector3D &B)
{
  return CVector3D(B.x * A, B.y * A, B.z * A);
}

inline CVector3D
operator/ (const CVector3D &A, const CVector3D &B)
{
  return CVector3D(A.x / B.x, A.y / B.y, A.z / B.z);
}

inline CVector3D
operator/ (const CVector3D &A, float B)
{
  float b = 1.0f / B;
  return CVector3D(A.x * b, A.y * b, A.z * b);
}

inline int
operator< (const CVector3D &A, const CVector3D &B)
{
  return A.x < B.x && A.y < B.y && A.z < B.z;
}

inline int
operator<= (const CVector3D &A, const CVector3D &B)
{
  return A.x <= B.x && A.y <= B.y && A.z <= B.z;
}

// Might replace floating-point == with comparisons of
// magnitudes, if needed.
inline int operator== (const CVector3D &A, const CVector3D &B)
{
  return (A.x == B.x) && (A.y == B.y) && (A.z == B.z);
}

inline CVector3D
CrossProd (const CVector3D &A, const CVector3D &B)
{
  return CVector3D(A.y * B.z - A.z * B.y,
		   A.z * B.x - A.x * B.z,
		   A.x * B.y - A.y * B.x);
}

inline void
CVector3D::Normalize()
{
  float sqrmag = x * x + y * y + z * z;
  if (sqrmag > 0.0)
    (*this) *= 1.0f / sqrt(sqrmag);
}

// Overload << operator for C++-style output
inline ostream&
operator<< (ostream &s, const CVector3D &A)
{
  return s << "(" << A.x << ", " << A.y << ", " << A.z << ")";
}

// Overload >> operator for C++-style input
inline istream&
operator>> (istream &s, CVector3D &A)
{
  char a;
  // read "(x, y, z)"
  return s >> a >> A.x >> a >> A.y >> a >> A.z >> a;
}

inline int
EpsilonEqualV3(const CVector3D &v1, const CVector3D &v2, float thr)
{
  if ( fabs(v1.x-v2.x) > thr )
    return false;
  if ( fabs(v1.y-v2.y) > thr )
    return false;
  if ( fabs(v1.z-v2.z) > thr )
    return false;
  return true;
}

inline int
EpsilonEqualV3(const CVector3D &v1, const CVector3D &v2)
{
  return EpsilonEqualV3(v1, v2, CLimits::Small);
}

__END_GOLEM_HEADER

#endif // __VECTOR3_H__

