// ===================================================================
//
// vector2.h
//     Header file for CVector2D class - implements 2-dimensional vector
//
// Class: CVector2D
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran

#ifndef __VECTOR2_H__
#define __VECTOR2_H__

// standard headers
#include <iostream>
#include <ostream>

// nanoGOLEM headers
#include "configh.h"
// #include "basmacr.h"
#include "basmath.h"

__BEGIN_GOLEM_HEADER

// ---------------------------------------------------------------------------
// This class describes two-dimensional vector.
// ---------------------------------------------------------------------------
class CVector2D
{
public:
  float xx, yy; // coordinates of the vector

  // constructors
  CVector2D(float xs, float ys):xx(xs), yy(ys) {}
  // default constructor
  CVector2D() {} 
  
  friend std::ostream& operator<< (std::ostream &s, const CVector2D &A);
  friend std::istream& operator>> (std::istream &s, CVector2D &A);

  const   float&   x() const { return xx; }
  const   float&   y() const { return yy; }
  float&   x() {return xx;}
  float&   y() {return yy;}
  
  // Functions to get at the vector components
  float& operator[] (int inx) {
    if (inx == 0) return xx;
    return yy;
  }

  const float& operator[] (int inx) const {
    if (inx == 0) return xx;
    return yy;
  }

  // returns x-coordinate for which==0, x-coordinate for which==1
  void     ExtractVerts(float *p, int which) const;

  float&   SetX(const float x) {return xx = x;}
  float&   SetY(const float y) {return yy = y;}

  CVector2D& operator=(CVector2D const &v) {
    xx = v.x(); yy = v.y(); return *this;
  }

  void   Set(float xs, float ys ) { xx = xs; yy = ys; }

  float  Size() const { return (float) sqrt(xx*xx + yy*yy);}

  // returns the squared magnitude of a vector
  friend inline float SqrMagnitude(const CVector2D &v);

  // returns the squared distance between two vectors
  friend inline float SqrDistance(const CVector2D &v1, const CVector2D &v2);
  
  // normalize the vector to the size=1.0
  float    Normalize();
  
  // Assignment operators
  CVector2D& operator+= (const CVector2D &a);
  CVector2D& operator-= (const CVector2D &a);
  CVector2D& operator*= (const CVector2D &a);
  CVector2D& operator*= (float a);
  CVector2D& operator/= (float a);

  // Binary operators
  friend inline CVector2D operator+ (const CVector2D &A, const CVector2D &B);
  friend inline CVector2D operator- (const CVector2D &A, const CVector2D &B);
  friend inline CVector2D operator* (float A, const CVector2D &B);
  friend inline CVector2D operator* (const CVector2D &A, float B);

  //  CVector2D operator*(const Matrix3C &mat) const;
  CVector2D operator-() const { return CVector2D(-x(),-y()); }

  int  operator==(const CVector2D &u) const {return Equal(u,CLimits::Small);}
  int  operator!=(const CVector2D &u) const {return !operator == (u);}

  int  Equal(const CVector2D &u,float trash) const;

  // dot product of the two vectors
  friend inline double DotProd(const CVector2D &u,
			       const CVector2D &v) {
    return ( u.x() * v.x()  +  u.y() * v.y() );
  }

  // the angle between two vectors $\in <0, PI>$
  float  Angle(const CVector2D &v) const;
  // cosine of the angle between the two vectors $\in <-1,1>$
  float  Cosine(const CVector2D &v) const;
  // supposes this vector is normalized
  float  CosineN(const CVector2D &v) const;

  // checks if the this vector is not opposite to a given vector
  int    IsOpposite(const CVector2D &v) const {
    return (Abs(DotProd(*this,v) + Size() * v.Size()) < CLimits::Small);
  }

  // computes the distance between this and a given vector
  float  Distance(const CVector2D &v) const { 
    return  (float) sqrt( sqr(xx-v.x()) + sqr(yy - v.y()));
  }

  // computes the squared distance between this and a given vector 
  float  SqrDistance(const CVector2D &v) const { 
    return  sqr(x()-v.x())+sqr(y()-v.y());
  }

  // if a given vector has a smaller(larger) coordinate, then this is updated
  CVector2D&  UpdateMin(const CVector2D &v);
  CVector2D&  UpdateMax(const CVector2D &v);

  // checks if both coordinates are smaller than a given one
  int operator<=(CVector2D &v) {
    return x() < (v.x() + CLimits::Small)
      && y() < (v.y()+CLimits::Small);
  }

  // checks if both coordinates are larger than a given one
  int operator>=(CVector2D &v) {
    return x() > (v.x()-CLimits::Small) &&
      y() > (v.y()-CLimits::Small);
  }

  // returns which coordinates has larger size
  int DominantAxis();
};

const CVector2D ZeroVector2(0,0);

inline CVector2D&
CVector2D::operator+= (const CVector2D &a)
{
  xx += a.xx;  yy += a.yy;
  return *this;
}

inline CVector2D&
CVector2D::operator-= (const CVector2D &a)
{
  xx -= a.xx;  yy -= a.yy;
  return *this;
}

inline CVector2D&
CVector2D::operator*= (float a)
{
  xx *= a;  yy *= a;
  return *this;
}

inline CVector2D&
CVector2D::operator/= (float a)
{
  xx /= a;  yy /= a;
  return *this;
}

inline CVector2D&
CVector2D::operator*= (const CVector2D &a)
{
  xx *= a.xx;  yy *= a.yy;
  return *this;
}

inline CVector2D
operator+ (const CVector2D &A, const CVector2D &B)
{
  return CVector2D(A.xx + B.xx, A.yy + B.yy);
}

inline CVector2D
operator- (const CVector2D &A, const CVector2D &B)
{
  return CVector2D(A.xx - B.xx, A.yy - B.yy);
}

inline CVector2D
operator* (const CVector2D &A, float B)
{
  return CVector2D(A.xx * B, A.yy * B);
}

inline CVector2D
operator* (float A, const CVector2D &B)
{
  return CVector2D(B.xx * A, B.yy * A);
}

inline float
SqrMagnitude(const CVector2D &v)
{
  return v.x() * v.x() + v.y() * v.y();
}

inline float
SqrDistance(const CVector2D &v1, const CVector2D &v2)
{
  return sqr(v1.xx - v2.xx) + sqr(v1.yy - v2.yy);
}

// Overload << operator for C++-style output
inline ostream&
operator<< (ostream &s, const CVector2D &A)
{
  return s << "(" << A.xx << ", " << A.yy << ")";
}

// Overload >> operator for C++-style input
inline istream&
operator>> (istream &s, CVector2D &A)
{
  char a;
  // read "(x, y, z)"
  return s >> a >> A.xx >> a >> A.yy >> a;
}

__END_GOLEM_HEADER

#endif // __VECTOR2_H__

