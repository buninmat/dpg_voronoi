// ===================================================================
//
// light.h
//     Header file for Light class
//
// Class: CLight
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, 2000

#ifndef __LIGHT_H__
#define __LIGHT_H__

// Standard C++ headers
#include <vector>

// nanoGOLEM headers
#include "configh.h"
#include "color.h"
#include "hminfo.h"
#include "sbbox.h"
#include "triangle.h"
#include "vector3.h"

// Forward declarations
class CRay;

// Abstract class
class CLight {
public:  
  int lightID;
  CVector3D pos;
};

// A single point light
class CPointLight:
  public CLight
{
public:
  CColor col;
  
  CPointLight() {}
  CPointLight(const CVector3D &nPos, const CColor &nCol):CLight() {
    pos = nPos; col = nCol;
  }
};


// Just a container of lights
class CLightList :
  public vector<CLight *>
{
public:
  CLightList() { }
};

#endif //  __LIGHT_H__

