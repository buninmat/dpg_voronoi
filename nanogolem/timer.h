// ================================================================
//
// timer.h
//     CTimer class to measure the elapsed time
//
// Class: CTimer
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, 1998.

#ifndef __TIMER_H__
#define __TIMER_H__

// standard headers
#include <iostream>
#ifdef __UNIX__
#include <sys/resource.h>
#include <sched.h>
#endif
#include <ctime>
#include <stdint.h> /* for uint64_t */
#include <time.h>  /* for struct timespec */

// Class CTimer is designed to measure the user/(user+system)/real time
// taken by the processor within a portion of source code.
class CTimer
{
private: // data
  /// true if InitTiming() has already been called
  static bool initTimingCalled;

  /// accumulated real time (returned by RealTime() method)
  mutable double realTime;
  /// real time at the last start event
  mutable double lastRealTime;

  /// the number of stopping this timer
  mutable int  countStop; 
  /// if the timer is running at this time
  mutable bool running;   

private: // methods

  /// interior implementation of Stop()
  void _start() const;
  // interior implementation of Start()
  void _stop() const;

  /// finds out whether performance timer is present (WIN32)
  void _initTiming();

public: // methods

  /// default constructor 
  CTimer();
  /// default destructor
  ~CTimer() {}
  /// reset the time to zero all time values
  void Reset();
  /// restart the timer from zero
  void Restart() { Reset(); Start();}
  /// start/restart the timer to measure the following events
  void Start() { _start(); }
  /// stop the timer not to measure the following events
  void Stop()  { _stop(); }
  /// returns true if timer is running, otherwise false
  bool IsRunning() const { return running;}
  /// returns the real time measured by timer in seconds
  double RealTime() const;
};

#endif // __TIMER_H__

