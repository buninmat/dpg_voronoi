// ===================================================================
//
// rt04app.h
//
//     Header file for the project - BRDF sampling + EM sampling
//     Multiple Importance Sampling alpha=0.50 as proposed by Veach and Guibas, 1995
//
// Class: CRT_04_App
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, 2015.

#ifndef __RT04APP_H__
#define __RT04APP_H__

// Standard C++ headers
#include <fstream>

// nanoGOLEM headers
#include "configh.h"
#include "rt02app.h"

__BEGIN_GOLEM_HEADER

// forward declarations
class CRT_04_App:
  public CRT_02_App
{
protected:
  // function to return the color for primary rays to be passed to
  // frame sampling scheme .. it is used by CColorMapper in the camera.
  virtual void ComputeColor(const CRay &ray, CHitPointInfo &info,
			    CColor &color);
  int evalEMcntST, evalBRDFcntST;
  void ResetCountersST() { evalEMcntST = evalBRDFcntST = 0; }
  void SetCountersST(int BRDFcnt, int EMcnt) {
    evalBRDFcntST = BRDFcnt;
    evalEMcntST = EMcnt;
  }
  void ResetStatistics() {
    statsSamplingBRDFtrack.Reset();
    statsSamplingEMtrack.Reset();
  }
  // Entry to store result for MIS evaluation later on
  struct SEntry {
    CColor RGB;
    // probability of sampling the RGB value above from first technique
    double probThis;
    // probability of sampling the RGB value above from second technique
    double probOther;
    // the technique indicator .. = 0 - first technique .. = 1 - second technique, -1 unused
    int    technique; 
    float  cosTheta; // cosine(theta) for this sample with respect to normal
    void   SetZero() { RGB = 0; probThis = probOther = 0; technique = -1;}
  };

  SEntry *rbrdf; // the records about sampling from BRDF
  SEntry *rem; // the records about sampling from EM
  int     raCnt; // the count of arrays
  void    AllocateArraysMIS();

  float EvaluateMISweight(float prob1, int cntSamples1, float prob2, int cntSamples2) {
    // This is formulation 4.2 and 4.3 of MIS in technical report
    const float eps = 1e-25f;
    float alpha1 = (float)cntSamples1/(cntSamples1 + cntSamples2);
    float alpha2 = 1.0f - alpha1;
    return 1.0f/(alpha1*prob1 + alpha2*prob2 + eps);
    // This requires to divide the result by 1.0/(cntSamples1 + cntSamples2) at the end
  }
  void ComputeMISpart(SEntry *ra, int cntSamplesFirst, int cntSamplesSecond,
		      int offset, int cntSamplesForSum,
		      CColor &resRGB, SVAR &variance, SVAR &varianceTotal);

  bool AddSampleEM(CVector3D &Tint, CVector3D &Nint, CVector3D &Bint,
		   float theta_v, float phi_v, float u, float v,
		   // input .. required number of samples to be taken at this stage
		   int newSamplesEMcnt,
		   int &cntAllSamplesEM, // update number of samples really taken
		   int &cntValidSamplesEM, // and those really taken
		   int cntAllSamplesBRDF, // the the number of EM samples taken so far
		   int newSamplesBRDFcnt);
  bool AddSampleBRDF(CVector3D &Tint, CVector3D &Nint, CVector3D &Bint,
		     float theta_v, float phi_v, float u, float v,
		     // input .. required number of samples to be taken at this stage
		     int newSamplesBRDFcnt,
		     int &cntAllSamplesBRDF, // update number of samples really taken
		     int &cntValidSamplesBRDF, // and those really taken
		     int cntAllSamplesEM, // the the number of EM samples taken so far
		     int newSamplesEMcnt);
  virtual void EvaluateColor(CVector3D &Tint, CVector3D &Nint, CVector3D &Bint,
			     float theta_v, float phi_v, float u, float v,
			     CColor &result); // output
public:
  // constructor
  CRT_04_App(): CRT_02_App(),rbrdf(0),rem(0) { }
  // destructor
  virtual ~CRT_04_App() { }
  virtual bool Init(CWorld *world, string &outputName);
  
  // deletes all the auxiliary data structures
  virtual bool CleanUp() {
    CRT_02_App::CleanUp();
    delete []rbrdf; rbrdf = 0;
    delete []rem; rem = 0;
    return false;
  }
};

__END_GOLEM_HEADER

#endif // __RT04APP_H__

