// ===================================================================
//
// asds.cpp
//      auxiliary spatial data structure source code
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, 1998

// standard headers
#include <cstring>

// nanoGOLEM headers
#include "configg.h"
#include "asds.h"
#include "hminfo.h"
#include "tdbvh.h"
#include "world.h"

__BEGIN_GOLEM_SOURCE

// ------------------------------------------------------------------------
// CASDS_Base
// ------------------------------------------------------------------------

// do any initialization that is required before traversing the ASDS,
// when this ASDS is used via function FindNearest()
void
CASDS_Base::TraversalReset()
{
  // do nothing for base class
}

const CObject3D*
CASDS_Base::FindNearest(CRay &ray, CHitPointInfo &info)
{
  TraversalReset();
  
  // call the interior function related to the data structure
  FindNearestI(ray, info);
  
  return (const CObject3D*)info.GetObject3D();
}

// ----------------------------------------------------
// CASDS class
// -----------------------------------------------------

CASDS::CASDS():
  objlist(NULL), builtUp(false), tInt(0.0), statsID(0)
{
}

CASDS::~CASDS() 
{
  Remove();
}

// Given the list of ASDSs, computes the enclosing bounding box for all ASDSs
void
CASDS::ComputeBoxForListOfASDS(SBBox &boxToInclude,
			       const TASDSList &asdslist)
{
  boxToInclude.Initialize();
  TASDSList &asdsl = (TASDSList&)asdslist;
  
  for (TASDSList::iterator it = asdsl.begin(); it != asdsl.end(); it++) {
    SBBox sbox = (*it)->BBox();
    boxToInclude.Include(sbox);
  } // for

  return;
}

// This function should be used on the object list to start from the offset
void
CASDS::ReassignUniqueIDs(CObjectList &objList)
{
  // reassign uniqe ID for all objects .. starting from 1
  // 0 is reserved for no intersection
  int objsID = 1; // pointer in the array
  
  for (CObjectList::iterator oi = objList.begin();
       oi != objList.end(); oi++) {
    // setting unique ID for every object instance
    (*oi)->SetUniqueID(objsID);
    objsID++;
  } // for
  CObject3D::SetCountID(objsID);
  
  return;
}

void
CASDS::Remove(void)
{
  if (objlist) {
    delete objlist;
    objlist = NULL;
  }
  builtUp = false;
}

// creates required type of ASDS for ray tracing
CASDS*
CASDS::Allocate(const CASDS_Base::EASDS_ID &dstruct)
{
  CASDS  *asds = NULL;
  
  switch (dstruct) {
    case CASDS_Base::ID_NaiveRSA:      asds = new CNaiveRSA; break;
    case CASDS_Base::ID_BVHTopDown:    asds = new CTDBVH; break;
    default: {
      FATAL << " No ASDS was defined !!\n" ;
      return NULL;
    }
  }

  return asds; // OK
}

//-----------------------------------------------------------------------
// implementation of CAObjectList class

// builds up auxiliary data structures for ray-intersection calculation
void
CAObjectList::BuildUp(const CObjectList &objl)
{
  if (objlist)
    delete objlist;
  
  objlist = new CObjectList(objl);
  assert(objlist);
  
  // the extent of the space taken by the objects is computed
  InitializeBox(bbox, *objlist);

  builtUp = true; 
}

// builds up auxiliary data structures for ray-intersection calculation
// and sets the bounding box to a specific instance
void
CAObjectList::BuildUp(const CObjectList &objl, const SBBox &nBox)
{
  if (objlist)
    delete objlist;

  objlist = new CObjectList(objl);
  assert(objlist);

  bbox = nBox;
  builtUp = true; 
}

const CObject3D*
CAObjectList::FindNearestI(CRay &ray, CHitPointInfo &info)
{
  // as default no object is intersected
  const CObject3D *retObject = NULL;

  // info.maxt must not be changed at all
  float saveMaxT = info.maxt;
  // some constant to avoid numerical imprecisions
  info.maxt += CLimits::Threshold;
  
  // iterate the whole list and find out the nearest intersection
  for (CObjectList::iterator sc = objlist->begin(); sc != objlist->end();
       sc++) {
    // DEBUG << "OBJL" << (*sc)->GetUniqueID() << endl;

    if ( (*sc)->NearestInt(ray, info)) {
      info.maxt = info.t;
      retObject = *sc;
    } // if
  } // for

  // recover the found minimum signed distance, if any object was intersected
  info.t = info.maxt;
  // recover maximum signed distance
  info.maxt = saveMaxT;  

  return (CObject3D*)(info.object = retObject);
}

// Removing involves really nothing 
void
CAObjectList::Remove(void)
{
  // DEBUG << "CAObjectList::Remove\n";

  CASDS::Remove();
}

CAObjectList::~CAObjectList()
{
  Remove();
  // DEBUG << "CAObjectList::~AObjectList\n";
}

//-----------------------------------------------------------------------
// implementation of CNaiveRSA class

// builds up auxiliary data structures for ray-intersection calculation
void
CNaiveRSA::BuildUp(const CObjectList &objl)
{
  if (objlist)
    delete objlist;
  
  objlist = new CObjectList(objl);
  assert(objlist);

  // the list of the unbounded objects
  CObjectList unboundeds;

  // the extent of the space taken by the objects is computed
  InitializeBox(bbox, *objlist);

  builtUp = true; 
}

// builds up auxiliary data structures for ray-intersection calculation
// and sets the bounding box to a specific instance
void
CNaiveRSA::BuildUp(const CObjectList &objl, const SBBox &nBox)
{
  if (objlist)
    delete objlist;

  objlist = new CObjectList(objl);
  assert(objlist);

  // the list of the unbounded objects
  CObjectList unboundeds;

  bbox = nBox;
  builtUp = true; 
}

__END_GOLEM_SOURCE

