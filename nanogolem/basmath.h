// ================================================================
//
// basmath.h
//     Basic mathematical definitions
//
// Class: CLimits
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, 2000.

#ifndef __BASMATH_H__
#define __BASMATH_H__

// Standard headers
#include <cassert>
#include <cmath>
#ifdef __UNIX__
//include <values.h>
#endif // __UNIX__

// nanoGOLEM headers
#include "configh.h"

__BEGIN_GOLEM_HEADER

// forward declarations

//-----------------------------------------------------------------------------
// Miscellaneous helper macros
//-----------------------------------------------------------------------------

// The definition of some constants
#ifndef M_PI
#define M_PI        3.14159265358979323846
#endif
#ifndef M_PI_2
#define M_PI_2	    1.57079632679489661923
#endif
#ifndef M_PI_4
#define M_PI_4	    0.78539816339744830962
#endif
#ifndef M_1_PI
#define M_1_PI	    0.31830988618379067154
#endif
#ifndef M_2_PI
#define M_2_PI	    0.63661977236758134308
#endif

#ifndef MAXFLOAT
#define MAXFLOAT    ((float)3.40282347e+38)
#endif

// -------------------------------------------------------------------
/// Limits.
/**
  This class encapsulates all the concessions to floating-point
      error made by ray tracers.
  */
class CLimits
{
public:
  /// The number used to reject too-close intersections.
  static float Threshold;

  /// This is a "small" number.  
  /** 
    Less than this number is assumed to be 0, when such things matter.
    The default value is 0.1.
  */
  static float Small;

  /// This is an impractically "large" number.
  /**
    Used for intersection parameters out to infinity (e.g. the span resulting from
    an FindAllIntersections operation on a plane).
    The default value is 100000.
  */
  static float Infinity;
};

// -------------------------------------------------------------------
// Basic Math Functions

/// Returns square of the variable
template<class T>
T sqr(T a)
{
  return a * a;
}

inline float
sqr(float a)
{
  return a * a;
}

/// Returns signum of the variable
inline int
signum(float a, float epsilon = CLimits::Small) 
{
  if (a > epsilon)
    return 1; 
  else
    if (a < -epsilon)
      return -1; 
  return 0;
}

// N-th power of base in integer
inline int
intintpow(int base, int exponent) {
  int res = 1;
  for(int i=0;i<exponent;i++)
    res *= base;
  return res;
}

/// Returns absolute value of a given type for floating point values
template<class T>
T
Abs(const T &x)
{
  return (x < 0) ? -x : x; 
}

/// Swaps two values.
template<class T>
inline void 
Swap(T &x, T &y)
{
  T temp = x;
  x = y;
  y = temp;
}

/// Returns nonzero if min <= candidate <= max.
template<class T>
inline int 
InRange(T min, T max, T candidate)
{
  return (candidate >= min) && (candidate <= max);
}

/// Returns maximum of two values.
template<class T>
inline const T&
Max(const T &x, const T &y)
{
  return (x < y) ? y : x;
}

/// Returns minimum of two values
template<class T>
inline const T&
Min(const T &x, const T &y)
{
  return (x > y) ? y : x;
}

/// Returns if two values are equal or not (by epsilon).
inline int
EpsilonEqual(const float &x, const float &y)
{
  return Abs(x-y) < CLimits::Small;
}

/// Returns if a value is zero (+/- epsilon)
inline int
EpsilonEqual(const float &x)
{
  return Abs(x) < CLimits::Small;
}

/// Returns if two values are equal or not by a given epsilon.
inline int
EpsilonEqual(const float &x, const float &y, const float &epsilon)
{
  return Abs(x-y) < epsilon;
}

/// Return binary logarithm of x, i.e. the logarithm base is 2.
#if (defined(_MSC_VER) || defined(__INTEL_COMPILER))
template<class T>
T log2(const T x)
{
  return log(x)*M_1_LN2;
}
#endif

inline int isnan_local(double x) { 
#ifdef __INTEL_COMPILER
  return isnan(x);
#else
  return std::isnan(x);
#endif
}

inline float
Luminance(const float RGB[3])
{
  assert(RGB);
  // coefficients to compute luminance from RGB
  const float wlum[3] = {0.299f,  // R
			 0.587f,  // G
			 0.114f}; // B
  float lum = wlum[0] * RGB[0] + wlum[1] * RGB[1] + wlum[2] * RGB[2];
  return lum;  
}

// statistics record in C++, see Donald Knuth's book
struct SVAR {
  float statsV; // computed statistics
  long int cnt; // the number of samples taken to compute statistics
  double mean; // mean value1
  double M2; // sum for variance1
  // Statistical support
  SVAR() { Reset();}

  // add a single sample
  void Update(const double newSampleValue) {
    cnt++;
    double delta = newSampleValue - mean;
    mean += delta/(double)cnt;
    M2 += delta*(newSampleValue - mean);
    // temporarily for debugging
    Evaluate();
  }
  void Reset() { statsV = 0; cnt = 0; mean=M2=0; }
  // It returns unbiased sample variance (so not for finite population)
  double Evaluate() {
    return (statsV = float(M2/(double)(cnt - 1)));
  }
  // It returns variance for the finite population (given N as fixed)
  double EvaluateFP() {
    return (statsV = float(M2/(double)cnt));
  }
};

__END_GOLEM_HEADER

#endif // __BASMATH_H__

