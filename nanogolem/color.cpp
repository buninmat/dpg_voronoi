// ===================================================================
//
// color.cpp
//
//     This is the implementation of color concepts in general
//      that is suitable for photorealistic rendering
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, November, 1999

// nanoGOLEM headers
#include "configg.h"
#include "color.h"

__BEGIN_GOLEM_SOURCE

//-------------------------------------------------
// static variables
// --------------------------------------------------
// constructors

// copy constructor
CColor::CColor(const CColor &val)
{
  // only copy the values to common variables
  for (int i = 0; i < 3; i++)
    _vals[i] = val._vals[i];
}

// assignment operator
CColor&
CColor::operator=(const CColor &val)
{
  if (this == &val)
    return *this; // OK
  
  // the common array of values 
  const float *src = &(val._vals[0]);
  float *dst = &(_vals[0]);
  for (int i = 3; i--;)
    *dst++ = *src++;
  return *this; // OK, everything finished
}

// output to stream
ostream&
operator<< (ostream &s, const CColor &A)
{
  const int N = 3;

  s << "ColorN=" << N  << " (";
  for (int i = 0; i < N; i++) {
    s << A[i];
    if (i < N-1)
      s << ", ";
  }
  s << ")";
  return s;
}

__END_GOLEM_SOURCE

