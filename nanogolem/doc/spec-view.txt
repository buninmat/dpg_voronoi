Written by Vlastimil Havran, October 2002
	
Specification of .view format
------------------------------

This format provides possibility to orient camera arbitrarily
and use any other geometry file for the scene itself.

General format is:

-v .....(position + orientation of camera)   geometryFile.ext

where by -v... switches we specify  only the camera position
and file 'geometryFile.ext' is the file that contains the geometry
of the scene using any implemented format.

Implementation and user note: The camera specified in .view file is
used as camera number 0 in world->scene->cameras, so preferably in the
all applications possible as the first camera available and used.

For position and orientation of the camera we have theses
possibilities:

---------------------------------------
Location (position) of the camera:

-vp x y z ; camera location point (center of projection)

---------------------------------------
Angle of the camera:

-vf f ; focus angle, default = 0.8 (radians). Should be thus
        smaller than M_PI.

or

-vh fh; focus angle horizontally (from left to right),
        but it degrees. Default value 45.8

-vv fv; focus angle horizontally (from left to right),
        but it degrees. Default value 45.8

----------------------------------------
Orientation of the camera:

Either one of these:

-vi x y z ; point of interest (we look at this point in the
            center of the screen).

-vd x y z ; the main direction of camera - default value (0, 0, -1)

and

-vu x y z ; up-vector  - default value (0, 1, 0)

-----------------------------------------------------------------

Example 1):

File conf.view contains following line:

-vi 0.0 0.0 0.0 -vp -4.0 -4.0 -4.0 -vu 0.0 0.0 1.0 conf.obj

we specify that we look from point (-4.0, -4.0, -4.0) to the
point (0.0, 0.0, 0.0) with the up vector (0.0, 0.0, 1.0) and the
scene that is used as the geometry, is file 'conf.mgf' (in 
Wavefront OBJ format).

-----------------------------------------------------------------
Example 2:

File conf.view contains following line:

-vp 7.62 0.15 2.44 -vd -0.4 0.907921 -0.24519 conf.nff

we specify that we look from point (- 7.62, 0.15, 2.44) at the direction
given by vector (-0.4 0.907921 -0.24519) with the default
up vector (0.0, 0.0, 1.0) and the scene that is used as the geometry,
is file 'conf.mgf' (in NFF format).
