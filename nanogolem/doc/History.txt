Date: 10/April/2016

History of a rendering package nanoGOLEM, written by Vlastimil Havran

The rendering system GOLEM was started in 1996/97 upon my PhD studies. 
Its original version was motivated by OORT package by Nicholas Wilt,
it was implemented in C++.

The rendering system has rather obsolete web pages at:

http://dcgi.felk.cvut.cz/home/havran/GOLEM/

Upon almost 16 years I devoted to this package a lot of time, also
several other people contributed to this package. For example, it included 
seven 3rd party libraries. The GOLEM became too big for an average student. It
included (without 3rd party libraries) over 40 rendering algorithms, 1260
classes and structs, 136000 lines of header files (*.h, 4.5 MBytes), 500,000
lines of implementation files (*.cpp, 16 MBytes).  The third party libraries
allowed parallelization, ray tracing NURBS, previewing on a screen, etc.

For the sake of large source code complexity, the system was again simplified 
to its core, which is called nanoGOLEM. The number of classes and functions is much
smaller and it is sufficient to use it to implement the exercise of the
subject "Data structures for computer graphics", lectured by Vlastimil Havran.

Of course, there are some imperfections in its initial release and I will 
appreciate the update and comments from the students.
