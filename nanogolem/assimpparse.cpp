// ===================================================================
//
// assimpparse.cpp
//     Source file for parsing the files with ASSIMP
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, 2016.

// standard headers
#include <vector>
#include <fstream>

// ASSIMP LIBRARY
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <assimp/Importer.hpp>


// nanoGOLEM headers
#include "configg.h"
#include "assimpparse.h"
#include "errorg.h"
#include "hdrimg.h"
#include "scene.h"
#include "world.h"

__BEGIN_GOLEM_SOURCE

// forward declarations
class CEnvironment;
class CScene;

void
CParserASSIMP::AddPointLights(const string &filename)
{
  const string newExt = string(".lights");
  string filename2(filename);
  filename2.append(newExt);

  fstream fp;
  fp.open(filename2.c_str());
  if (!fp)
    return;

  // The file is opened
  string data;
  int lightCount = 0;
  for (;;) {
    if (fp.eof()) {
      STATS << "Cnt lights = " << lightCount << endl;
      break;
    }
    CVector3D lightpos;
    CColor lightcolor;
    fp >> lightpos.x; // read the light position
    fp >> lightpos.y; // read the light position
    fp >> lightpos.z; // read the light position
    fp >> lightcolor[0]; // read the light color
    fp >> lightcolor[1]; // read the light color
    fp >> lightcolor[2]; // read the light color

    CPointLight *light = new CPointLight(lightpos, lightcolor);

    // adding the point to the set of objects
    world->_scene->AddLight(light);
    lightCount++;
  } // for      

  fp.close();
  return;
}

/// triangle as 3 indices
struct TriangleIdx
{
  TriangleIdx() {}
  TriangleIdx(unsigned int _a, unsigned int _b, unsigned int _c) :a(_a), b(_b), c(_c) {}
  TriangleIdx(const unsigned int * arr) :a(arr[0]), b(arr[1]), c(arr[2]) {}

  unsigned int a, b, c;
};

bool
CParserASSIMP::ParseBinary(const string &binaryFilename, CEnvironment &/*env*/, CScene &scene)
{
  scene.CleanUp();

  unsigned triangleCount;
  ifstream binaryFile;
  binaryFile.open(binaryFilename.c_str(), ios::in | ios::binary);
  // read the number of triangles
  binaryFile.read(reinterpret_cast<char *>(&triangleCount), sizeof(unsigned));
  // 3 vertices per triangle, each has 3 coordinates
  unsigned verticesSize = 3 * 3 * triangleCount;
  // 1 normal per triangle, each has 3 coordinates
  unsigned normalsSize = 3 * triangleCount;	
  float * vertices = new float[verticesSize];
  float * normals = new float[normalsSize];
  // skip the header
  binaryFile.seekg(256);
  // read the vertices
  binaryFile.read(reinterpret_cast<char *>(vertices), sizeof(float) * verticesSize);
  // skip to the beginning of normals
  binaryFile.seekg(256 + sizeof(float) * verticesSize);
  // read the normals
  binaryFile.read(reinterpret_cast<char *>(normals), sizeof(float) * normalsSize);
  binaryFile.close();
  CVector3D triangleVertices[3];
  CVector3D triangleNormal;
  CTriangle * triangle;
  int verticesIndex = 0;
  int normalIndex = 0;
  for (unsigned i = 0; i < triangleCount; i++) {
    triangleVertices[0].Set(vertices[verticesIndex], vertices[verticesIndex + 1], vertices[verticesIndex + 2]);
    triangleVertices[1].Set(vertices[verticesIndex + 3], vertices[verticesIndex + 4], vertices[verticesIndex + 5]);
    triangleVertices[2].Set(vertices[verticesIndex + 6], vertices[verticesIndex + 7], vertices[verticesIndex + 8]);
    triangleNormal.Set(normals[normalIndex], normals[normalIndex + 1], normals[normalIndex + 2]);
    triangle = new CTriangleNorm(triangleVertices, triangleNormal);
    scene.AddObject(triangle);
    verticesIndex += 9;
    normalIndex += 3;
  }
  delete[] vertices;
  delete[] normals;
  STATS << "Triangles cnt = " << triangleCount << endl;
  return false;
}

bool
CParserASSIMP::ParseOBJ(const string &filename, CEnvironment &/*env*/, CScene &scene)
{
  // Release the previously loaded mesh (if it exists)
  scene.CleanUp();
  Assimp::Importer imp;
  bool flagProcessUV = true;

  /// TODO: use a better approach, this just opens and reads the file again
  const aiScene * scn = imp.ReadFile(filename, aiProcess_Triangulate
				     | aiProcess_GenSmoothNormals
				     | aiProcess_GenUVCoords
				     | aiProcess_CalcTangentSpace
				     | aiProcess_JoinIdenticalVertices
				     );
  STATUS << "ASSIMP finished" << endl << flush;
  if (!scn) {
    WARNING << imp.GetErrorString() << endl;
    return NULL;
  }

  if (scn->mNumMeshes < 1) {
    WARNING << "no meshes found in scene " << filename << endl;
    return NULL;
  }

  unsigned int tot_vert = 0;
  unsigned int tot_faces = 0;
  for (unsigned int m = 0; m < scn->mNumMeshes; ++m) {
    aiMesh * am = scn->mMeshes[m];
    tot_vert += am->mNumVertices;
    tot_faces += am->mNumFaces;
    /// TODO: handle more + generate
    if (am->GetNumUVChannels() != 1)
      WARNING << "unsupported file with mutltiple (or no) textures "
	      << am->GetNumUVChannels() << " texturing will be broken for this object" << endl;
    if ((am->mNumFaces > 0) && (am->mFaces[0].mNumIndices != 3)) {
      FATAL << "mesh not triangular (assimp error ?)" << endl;
      return NULL;
    }
  }
  if (tot_vert < 1) {
    WARNING << "scene with no faces!" << endl;
    return NULL;
  }

  CVector3D* vtx = new CVector3D[tot_vert];
  CVector3D* nor = new CVector3D[tot_vert];
  CVector2D* tex = new CVector2D[tot_vert];
  TriangleIdx * tri = new TriangleIdx[tot_faces];
  unsigned int curr_vert = 0;
  unsigned int curr_tri = 0;

  unsigned triangleCount = 0;
  for (unsigned i = 0; i < scn->mNumMeshes; i++)
    triangleCount += scn->mMeshes[i]->mNumFaces;
  unsigned verticesSize = 3 * 3 * triangleCount;		// 3 vertices per triangle, each has 3 coordinates
  unsigned normalsSize = 3 * triangleCount;			// 1 normal per triangle, each has 3 coordinates
  float * verticesFile = new float[verticesSize];
  float * normalsFile = new float[normalsSize];
  int verticesIndex = 0;
  int normalsIndex = 0;

  for (unsigned int m = 0; m < scn->mNumMeshes; ++m) {
    aiMesh * am = scn->mMeshes[m];
    //int firstTriangle = curr_tri;
    for (unsigned int i = 0; i < am->mNumVertices; i++) {
      vtx[curr_vert + i] = CVector3D(am->mVertices[i].x, am->mVertices[i].y, am->mVertices[i].z);
    }
    memcpy(nor + curr_vert, am->mNormals, am->mNumVertices * sizeof(CVector3D));
    if (am->GetNumUVChannels() == 1) {
      for (unsigned int i = 0; i < am->mNumVertices; i++) {
	float xx = (*(am->mTextureCoords[0]))[i + 0];
	float yy = (*(am->mTextureCoords[0]))[i + 1];
	tex[curr_vert + i] = CVector2D(xx, yy);
      }
    }
    else
      memset(tex + curr_vert, 0, sizeof(CVector2D) * am->mNumVertices);

    CVector3D verts[3];
    CVector2D texcoords[3];
    CVector3D normals[3];
    CTriangle *poly = 0;
    for (unsigned int i = 0; i < am->mNumFaces; i++) {
      tri[curr_tri + i] = TriangleIdx(am->mFaces[i].mIndices);
      tri[curr_tri + i].a += curr_vert;
      tri[curr_tri + i].b += curr_vert;
      tri[curr_tri + i].c += curr_vert;
      // Either texture or not
      if ((am->HasTextureCoords(0)) && (flagProcessUV)) {
	verts[0] = vtx[tri[curr_tri + i].a];
	verts[1] = vtx[tri[curr_tri + i].b];
	verts[2] = vtx[tri[curr_tri + i].c];
	CVector3D v1 = Normalize(verts[1] - verts[0]);
	CVector3D v2 = Normalize(verts[2] - verts[0]);
	CVector3D normal = CrossProd(v1, v2);
	normals[0] = normals[1] = normals[2] = normal;
	texcoords[0] = tex[curr_vert + i];
	texcoords[1] = tex[curr_vert + i + 1];
	texcoords[2] = tex[curr_vert + i + 2];
	poly = new CTriangleInt(verts, normals, texcoords);

	verticesFile[verticesIndex] = verts[0][0];
	verticesFile[verticesIndex + 1] = verts[0][1];
	verticesFile[verticesIndex + 2] = verts[0][2];
	verticesFile[verticesIndex + 3] = verts[1][0];
	verticesFile[verticesIndex + 4] = verts[1][1];
	verticesFile[verticesIndex + 5] = verts[1][2];
	verticesFile[verticesIndex + 6] = verts[2][0];
	verticesFile[verticesIndex + 7] = verts[2][1];
	verticesFile[verticesIndex + 8] = verts[2][2];
	normalsFile[normalsIndex] = normal[0];
	normalsFile[normalsIndex + 1] = normal[1];
	normalsFile[normalsIndex + 2] = normal[2];
	verticesIndex += 9;
	normalsIndex += 3;
      }
      else {
	verts[0] = vtx[tri[curr_tri + i].a];
	verts[1] = vtx[tri[curr_tri + i].b];
	verts[2] = vtx[tri[curr_tri + i].c];
	CVector3D v1 = Normalize(verts[1] - verts[0]);
	CVector3D v2 = Normalize(verts[2] - verts[0]);
	CVector3D normal = CrossProd(v1, v2);

	poly = new CTriangleNorm(verts, normal);

	verticesFile[verticesIndex] = verts[0][0];
	verticesFile[verticesIndex + 1] = verts[0][1];
	verticesFile[verticesIndex + 2] = verts[0][2];
	verticesFile[verticesIndex + 3] = verts[1][0];
	verticesFile[verticesIndex + 4] = verts[1][1];
	verticesFile[verticesIndex + 5] = verts[1][2];
	verticesFile[verticesIndex + 6] = verts[2][0];
	verticesFile[verticesIndex + 7] = verts[2][1];
	verticesFile[verticesIndex + 8] = verts[2][2];
	normalsFile[normalsIndex] = normal[0];
	normalsFile[normalsIndex + 1] = normal[1];
	normalsFile[normalsIndex + 2] = normal[2];
	verticesIndex += 9;
	normalsIndex += 3;
      }
      scene.AddObject(poly);
    }
    curr_vert += am->mNumVertices;
    curr_tri += am->mNumFaces;
  }

  STATS << "Triangles cnt = " << curr_tri << endl;

  // save the vertices and indices to a binary file
  string binaryFilename(filename);
  string extension("bin");
  binaryFilename.replace(binaryFilename.end() - 3, binaryFilename.end(), extension);
  unsigned header[64];				// 256B
  header[0] = triangleCount;
  ofstream binaryFile;
  binaryFile.open(binaryFilename.c_str(), ios::out | ios::binary);
  // write the header
  binaryFile.write(reinterpret_cast<char *>(header), sizeof(unsigned) * 64);
  // skip the header
  binaryFile.seekp(256);
  // write the vertices
  binaryFile.write(reinterpret_cast<char *>(verticesFile), sizeof(float) * verticesSize);
  // skip to the beginning with vertices
  binaryFile.seekp(256 + sizeof(float) * verticesSize);
  // wite the normals
  binaryFile.write(reinterpret_cast<char *>(normalsFile), sizeof(float) * normalsSize);
  binaryFile.close();
  delete[] verticesFile;
  delete[] normalsFile;

  return false;
}

bool
CParserASSIMP::Parse(const string &filename, CEnvironment & env, CScene &scene)
{
  string binaryFilename(filename);
  string extension("bin");
  binaryFilename.replace(binaryFilename.end() - 3, binaryFilename.end(), extension);
  ifstream binaryFile(binaryFilename.c_str());
  bool binaryFileExists = binaryFile.good();
  binaryFile.close();
  if (binaryFileExists)
    return ParseBinary(binaryFilename, env, scene);
  else
    return ParseOBJ(filename, env, scene);
}

__END_GOLEM_SOURCE

