// ===================================================================
//
// basstr.cpp
//     Helper routines for strings
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, 1999.

// nanoGOLEM headers
#include "configg.h"
#include "basstr.h"

__BEGIN_GOLEM_SOURCE

// Puts the specified number of spaces out to given stream. Used for
// indentation during CObject3D::Describe().
void
Indent(ostream &app, int ind)
{
  int i;
  for (i = 0; i < ind; i++)
    app << ' ';
}

// prefix filename by a given directory name e.g. "aa.out","ref" makes
// "ref/aa.out", but "stats/aa.out","ref" makes "stats/ref/aa.out"
void
PrefixNameByDir(const string &filename, const string &dirname,
		string &outputName)
{
  outputName.resize(0);
  char dircat[2];
  dircat[0] = (char) DIRCAT;
  dircat[1] = 0x00;

  unsigned int pos = (unsigned int)filename.find_last_of(dircat[0]);

  if (pos < filename.length()) { // current name contains some dir separators
    outputName.assign(filename, 0, pos-1);
    outputName += dircat;
  }
  else
    pos = 0;
    
  outputName.append(dirname);
  outputName += dircat;
  outputName.append(filename, pos, filename.length()-pos);
}

// prefix filename extension by a given extPrefix
// for example the filename "data/tetra.rgb" and extPrefix 0102 makes
// the output filename "data/tetra0102.rgb"
// If no extension is found, then extPrefix is simply appended to the
// file name, e.g. for "balls" makes "balls0102"
void
PrefixExtensionByName(const string &filename, const string &extPrefix,
	  	      string &outputName)
{
  unsigned int pos = (unsigned int)filename.find_last_of('.');
  
  if (pos >= filename.length()) {
    outputName = filename;
    outputName.append(extPrefix); // just append
  }
  else {
    // the storage for extension of filename
    outputName.assign(filename, 0, pos);
    // append the extPrefix
    outputName.append(extPrefix);
    outputName.append(filename, pos, filename.length()-pos);
  }

  return;
}

// Removes path from filename,
// for example the filename "data/tetra.rgb" gets "tetra.rgb"
void
RemovePathFromFilename(const string &filename, string &outputName)
{
  unsigned int pos = static_cast<unsigned>(filename.find_last_of('/'));
  unsigned int length = static_cast<unsigned>(filename.length());
  if (pos >= length) {
    outputName = filename; // no path exists
  }
  else {
    // the storage for extension of filename
    outputName.assign(filename, pos+1, length-pos-1);
  }

  return;
}

// Removes filename from the whole path
// for example the filename "A/data/tetra.rgb" gets "data"
void
RemoveFilenameOnly(const string &filename, string &outputDir)
{
  unsigned int pos = static_cast<unsigned>(filename.find_last_of('/'));
  unsigned int length = static_cast<unsigned>(filename.length());
  if (pos >= length) {
    outputDir = filename; // no path exists
  }
  else {
    // the storage for extension of filename
    outputDir.assign(filename, 0, pos);
  }

  return;
}

// changing the extension of the filename
// filename .. filename to be changed
// maxLength .. maximal length of array filename that should not be exceeded
// newExtension .. newExtension added/replaced  ("vis", "dbg", "rgb" etc.)
void
ChangeFilenameExtension(const string &filename, const string &newExtension,
			string &outputName)
{
  outputName = filename;
  unsigned int pos = (unsigned int)outputName.find_last_of('.');
  
  if ( pos == outputName.length()) {
    // filename has no extension
    outputName.append(".");
  }
  else { // shorten the string
    // If the newExtension is of the zero size
    if (newExtension.size() == 0)
      // remove the former extension including the dot
      // This can be used to remove the last extension completely
      outputName.resize(pos);
    else
      // Keep the last dot befor the extension
      outputName.resize(pos+1);
  }

  outputName.append(newExtension);
}

/// Extracts the only filename, excludes the whole path, e.g. "aaaa/bbb.dbg" -> "bbb"
string
ExtractFileName(const string &fileName)
{
  // makes just a single name
  string filenameRead(fileName);
  string gname, gname2; // remove 'prepend' path
  RemovePathFromFilename(filenameRead, gname2);
  // store basic name to 'gname'
  ChangeFilenameExtension(gname2, string(""), gname);
  return gname;
}

/// Extracts the only extension, e.g. "aaaa/bbb.dbg" -> "dbg"
string
ExtractExtension(const string &fileName)
{
  // makes just a single name
  string gname(fileName);
  unsigned int length = static_cast<unsigned>(gname.length());
  unsigned int pos = static_cast<unsigned>(gname.find_last_of('.'));

  string output = string(gname, pos+1, length-pos-1);

  return output;
}
  
// Skip all the rows in a text file starting from the current character,
// when the current character is a 'mark'. All the following lines
// starting with the 'mark' character are also skipped.
void
EatComments(istream &inpf, char mark)
{
  // current character
  char ch;
  
  // first character to be read
  inpf >> ch;
  
  // loop
  while (ch == mark) {
    inpf.ignore(100000, '\n'); // skip the line completely
    // first character to be read
    if (inpf.eof())
      return; // end of the stream
    else
      inpf >> ch; // read the first character on the line
  }
  
  // return the character back to the stream
  inpf.putback(ch);
}

// Skip all the whitespaces in the input stream
void
EatWhitespace(istream &inpf)
{
  // the current character
  char ch;

  // first character to be read
  inpf >> ch;

  // loop
  while (ch==' ' || ch=='\t' || ch=='\n' || ch=='\f' || ch=='\r' || ch=='\b') {
    if (inpf.eof())
      return; // end of the stream
    else
      inpf >> ch; // read the first character on the line
  }
  
  // return the first non-white character back to the stream to be read by the
  // next operation for reading
  inpf.putback(ch);
}

bool
FindLineWithKeyword(istream &inpf, const string &keyword)
{
  char ch;
  int length = (unsigned int)keyword.size();

  // now we assume that we are at the beggining of a line,
  // otherwise the same string when multiply stated on a line
  // can be found.
  
  for (;;) {
    bool match = true;
    for (int i = 0; i < length; i++) {
      // i-th character on a line to be read
      if (!inpf.get(ch)) {
	match = false;
	inpf.ignore(100000, '\n');
	if (inpf.eof()) {
	  // end of the stream
	  return true; // text not found - end of stream
	}
      }

      if (ch != keyword[i]) {
	inpf.ignore(100000, '\n'); // skip the line completely
	// first character to be read
	match = false; // next line
	break;
      }
    } // for i

    if (match)
      return false; // OK, line starting with 'mark' string found
    // otherwise go to the next line
  }
  return true; // not found
}

// Converts all the possible characters to lowercase
void
ToLowerCase(string &str)
{
  int i;
  int length = (int) str.length();
  for (i = 0; i < length; i++) {
    str[i] = tolower(str[i]);
  }
}

__END_GOLEM_SOURCE
