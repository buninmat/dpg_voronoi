// ================================================================
//
// basmacr.h
//     Basic macros and definitions
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, 2000.

#ifndef __BASMACR_H__
#define __BASMACR_H__

// nanoGOLEM headers
#include "configh.h"
#include "errorg.h"

__BEGIN_GOLEM_HEADER

// forward declarations

//-----------------------------------------------------------------------------
// Miscellaneous helper macros
//-----------------------------------------------------------------------------

/// A very useful macro for debugging purposes.
#define PRINTVAR(a) DEBUG << #a"\t" << a << std::endl;

/// Delete a scalar pointer and set it to zero.
#ifndef SAFE_DELETE
#define SAFE_DELETE(p)       { delete   (p); (p)=0;  }
#endif

/// Delete an array pointer and set it to zero.
#ifndef SAFE_DELETE_ARRAY
#define SAFE_DELETE_ARRAY(p) { delete[] (p); (p)=0;  }
#endif

/// Call a method for the given object if the object != 0
#define SAFE_CALL(object,method)  { if(object) { (object)->method ; } }

// Definition of NULL for new C++ compilers
#ifndef NULL
#define NULL (void *)0
#endif // NULL

__END_GOLEM_HEADER

#endif // __BASMACR_H__
