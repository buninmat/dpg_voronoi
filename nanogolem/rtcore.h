// ===================================================================
//
// rtcore.h
//     Header file for Application Algoritm class.
//
// Class: CRT_Core_App
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, December 1999, 2000

#ifndef __RTCORE_H__
#define __RTCORE_H__

// nanoGOLEM headers
#include "configh.h"
#include "appcore.h"
#include "asds.h"
#include "color.h"

__BEGIN_GOLEM_HEADER

// forward declarations

// -------------------------------------------------------------------------
// This is the core class of rendering algorithms casting rays. The camera
// is not defined in the class since it is not required.
// -------------------------------------------------------------------------
class CRT_Core_App:
  public CSynthesisApp
{
protected:
  // auxiliary spatial data structure to accelerate
  // ray-casting/visibility tests
  CASDS                    *_asds;
  
  // builds up the ASDS for a set of objects
  bool BuildUpASDS();
  // save/load the application data to/from visibility file

  // Initialize the ASDS
  bool InitASDS();

  void InitObjects(CObjectList &objects);
public:
  // constructor
  CRT_Core_App();
  // destructor
  virtual ~CRT_Core_App();
  
  // precompute anything what is needed
  // returns false, if everything is OK
  virtual bool Init(CWorld *world, string &outputName);
  // deletes all the auxiliary data structures
  virtual bool CleanUp();
  // runs the application, in this common class do nothing
  virtual bool Run() { return false;}
  // reports the statistics result of the specific application
  virtual bool DoReport(const CWorld &world, ostream &app);

  // returns application type, must be defined by the application
  virtual EApplicationType GetApplicationType() const {
    return EE_RT_Core;
  }
  // returns the ASDS if it is constructed within the application. If
  // the ASDS is not constructed, returns NULL.
  virtual const CASDS* GetASDS() const { return _asds; }
};

__END_GOLEM_HEADER

#endif // __RTCORE_H__

