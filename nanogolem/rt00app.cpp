// ===================================================================
//
// rt00app.cpp
//             Ray tracer algorithm number 00 - pure ray casting
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, 2010

// standard headers
#include <cstdlib>
#include <fstream>
#include <algorithm>

// nanoGOLEM headers
#include "configg.h"
#include "environ.h"
#include "rnd.h"
#include "rt00app.h"
#include "scene.h"
#include "vector3.h"

__BEGIN_GOLEM_SOURCE

void
CRT_00_App::ComputeColor(const CRay &ray,
			 CHitPointInfo &info, CColor &color)
{
  // just set the black color if no object was hit
  if (info.GetObject3D() == NULL) {
    color = _scene->bgColor;
    return;
  }

  // the diffuse BRDF of the surface 
  info.Extrap();
  CColor d;

  // -----------------------------------------------------------------
  // use the new property of the surface returning standard color
  float minCosAngle = 0.05f;
  
  float sum = d.Sum();
  if (sum < 0.02f) // almost black
    d = 0.3f; // set it to gray

  // We have to call GetNormal() as it is not computed
  float dotProd = -DotProd(info.GetNormal(), ray.GetDir());
  
  // whether to show the surfaces with opposite normals as well -
  // for scene debugging!
  if (_showBackFaceSurfaces) {
    d = std::max<float>(minCosAngle, fabs(dotProd));
  }
  else {
    if (dotProd < 0.0) {
      // surface normal oriented towards the viewer
      d = std::max<float>(minCosAngle, -dotProd);
    }
    else
      d = 0.f; // black color
  }    
  // finaly, set the color of a pixel, using the weights
  color = d;
  color *= _imgHdrMult; // multiplying the intensity
  return;
}

bool
CRT_00_App::Init(CWorld *world, string &outputName)
{
  // init previous classes
  if (CRT_CoreWithCamera_App::Init(world, outputName))
    return true; // error occured

  _environment->GetBool("RTCore.showBackfaceSurfaces",
			_showBackFaceSurfaces);
  _environment->GetFloat("Image.HDR.multiplier",
			 _imgHdrMult);

  STATUS << "Pure ray casting" << endl << flush;
  return false; // ok
}

bool
CRT_00_App::RunCommonPrepare()
{
  bool OK = CRT_CoreWithCamera_App::RunCommonPrepare();
  if (OK)
    return OK; // problem

  return false; // OK
}

// --------------------------------------------------------------------------
// This computes the optimization
bool
CRT_00_App::Run()
{
  if (RunCommonPrepare())
    return true; // problem
  
  bool ok;
  ok = RunCommonCompute();
  if (ok)
    return ok; // problem

  bool HDRflag = true; // true .. save HDR image, false ... LDR image

  string ftype;
  _environment->GetString("OutputFileType", ftype);
  if ( (ftype.compare("png") == 0) ||
       (ftype.compare("PNG") == 0) ) {
    HDRflag = false;
  }
  
  // Save image from this single camera
  SaveImage(HDRflag);
  
  // Save the image to the file and deallocate the dynamic variables
  if (RunCommonFinish())
    return true; // problem
  
  return ok;
}

extern CSynthesisApp*
AllocRT_00_App()
{
  return new CRT_00_App();
}

__END_GOLEM_SOURCE

