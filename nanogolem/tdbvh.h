// ===================================================================
//
// tdbvh.h
//      CTDBVH - Kajiya bounding volume hierarchy
//
// Class: CTDBVH
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, 2005

#ifndef __TDBVH_H__
#define __TDBVH_H__

// standard C++ headers
#include <iostream>
#include <ostream>

// nanoGOLEM headers
#include "configh.h"
#include "asds.h"
#include "sbbox.h"

__BEGIN_GOLEM_HEADER

// forward declarations
class CObjectList;
class CObject3D;
class CRay;

// ----------------------------------------------------------
class CTDBVH:
  public CASDS_BB
{
protected:
  // the number of objects in the case the object list is
  // not copied to this->objlist
  int countObjects;

  // ------------------------------------------------------------------
  // The generic node of the bounding volume hierarchy,
  // defining just a leaf, in total 8 bytes.
  struct SBVHnode {
    int type; // 0 .. leaf, interior node: 1 X-axis, 2 Y-axis, 3 Z-axis
    union {
      SBVHnode *left;
      CObject3D *obj;
    };
    // constructor
    SBVHnode() {type = 0; this->obj = (CObject3D*)0;}
    int GetType() const {return type;}
  };
  
  // -------------------------------------------------------------------
  // Struct definining interior node of BVH, in total 8+24 = 32 Bytes when
  // optimized and using DFS order in the hierarchy, otherwise, it is 32+4=36B
  struct SBVHinterior:
    public SBVHnode
  {
    SBVHinterior():SBVHnode() { type=1; right = 0; }
    inline SBVHinterior(SBVHnode* prim1, SBVHnode* prim2,
			const SBBox &_bbox);
    // The box of the whole interior node
    SBBox bbox;
    // the pointer to the right child
    SBVHnode* right;
    void Delete();
  };

  // The root node of the hierarchy
  SBVHnode *root;

  // The array of objects
  CObject3D **array;

  // This macro allows to use boxes in the array, which makes the
  // construction algorithm faster.
#define ARRAY_BOXES

  // The array of boxes
#ifdef ARRAY_BOXES
  SBBox *boxes;
#endif // ARRAY_BOXES

  // Splitting algorithm
  int _splittingAlgorithm;
  
  // The function for building recursively - using a single axis, round robin
  SBVHnode *BuildRecursively(const SBBox &bbox,
			     unsigned int firstIndex,
			     unsigned int lastIndex,
			     unsigned int axis);

  // The definition of the stack
  struct SStackEntry {
    SBVHnode *pointer; // pointer to the node to be traversed
    float mint;    // minimum distance of the ray interval
    float maxt;    // maximum distance of the ray interval
    float padding; // just padding to have 16-Bytes alignment
  };

  // Definition of the stack depth
  enum {
    DepthOfStack = 100
  };
  
  // The stack for traversal
  static CTDBVH::SStackEntry stack[DepthOfStack];

  // statistics data
  int currentDepth;

public:
  // default constructor
  CTDBVH();

  // default destructor
  virtual ~CTDBVH();

  virtual void BuildUp(const CObjectList &objlist);
  virtual EASDS_ID GetID() const { return ID_BVHTopDown;}  
  
  virtual void GetBBox(SBBox &box) { box = bbox;}  
  virtual void ProvideID(ostream &app);

  void CheckRecursively(SBVHnode *node) const;
  
  // Delete the data structures
  virtual void Remove();

protected:
  // find nearest
  virtual const CObject3D* FindNearestI(CRay &ray, CHitPointInfo &info);
};

__END_GOLEM_HEADER

#endif // __TDBVH_H__

