
#ifndef __ASDS_VOR_H__
#define __ASDS_VOR_H__

#include "configg.h"
#include "configh.h"
#include "appcore.h"
#include "asds.h"
#include "color.h"
#include "vector3.h"
#include <array>

__BEGIN_GOLEM_HEADER

class CASDS_Voronoi:
	public CAObjectList
{
public:

	CASDS_Voronoi() { }

	virtual ~CASDS_Voronoi(){}

	virtual EASDS_ID GetID() const override { return ID_ASDS_Voronoi; };

	virtual void ProvideID(std::ostream& app) override;

	virtual void TraversalReset() override;

	virtual void BuildUp(const CObjectList& objlist) override;

protected:
	virtual const CObject3D* FindNearestI(CRay& ray, CHitPointInfo& info);
private:

	struct Particle {
		CVector3D pos;
		CObjectList objects;
	};
	struct Vertex {
		vector<Particle> particles;
		vector<Vertex> contiguous;
		CVector3D pos;
		Vertex() = default;
		Vertex(array<CVector3D, 4> simplex);
	};

	array<CVector3D, 4> boundingSimplex;

	CVector3D centroid;
	Vertex centralVertex;

	void AddParticle(Particle& p);

	void CreateBoundingSimplex(const vector<CVector3D> &particles);
};

__END_GOLEM_HEADER

#endif
