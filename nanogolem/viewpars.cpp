// ===================================================================
//
// viewpars.cpp
//     Implementation of the VIEW format
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, 2000.

// Standard C++ headers
#include <fstream>

// nanoGOLEM headers
#include "configg.h"
#include "errorg.h"
#include "vector3.h"
#include "viewpars.h"
#include "world.h"

__BEGIN_GOLEM_SOURCE

// Parse VIEW file
bool
CParserVIEWRun(const string &filename, string &newSceneFilename, CWorld &world)
{
  const int MaxStringLength = 256;
  char nexttok[MaxStringLength];
  char cmd;

  // open the scene file
  ifstream inpf(filename.c_str());
  if (!inpf) {
    sprintf(nexttok, "Could not open input file: %s", filename.c_str());
    FATAL << nexttok << endl;
    return true; // error
  }

  /* The camera setting defined by these commands:

    -vi x y z ; point of interest

    OR

    -vd x y z ; the view direction of camera (need not be normalized)

    -vu x y z ; up-vector

    OR

    -vp x y z ; camera location point

    -vf f ; focus angle, default = 50 degrees

    -vh hsize; horizontal view size in degrees. This will be measured in degrees for the full
               field of view for perspective and fisheye views, and in world
               coordinates for parallel views.
    -vv vsize; vertical view size in degrees. This will be measured in degrees for the full field
               of view for perspective and fisheye views, and in world coordinates for
               parallel views.

    -d new camera setting; used as as separator as the cameras are being defined

    -s scenefilename.ext
  */

  // the location of the camera
  CVector3D orig = CVector3D(0, 0, -1);
  // position to which is camera oriented
  CVector3D at = CVector3D(0, 0, 0);
  // up vector
  CVector3D up = CVector3D(0, 1, 0);
  // Angle of focus in horizontal direction - radians
  float vh_ang = 50.0f * float(M_PI)/180.0f ;
  // Angle of focus in vertical direction - radians
  float vv_ang = vh_ang;
  // Direction of camera
  CVector3D dir = CVector3D(0, 0, 1);

  // just flags if the corresponding feature was specified
  int atSpecified = 0;
  int dirSpecified = 0;
  int origSpecified = 0;

  // flag if the scene is defined - inital setting to true
  // is used as the hack, do not change it!
  bool sceneDefined = true;
  
  nexttok[0] = '\0';
  while (!inpf.eof()) {
    if (nexttok[0]) {
      cmd = nexttok[0];
      nexttok[0] = '\0';
    }
    else {
      nexttok[1] = nexttok[2] = nexttok[3] = nexttok[4] = '\0';
      inpf >> nexttok;
      cmd = nexttok[0];
      nexttok[0] = '\0';
      if (!inpf)
        break;
    }

    switch (cmd) {
      // Comment
      case '#': {
        inpf.getline(nexttok, MaxStringLength - 1);
        nexttok[0] = '\0';
        break;
      }

      // Viewpoint specification
      case '-': {
	if (nexttok[1] == 'v') {
	  cmd = nexttok[2];

	  if (cmd == 'i') { // -vi point of interest
	    inpf >> at.x >> at.y >> at.z;
	    atSpecified = 1;
	    break; // next command
	  }

	  if (cmd == 'p') { // -vp camera point
	    inpf >> orig.x >> orig.y >> orig.z;
	    origSpecified = 1;
	    break; // next command
	  }

	  if (cmd == 'd') { // -vd direction vector
	    inpf >> dir.x >> dir.y >> dir.z;
	    float mag = dir.x*dir.x + dir.y*dir.y + dir.z*dir.z;
	    if (mag <1e-8) {
	      FATAL << "Incorrect view direction = "
		    << dir.x << ", " << dir.y << ", " << dir.z << endl;
	      FATAL << "Exiting." << endl;
	      FATAL_ABORT;
	    }
	    mag = sqrt(mag);
	    dir /= mag;
	    dirSpecified = 1;
	    break; // next command
	  }

	  if (cmd == 'u') { // -vu up-vector
	    inpf >> up.x >> up.y >> up.z;
	    break; // next command
	  }
	
	  if (cmd == 'f') { // -vf f ; focus, default = 0.8
	    inpf >> vv_ang;
	    //vv_ang *= float(M_PI) / 180.0f; // result in radians
	    vh_ang = vv_ang; // both angles, horizontal and vertical
	    break; // next command
	  }

	  if (cmd == 'v') { // -vv f ; vertical view size in degrees
	    inpf >> vv_ang;
	    vv_ang *= float(M_PI) / 180.0f; // in radians
	    break; // next command
	  }
	  if (cmd == 'h') { // -vh f ; horizontal view size in degrees
	    inpf >> vh_ang;
	    vh_ang *= float(M_PI) / 180.0f; // in radians
	    break; // next command
	  }
	}
	else {
	  cmd = nexttok[1];
	  // put new commands after '-' here

	  if (cmd == 'd') { // -d
	    // define the camera now
	    sceneDefined = false;
	    // break is not placed here intentionally
	  }
	  else {
	    if (cmd == 's') { // -s
	      // the name of the scene follows
	      inpf >> nexttok;
	      // this copy must be here
	      cmd = nexttok[0];
	      sceneDefined = true;
	      // break is not placed here intentionally
	    }
	    else
	      return true; // unexpected command!
	  }
	}

	// No other commands are allowed here !
      } // case '-'

      default: { // the name of the file with the scene geometry

	// the location specification
	int sumspec = atSpecified + dirSpecified + origSpecified;

	if (sumspec > 2) {
	  FATAL << "The camera setting is overspecified, use one/two"
                    "parameters for location and orientation!" << endl;
	  return true;
	}

	if (sumspec < 2) {
	  WARNING << "The camera setting is underspecified, using "
                      "some defalt setting for unknown parameters!" << endl;
	}

	if (dirSpecified) {
	  if (atSpecified)
	    orig = at - dir;
	  else {
	    if (origSpecified)
	      at = orig + dir;
	    else
	      dir = orig - at;
	  }
	}
	else { // dir not specified, so "at and orig" have to be specified
	  assert(atSpecified);
	  assert(origSpecified);
	  dir = at - orig;
	}
	
	if (1) {
	  STATS << "Camera orig = " << orig.x << " " << orig.y << " " << orig.z << endl;
	  CVector3D dirpr(at.x - orig.x, at.y - orig.y, at.z - orig.z);
	  dirpr.Normalize();
	  STATS << "Camera dir = " << dirpr.x << " " << dirpr.y << " " << dirpr.z << endl;
	  STATS << "Camera at = " << at.x << " " << at.y << " " << at.z << endl;
	  STATS << "Camera up = " << up.x << " " << up.y << " " << up.z << endl;
      STATS << "Camera f = " << vv_ang << endl;
	}

	// Set the camera
	world._camera.loc = orig;
	world._camera.dir = Normalize(dir);
	world._camera.up = Normalize(up);
	// the resulting field of view angle in radians
	world._camera.FOV = sqrt(sqr(vv_ang) + sqr(vh_ang));
	world._camera.wasSet = true;

	if (sceneDefined) {
	  // copy back the first character
	  nexttok[0] = cmd;
	  // This must be the filename of geometry file to parse
	  newSceneFilename.assign(nexttok);
	  // closing the input stream
	  inpf.close();
	  // OK, continue parsing the other file with the geometry
	  STATUS << "The scene definition file = " << newSceneFilename << endl;
	  return false;
	}

	// go for other commands in the file
	// Reset all the flags, when new flag is added, it must be here

	// the location of the camera
	orig.x = 0; orig.y = 0; orig.z = 0;
	// position to which is camera oriented
	at.x = 1; at.y = 0; at.z = 0;
	// up vector
	up.x = 0; up.y = 0; up.z = 1;
	// Angle of focus in horizontal direction
	vh_ang = 50.0f * float(M_PI)/180.0f; // in radians
	// Angle of focus in vertical direction
	vv_ang = vh_ang;
	// Direction of camera
	dir.x = 0; dir.y = 0; dir.z = -1;
	// just flags if the corresponding feature was specified
        atSpecified = 0;
	dirSpecified = 0;
	origSpecified = 0;

	// again, hack
	sceneDefined = true;

	// for reading the next command
	nexttok[0] = '\0';
      }
    } // switch
  } // while

  // closing the stream
  inpf.close();

  FATAL << "Unknown command specified in " << filename << " file" << endl;
  
  return true; // something unexpected happened
}

__END_GOLEM_SOURCE

