// ================================================================
//
// hdrimg.cpp
//     This is the support to report messages and errors/
//       and debug bugs in nanoGOLEM.
//
// Class: CHDRImage, CEnvironmentMap
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, February 2000

// Standard C++ headers
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <vector>
#include <algorithm>

// 3rd party library headers
#include "png.h"

// nanoGOLEM headers
#include "configg.h"
#include "basstr.h"
#include "colorfs.h"
#include "hdrimg.h"
#include "rnd.h"
#include "talloc.h"
#include "vector2.h"
#include "vector3.h"

__BEGIN_GOLEM_SOURCE

// Copy constructor
CHDRImage::CHDRImage(const CHDRImage &src)
{
  this->cols = 0;
  this->Allocate(src.width, src.height);
  this->filenameRead = src.filenameRead; // if read, then filename
  this->gname = src.gname; // the name without extension and path
  this->CopyDataFromArray(width, height, 3, src.cols, width, 0, 0, false);
}

// More complex copy constructor
CHDRImage::CHDRImage(const CHDRImage &src, int pseudoColorTechnique,
		     bool copyImage, int widthBargraph, int borderBargraph)
{
  this->cols = 0;
  this->Allocate(src.GetWidth() + widthBargraph + borderBargraph,
		 src.GetHeight());
  int w2 = src.GetWidth();
  for (int y = 0; y < height; y++) {
    if (copyImage) {
      for (int x = 0; x < w2; x++) {
	int wi = (w2*y+x)*3; // source
	int wo = (width*y+x)*3; // target
	cols[wo+0] = src.cols[wi+0];
	cols[wo+1] = src.cols[wi+1];
	cols[wo+2] = src.cols[wi+2];
	//setRGB_XY(x, y, imgBTF[i], imgBTF[i+1], imgBTF[i+2]);
      } // for x
    }
    if (1) {
      // copy the bargraph values to the right
      CColor RGB;
      float value = (float)(height-y)/(float)height; // get the color
      GetPseudoColor(pseudoColorTechnique, value, 0.0, 1.0, RGB);
      for (int x = w2; x < w2 + borderBargraph; x++) {
	SetRGB_XY(x, y, 0, 0, 0); // make it black
      }
      for (int x = w2 + borderBargraph; x < width; x++) {
	SetRGB_XY(x, y, RGB[0], RGB[1], RGB[2]);	
      }
    }
  } // for y
  return;
}

// Returns maximum value
float
CHDRImage::GetMaxValue(CColor &rgb) const
{
  float lum[3]; // storing maximum luminance here for all channels
  float ml = 0;
  for (int y = 0; y < height; y++) {
    for (int x = 0; x < width; x++) {
      lum[0] = GetValueChannel(x, y, 0);
      lum[1] = GetValueChannel(x, y, 1);
      lum[2] = GetValueChannel(x, y, 2);
      float l = Luminance(lum);
      if (l > ml) {
	ml = l;
	rgb[0] = lum[0];
	rgb[1] = lum[1];
	rgb[2] = lum[2];
      }
    } // for x
  } // for y
  return ml;
}

int
CHDRImage::Mask(const CHDRImage &m, float threshold, int x1, int y1, int x2, int y2)
{
  if (x2 < 0)
    x2 = 0;
  if (y2 < 0)
    y2 = 0;
  if (x2 > width)
    x2 = width;
  if (y2 > height)
    y2 = height;
  int maskedCnt = 0;
  // Copy the data from PNG representation
  for (int y=y1; y < y2; y++) {
    for (int x=x1; x < x2; x++) {
      int ww = (width * y + x)*3;
      int ww2 = (m.width * y + x)*3;
      float s = m.cols[ww2] + m.cols[ww2+1] + m.cols[ww2+2];
      if (s < threshold) {
	maskedCnt++;
	// Zero the values
	for (int isp=0; isp < 3; isp++) {
	  // set it to the output format as required by RGBE_WRITE
	  cols[ww+isp] = 0.0;
	}
      }
    } // for jcol
  } // for irow
  return maskedCnt;
}

int
CHDRImage::Mask(const CHDRImage &m, float threshold, float red, float green, float blue,
		int x1, int y1, int x2, int y2)
{
  if (x2 < 0)
    x2 = 0;
  if (y2 < 0)
    y2 = 0;
  if (x2 > width)
    x2 = width;
  if (y2 > height)
    y2 = height;
  int maskedCnt = 0;
  // Copy the data from PNG representation
  for (int y=y1; y < y2; y++) {
    for (int x=x1; x < x2; x++) {
      int ww = (width * y + x)*3;
      int ww2 = (m.width * y + x)*3;
      float s = m.cols[ww2] + m.cols[ww2+1] + m.cols[ww2+2];
      if (s < threshold) {
	maskedCnt++;
	// Set the values to a color specified
	cols[ww+0] = red;
	cols[ww+1] = green;
	cols[ww+2] = blue;
      }
    } // for jcol
  } // for irow
  return maskedCnt;
}

void
CHDRImage::Multiply(float coeff, int x1, int y1, int x2, int y2)
{
  if (x2 < 0)
    x2 = width;
  if (y2 < 0)
    y2 = height;
  
  for (int y=y1; y < y2; y++) {
    for (int x=x1; x < x2; x++) {
      int ww = (width * y + x)*3;
      for (int isp=0; isp < 3; isp++) {
	cols[ww+isp] *= coeff;
      }
    } // for jcol
  } // for irow
}

void
CHDRImage::AddOffset(float coeff, int x1, int y1, int x2, int y2)
{
  if (x2 < 0)
    x2 = width;
  if (y2 < 0)
    y2 = height;
  
  for (int y=y1; y < y2; y++) {
    for (int x=x1; x < x2; x++) {
      int ww = (width * y + x)*3;
      for (int isp=0; isp < 3; isp++) {
	cols[ww+isp] += coeff;
      }
    } // for jcol
  } // for irow
  return;
}

// This allows limit if from the bottom
void
CHDRImage::Maximize(float coeff, int x1, int y1, int x2, int y2)
{
  if (x2 < 0)
    x2 = width;
  if (y2 < 0)
    y2 = height;
  
  for (int y=y1; y < y2; y++) {
    for (int x=x1; x < x2; x++) {
      int ww = (width * y + x)*3;
      for (int isp=0; isp < 3; isp++) {
	if (cols[ww+isp] < coeff)
	  cols[ww+isp] = coeff;
      }
    } // for jcol
  } // for irow
}

 // This allows limit if from the top
void
CHDRImage::Minimize(float coeff, int x1, int y1, int x2, int y2)
{
  if (x2 < 0)
    x2 = width;
  if (y2 < 0)
    y2 = height;
  
  for (int y=y1; y < y2; y++) {
    for (int x=x1; x < x2; x++) {
      int ww = (width * y + x)*3;
      for (int isp=0; isp < 3; isp++) {
	if (cols[ww+isp] > coeff)
	  cols[ww+isp] = coeff;
      }
    } // for jcol
  } // for irow
  return;
}

// This makes absolute value of the image
void
CHDRImage::Fabs(int x1, int y1, int x2, int y2)
{
  if (x2 < 0)
    x2 = width;
  if (y2 < 0)
    y2 = height;
  
  for (int y=y1; y < y2; y++) {
    for (int x=x1; x < x2; x++) {
      int ww = (width * y + x)*3;
      for (int isp=0; isp < 3; isp++) {
	if (cols[ww+isp] < 0)
	  cols[ww+isp] = -cols[ww+isp];
      }
    } // for jcol
  } // for irow
  return;
}

void
CHDRImage::MakeGray(int x1, int y1, int x2, int y2)
{
  for (int y=y1; y < y2; y++) {
    for (int x=x1; x < x2; x++) {
      int ww = (width * y + x)*3;
      float lum = Luminance(cols+ww);
      // turn it to gray, whole image
      cols[ww+0] = lum;
      cols[ww+1] = lum;
      cols[ww+2] = lum;
    } // for jcol
  } // for irow
  return;
}

void
CHDRImage::MakeWholeGray()
{
  for (int y=0; y < height; y++) {
    for (int x=0; x < width; x++) {
      int ww = (width * y + x)*3;
      float lum = Luminance(cols+ww);
      // turn it to gray, whole image
      cols[ww+0] = lum;
      cols[ww+1] = lum;
      cols[ww+2] = lum;
    } // for jcol
  } // for irow
  return;
}

float
CHDRImage::MinLuminanceValue() const
{
  float minValue = 1e31f;

  for (int y=0; y < height; y++) {
    for (int x=0; x < width; x++) {
      int ww = (width * y + x)*3;
      float lum = Luminance(cols+ww);
      if (lum < minValue) {
	minValue = lum;
      }
    } // for jcol
  } // for irow

  // return the minimumValue
  return minValue;
}

float
CHDRImage::MaxLuminanceValue() const
{
  float maxValue = -1e31f;

  for (int y=0; y < height; y++) {
    for (int x=0; x < width; x++) {
      int ww = (width * y + x)*3;
      float lum = Luminance(cols+ww);
      if (lum > maxValue) {
	maxValue = lum;
      }
    } // for jcol
  } // for irow

  // return the ratio of white pixels
  return maxValue;
}


// Computes percentile between values (darkest=0.0 and brightest=1.0)
float
CHDRImage::PercentileLuminanceValue(float ratio) const
{
  //float maxValue = -1e31;
  std::vector<float> myvector;
  // to speed up the filling with the data
  myvector.reserve(height*width);

  for (int y=0; y < height; y++) {
    for (int x=0; x < width; x++) {
      int ww = (width * y + x)*3;
      float lum = Luminance(cols+ww);
      myvector.push_back(lum);
    } // for jcol
  } // for irow

  if (ratio < 0.0) ratio = 0.0;
  if (ratio > 1.0) ratio = 1.0;
  int totalcnt = height*width;
  int position = (int)((float)totalcnt*ratio);
  // rearranges the elements
  std::nth_element (myvector.begin(), myvector.begin()+position, myvector.end());
  // It should return the ratio-th element value
  float lum = myvector[position];
  int cnt = 0;
  for (int i = 0; i < totalcnt; i++) {
    if (myvector[i] <= lum)
      cnt++;
  }

  STATS << "cnt= " << cnt << " totalcnt= " << totalcnt
	<< " ratio= " << (float)cnt/(float)totalcnt << endl;
  
  // return the ratio of white pixels
  return lum;
}

// shifts circularly the image to left or right
void
CHDRImage::HorizontallyShift(int pixelOffset)
{
  int planesCnt = 3;
  float ***rowaux = allocation3(0, planesCnt, 0, 1, 0, width);

  for (int y=0; y < height; y++) {
    // copy the data to a temporary buffer
    for (int x=0; x < width; x++) {
      int ww = (width * y + x)*3;
      for (int isp=0; isp < planesCnt; isp++) {
	// set it to the output format as required by RGBE_WRITE
	rowaux[isp][0][(x+pixelOffset)%width] = cols[ww+isp];
      }
    } // for whole row
    // and save it back to the same line
    for (int x=0; x < width; x++) {
      int ww = (width * y + x)*3;
      for (int isp=0; isp < planesCnt; isp++) {
	cols[ww+isp] = rowaux[isp][0][x];
      }
    } // for whole row
  } // for irow

  // deallocate the data needed for writing
  freemem3(rowaux, 0, planesCnt, 0, 1, 0, width);
    
  return;
}

void
CHDRImage::MakeMarkRGB(int mx, int my, EMarkType typeMark,
		       int marksize, float mult)
{
  int x, y;
  
  float r, g, b;
  if (typeMark == EE_BRDFSamples) {
    r = g = b = 255.0; // white
  }
  else {
    // ENVMAP samples
    r = 255.0; g = 5.0; b = 5.0; // red
  }
  // Make mark with correct color
  r *= mult;
  g *= mult;
  b *= mult;

  if (typeMark == EE_BRDFSamples) {
    // the horizontal direction
    if ((my >= 0) && (my < height)) {
      for (x = mx - marksize; x <= mx + marksize; x++) {
	if ((x < 0) || (x >= width))
	  continue;
	SetRGB_XY(x, my, r, g, b);
      }
    }
    if ((mx >= 0) && (mx < width)) {
      for (y = my - marksize; y <= my + marksize; y++) {
	if ((y < 0) || (y >= height))
	  continue;
	SetRGB_XY(mx, y, r, g, b);
      }
    }
  }
  else { // environment map samples
    int t;
    // from left bottom to right top corner
    for (t = - marksize; t <= marksize; t++) {
      x = mx + t;
      if ((x < 0) || (x >= width))
	continue;
      y = my + t;
      if ((y < 0) || (y >= height))
	continue;
      SetRGB_XY(x, y, r, g, b);
    } // for t
    // from left top to right bottom corner
    for (t = - marksize; t <= marksize; t++) {
      x = mx + t;
      if ((x < 0) || (x >= width))
	continue;
      y = my - t;
      if ((y < 0) || (y >= height))
	continue;
      SetRGB_XY(x, y, r, g, b);
    } // for t
  }

  return; // mark finished
}

// the whole image is filled in by the same color
void
CHDRImage::SetConstant(float r, float g, float b)
{
  // Copy the data from PNG representation
  for (int y=0; y < height; y++) {
    for (int x=0; x < width; x++) {
      int ww = (width * y + x)*3;
      // set it to the output format as required by RGBE_WRITE
      cols[ww+0] = r;
      cols[ww+1] = g;
      cols[ww+2] = b;
    } // for jcol
  } // for irow
  return;
}

// the whole image is powered by some values
void
CHDRImage::PowerFunction(float rexp, float gexp, float bexp)
{
  // Copy the data from PNG representation
  for (int y=0; y < height; y++) {
    for (int x=0; x < width; x++) {
      int ww = (width * y + x)*3;
      // set it to the output format as required by RGBE_WRITE
      cols[ww+0] = powf(cols[ww+0], rexp);
      cols[ww+1] = powf(cols[ww+1], gexp);
      cols[ww+2] = powf(cols[ww+2], bexp);
    } // for jcol
  } // for irow
  return;
}

int
CHDRImage::SetsPseudocolor(CHDRImage &i2, int pseudoColorTechnique,
			   float lowerBound, float upperBound) const
{
  int w2 = Min(i2.width, width);
  int h2 = Min(i2.height, height);
  //bool differ = false;
  if ((i2.width != width)||
      (i2.height != height)) {
    WARNING << "The size of image differ, selecting smaller sizes: "
	    << w2 << " x " << h2 << endl;
    //differ = true;
  }
  // Copy the data from PNG representation
  CColor RGB;
  for (int y=0; y < h2; y++) {
    for (int x=0; x < w2; x++) {
      int ww = (width * y + x)*3;
      float lum1 = Luminance(&(cols[ww+0])); // of this image
      GetPseudoColor(pseudoColorTechnique, lum1, lowerBound, upperBound, RGB);
      i2.SetValueChannel(x, y, 0, RGB[0]);
      i2.SetValueChannel(x, y, 1, RGB[1]);
      i2.SetValueChannel(x, y, 2, RGB[2]);
    } // for jcol
  } // for irow
  return false; // done
}

// Subtracts two images and prints possibly the output, that is created
// by this function
CHDRImage*
CHDRImage::Subtract(CHDRImage &i2, CHDRImage *i3, bool computeRMSE, int verbose) const
{
  int w2 = Min(i2.width, width);
  if (i3)
    w2 = Min(w2, i3->width);
  
  int h2 = Min(i2.height, height);
  if (i3)
    h2 = Min(h2, i3->height);

  if ((i2.width != width)||
      (i2.height != height)) {
    WARNING << "The size of image differ, selecting smaller sizes: "
	 << w2 << " x " << h2 << endl;
  }
  CHDRImage* ires = 0;
  if (i3 == 0) {
    ires = new CHDRImage();
    ires->Allocate(w2, h2);
  }
  else // we got the image
    ires = i3;
  
  long countPixels = w2 * h2;
  // the number of all different pixels
  long countDiffPixels = 0L;

  // the sum and square sum
  long double sumRGB = 0.0;
  long double sum2RGB = 0.0;
  long double sumLum = 0.0;
  long double sum2Lum = 0.0;
  // Copy the data from PNG representation
  for (int y=0; y < h2; y++) {
    long double sumRGBForScanline = 0;
    long double sumLumForScanline = 0;
    long double sumRGBSquaredForScanline = 0;
    long double sumLumSquaredForScanline = 0;
    for (int x=0; x < w2; x++) {
      int ww = (width * y + x)*3;
      int ww2 = (i2.width * y + x)*3;
      int wwres = (w2 * y + x)*3;
      float diffr = (cols[ww+0] - i2.cols[ww2+0]);
      float diffg = (cols[ww+1] - i2.cols[ww2+1]);
      float diffb = (cols[ww+2] - i2.cols[ww2+2]);
      ires->cols[wwres+0] = diffr;
      ires->cols[wwres+1] = diffg;
      ires->cols[wwres+2] = diffb;
      //------------------------
      if (computeRMSE) {
	float lum1 = Luminance(&(cols[ww+0]));
	float lum2 = Luminance(&(i2.cols[ww2+0]));
	sumRGBForScanline += fabs(diffr)+fabs(diffg)+fabs(diffb);
	sumRGBSquaredForScanline += diffr*diffr+diffg*diffg+diffb*diffb;
	sumLumForScanline += fabs(lum1-lum2);
	sumLumSquaredForScanline += sqr(lum1-lum2);
	const float epsilonLum = 1e-20f;      
	if ((lum1 - lum2) > epsilonLum)
	  countDiffPixels++;
      }
    } // for jcol
    if (computeRMSE) {
      sumRGB += sumRGBForScanline;
      sum2RGB += sumRGBSquaredForScanline;
      sumLum += sumLumForScanline;
      sum2Lum += sumLumSquaredForScanline;
    }
  } // for irow

  if (computeRMSE) {
    // compute root-mean-square error across the whole image
    long double RMSErrorLum = sqrt(sum2Lum / (long double)(countPixels));
    long double RMSErrorRGB = sqrt(sum2RGB / (long double)(countPixels));
    
    // compute standard deviation from all different pixels of the image
    long double devLum = 1.0 / (long double)countPixels * sum2Lum - sumLum * sumLum /
      ( (long double)countPixels * (long double)countPixels);
    long double devRGB = 1.0 / (long double)countPixels * sum2RGB - sumRGB * sumRGB /
      ((long double)countPixels / (long double)countPixels)*9.f;
    if (devRGB > 0.0)
      devRGB = sqrt(devRGB);
    else
      devRGB = 0.f;
    if (devLum > 0.0)
      devLum = sqrt(devLum);
    else
      devLum = 0.f;
    
    long double defChangedPixelsLum = 0.0;
    long double RMSErrorChangedPixelsLum = 0.0;
    long double defChangedPixelsRGB = 0.0;
    long double RMSErrorChangedPixelsRGB = 0.0;
    
    // compute RMSE and the standard deviation from all the pixels changed
    if (countDiffPixels) {
      defChangedPixelsLum = 1.0 / (long double)countDiffPixels * sum2Lum -
	sumLum * sumLum / (long double)countDiffPixels /
	(long double)countDiffPixels;
      if (defChangedPixelsLum > 0.f)
	defChangedPixelsLum = sqrt(defChangedPixelsLum);
      else
	defChangedPixelsLum = 0;
      defChangedPixelsRGB = 1.0 / (long double)countDiffPixels * sum2RGB -
	sumRGB * sumRGB / (9*(long double)countDiffPixels *
			   (long double)countDiffPixels);
      if (defChangedPixelsRGB > 0.f)
	defChangedPixelsRGB = sqrt(defChangedPixelsRGB);
      else
	defChangedPixelsRGB = 0;
      
      RMSErrorChangedPixelsLum = sqrt(sum2Lum / (long double)(countDiffPixels));
      RMSErrorChangedPixelsRGB = sqrt(sum2RGB / (long double)(9*countDiffPixels));
    }

    // computing PSNR - peak signal-to-reconstruced image measure
    long double PSNR = 20 * log10(255.0 / RMSErrorLum);
    long double PSNR_RGB = 20 * log10(255.0 / RMSErrorRGB);
    if (verbose) {
      STATS << "Comparing two images by RMSE (root-mean-squared) error"
	   << endl;
      STATS << "Percentage of changed pixels is "
	   << 100.0f * countDiffPixels / countPixels << " [%] ("
	   << countDiffPixels << " from cntAllPixels = " << countPixels
	   << ")" << endl;
      STATS << "For all pixels RMSE error[RGB,LUM] = "
	   << RMSErrorRGB * 100.0f << " " << RMSErrorLum * 100.0f << " [%] " << endl; 
      STATS << "For only changed pixels RMSE error = "
	   << RMSErrorChangedPixelsRGB * 100.0f << " " << RMSErrorChangedPixelsLum * 100.0f << " [%] " << endl; 
      STATS << "Standard deviation across whole image is = " << devRGB << " " << devLum << endl;
      STATS << "Standard deviation for changed pixels only image is = "
	   << defChangedPixelsRGB << " " << defChangedPixelsLum << endl;
      STATS << "For all pixels PSNR = " << PSNR_RGB << " " << PSNR << " [decibels] " << endl;
      STATS << "Main result from comparing two images = " << RMSErrorLum << endl;
    }
  }
  return ires; // the same size .. false, different size .. true
}


void
CHDRImage::CopyDataFromArray(int nwidth, int nheight, int channels,
			     float *buffer, int bufferwidth,
			     int xoffset, int yoffset,
			     bool flipVertically)
{
  // Copy the data from buffer
  for (int y=0; y < nheight; y++) {
    if (y+yoffset < 0)
      continue;
    if (y+yoffset >= height)
      break;
    for (int x=0; x < nwidth; x++) {
      if (x+xoffset < 0)
	continue;
      if (x+xoffset >= width)
	break;
      // index for storing data
      int si = (width * (y+yoffset) + x+xoffset);
      // index for reading data
      int ri;
      if (flipVertically) // flipping image vertically
	ri = (bufferwidth * (nheight - y) + x);
	  else
        ri = (bufferwidth * y + x);
      if ((channels == 3)||(channels==4)) {
	//indexes are the same, it can be either RGB or RGBA image
	// our image here has 3 channels
	// source buffer has 3 channels
	si *= 3;
	ri *= channels;
	cols[si] = buffer[ri];
	cols[si+1] = buffer[ri+1];
	cols[si+2] = buffer[ri+2];
      } else {
	// read index different to write index
	// our image here has 3 channels
	// source buffer has 1 channels
	si *= 3;
	ri *= channels;
	cols[si] = buffer[ri]; // copy the first channel to all three channels
	bool copyToAll = true;
	if (copyToAll) {
	  cols[si+1] = buffer[ri];
	  cols[si+2] = buffer[ri];
	}
	else {
	  if (channels == 2) {
	    // we have also the second channel
	    cols[si+1] = buffer[ri+1];	  
	  }
	  cols[si+2] = 0; // the last channel set to zero
	}
      } // channel = 1 or channel = 2
    } // for jcol
  } // for irow
  return;
}


// -------------------------------------------------------------
// Environment map

CEnvironmentMap::~CEnvironmentMap()
{
  delete []lums; lums = 0;
  delete []cumlums; cumlums = 0;
  // summing up columns in the horizontal direction in the second pass
  delete []cumcols; cumcols = 0;
  DeallocatePrecomputedLights();
}

CEnvironmentMap::CEnvironmentMap():
  CHDRImage(), lums(0), cumlums(0),
  cumcols(0),
  totalPower(0),
  totalPowerOrig(-100),
  envLightPositions(0),
  envLightColours(0),
  envLightPositionsBackup(0),
  envLightColoursBackup(0),
  envLightProbs(0),
  cntAllocatedLights(0),
  cntLights(0),
  cntDirsVDmax(0),
  randomizePrecomputedLights(false)
{
}

void
CEnvironmentMap::DeallocatePrecomputedLights()
{
  // data for the lights
  freemem2(envLightPositions, 0, cntAllocatedLights-1, 0, 2);
  envLightPositions = 0;
  freemem2(envLightPositionsBackup, 0, cntAllocatedLights-1, 0, 2);
  envLightPositionsBackup = 0;
  freemem2(envLightColours, 0, cntAllocatedLights-1, 0,2);
  envLightColours = 0;
  freemem2(envLightColoursBackup, 0, cntAllocatedLights-1, 0,2);
  envLightColoursBackup = 0;
  freemem1(envLightProbs, 0, cntAllocatedLights-1);
  envLightColoursBackup = 0;
  cntAllocatedLights = 0;
  cntLights = 0;
}

// deep copy constructor
CEnvironmentMap::CEnvironmentMap(const CEnvironmentMap &src):
  CHDRImage(src)
{
  ComputeLuminance();
  ComputeCDF();
  cntLights = src.cntAllocatedLights;
  if (cntLights) {
    AllocatePrecomputedLights(src.cntAllocatedLights);  
    // Let us normalize to the unit power from the whole sphere
    for(int i=0; i < cntLights; i++) {
      // Positions
      envLightPositions[i][0] = src.envLightPositions[i][0];
      envLightPositions[i][1] = src.envLightPositions[i][1];
      envLightPositions[i][2] = src.envLightPositions[i][2];
      // Positions backups
      envLightPositionsBackup[i][0] = src.envLightPositionsBackup[i][0];
      envLightPositionsBackup[i][1] = src.envLightPositionsBackup[i][1];
      envLightPositionsBackup[i][2] = src.envLightPositionsBackup[i][2];
      // Colors
      envLightColours[i][0] = src.envLightColours[i][0];
      envLightColours[i][1] = src.envLightColours[i][1];
      envLightColours[i][2] = src.envLightColours[i][2];
      // Probabality density for the light
      envLightProbs[i] = src.envLightProbs[i];
      // Make a backup of colours
      envLightColoursBackup[i][0] = src.envLightColoursBackup[i][0];
      envLightColoursBackup[i][1] = src.envLightColoursBackup[i][1];
      envLightColoursBackup[i][2] = src.envLightColoursBackup[i][2];
    } // no of lights
  }
}

void
CEnvironmentMap::AllocatePrecomputedLights(int cnt)
{
  cntAllocatedLights = cnt;
  envLightPositions = allocation2(0, cntAllocatedLights-1, 0, 2);
  assert(envLightPositions);
  envLightPositionsBackup = allocation2(0, cntAllocatedLights-1, 0, 2);
  assert(envLightPositionsBackup);
  envLightColours = allocation2(0, cntAllocatedLights-1, 0, 2);
  assert(envLightColours);
  envLightColoursBackup = allocation2(0, cntAllocatedLights-1, 0, 2);
  assert(envLightColoursBackup);
  envLightProbs = allocation1(0, cntAllocatedLights-1);
  assert(envLightProbs);
}  

void
CEnvironmentMap::SetFunctionConstNorthHemisphere(float r, float g, float b)
{
  // Copy the data from PNG representation
  for (int y=0; y < height; y++) {
    float theta = (float)YtoTheta(float(y));
    float cosTheta = fabs(cos(theta))+1e-6f;
    if (theta <= M_PI/2.f) {
      assert(cosTheta >= 0.f);
      for (int x=0; x < width; x++) {
	int ww = (width * y + x)*3;
	// set it to the output format as required by RGBE_WRITE
	cols[ww+0] = r;
	cols[ww+1] = g;
	cols[ww+2] = b;
      } // for jcol
    } // if north hemisphere
    else {
      // South hemisphere set to almost zero value
      const float almostZero = 1e-20f;
      for (int x=0; x < width; x++) {
	int ww = (width * y + x)*3;
	// set it to the output format as required by RGBE_WRITE
	cols[ww+0] = almostZero*cosTheta;
	cols[ww+1] = almostZero*cosTheta;
	cols[ww+2] = almostZero*cosTheta;
      } // for x
    } // if south hemisphere
  } // for y
  return;
}

void
CEnvironmentMap::SetFunctionConstSouthHemisphere(float r, float g, float b)
{
  // Copy the data from PNG representation
  for (int y=0; y < height; y++) {
    float theta = (float)YtoTheta(float(y));
    assert(fabs(cos(theta))+1e-6 >= 0.f);
    if (theta > M_PI/2.f) {
      // if north hemisphere
      //const float almostZero = 1e-20;
      for (int x=0; x < width; x++) {
	int ww = (width * y + x)*3;
	// set it to the output format as required by RGBE_WRITE
	cols[ww+0] = r;
	cols[ww+1] = g;
	cols[ww+2] = b;
      } // for jcol
    } 
    else {
      // if north hemisphere - do nothing
    }
  } // for y
  return;
}

// Compute luminance from color values
void
CEnvironmentMap::ComputeLuminance()
{
  delete []lums;
  lums = new float[height * width];
  assert(lums);

  const int maxindex = 3 * width * height;
  // There is no multiplication by sin(theta) here !
  for (int h = 0; h < height; h++) {
    for (int w = 0; w < width; w++) {
      int wl = h*width+w;
      int wc = wl*3;
      assert(wc+2 < maxindex);
      float lum = Luminance(&(cols[wc]));
      lums[wl] = lum; // set the luminance
    } // w
  } // h

  return;
}

// Normalize the power of EM map so the integral is 1 Watt.
void
CEnvironmentMap::NormalizePower()
{
  if (fabs(totalPower - 1.0f) < 1e-2) {
    // the power of EM was already normalized or is accidentally 1.0
    return;
  }
  // if luminance was not computed, do it now
  if (lums == 0)
    ComputeLuminance();
  int maxIterations = 25;
  for (int i = 0; i < maxIterations; i++) {
    ComputeTotalPower();
    assert(totalPower > 0.f);
    float mult = 1.0f/float(totalPower);
    // normalize the values so the totalPower = 1.0
    const int maxIndex = width*height*3;
    for (int j = 0; j < maxIndex; j++)
      cols[j] *= mult;
    // compute the luminance and reevaluate the power
    ComputeLuminance();
    ComputeTotalPower();
    if (fabs(totalPower - 1.0f) < 1e-3) {
      // Environment map was successfully normalized in "i+1" iterations to unit power
      return;
    }
    else {
      STATS << "Current result in the implementation of normalization of total power " << endl;
      STATS << "totalPower = " << totalPower << endl;
    }
  } // for i
  FATAL << "Environment map was normalized in " << maxIterations
	<< " iterations to power = " << totalPower << endl;
  FATAL_ABORT; // Why this happens ?
  return;
}


void
CEnvironmentMap::ComputeTotalPower()
{
#define DETERMINISTIC
#ifdef DETERMINISTIC
  // There is a bug in this code, when integrating 1 over the sphere,
  // we are not getting 4*PI, Vlastimil Havran, 2013-09-10
  
  // Fast version with precomputation of sin and cos arrays
  int iphi, itheta;
  double a1, a2, b1, b2, dp, dt;//, aa, bb;

#ifdef _COMPUTE_MAXINTENSITY
  // the information to be computed is initialized
  float _maximum_d_INTENSITY = -1.0;
#endif  
  this->totalPower = 0.0;

  // Precompute sinus and cosinus for theta
  float *sinDT = new float[height];
  assert(sinDT);
  long double sum = 0.;
  // precompute sine and cosine of theta
  for (itheta = 0; itheta < height ; ++itheta)
  {
    float theta = (float)M_PI*(itheta+0.5f)/height;
    sinDT[itheta] = sin(theta);
    //cosDT[itheta] = cos(theta);
  } // for itheta
  
  // for all patches on the goniometric web
  for (iphi = 0; iphi < width; ++iphi) {
    for (itheta = 0; itheta < height - 1; ++itheta) {
      // the four values for given goniometric web diagram patch
      int iphiRight = (iphi + 1) % width;
      // The densities for the four corners of the goniometric patch
      a1 = lums[AddrLum(iphi, itheta)];
      a2 = lums[AddrLum(iphiRight, itheta)];
      b1 = lums[AddrLum(iphi, itheta)];
      b2 = lums[AddrLum(iphiRight, itheta+1)];
#ifdef _COMPUTE_MAXINTENSITY
      // Find out the maximum for statistic purposes
      dt = a1;
      if (a2 > dt)
	dt = a2;
      if (b1 > dt)
	dt = b1;
      if (b2 > dt)
	dt = b2;
      if (dt > _maximum_d_INTENSITY)
        _maximum_d_INTENSITY = dt;
#endif      
      // delta theta
      dt = M_PI/(float)height;
      // deltha phi
      dp = 2.f*M_PI/(float)width;
      // for a single patch inaccurate computation - we average the intensities
      // and multiply the result by the area of the patch dt*dp and also by sin(theta)
      double deltaPower = sinDT[itheta]*(a1+a2+b1+b2)*0.25*dt*dp;
      assert(deltaPower >= 0.f);
      sum += deltaPower;
    } // for itheta
  } // for iphi
  totalPower = sum;
  delete []sinDT;
#else
  // RANDOM evaluation
  int cnt = 1000000;
  long double sum = 0.;
  float lum;
  for(int i = 0; i < cnt; i++) {
    //Generate random direction on unit sphere with density prob(theta) = 1/(2*PI).
    CVector3D result;
    // constant density on surface of sphere
    result = RND::sph_const(); 
    //result = RND::sph_const(rndvalue);
    float theta, phi;
    ConvertXYZtoThetaPhi(result, theta, phi); // in radians
    CColor RGB;
    this->GetRGB_ThetaPhi(theta, phi, RGB);
    float lum = Luminance(RGB);
    if ((lum < 0.f)||(isnan_local(lum))) {
      FATAL << "sum = " << sum << endl;
      FATAL << "Problem = " << lum << endl;
      FATAL_ABORT;
    }
    sum += (long double)lum;
  }
  sum /= (long double) cnt;
  const float SAHEM = 4.0f*M_PI; // the surface area of the sphere
  totalPower = SAHEM * sum;
#endif

  if (fabs(totalPowerOrig+100.f) < 1e-5) {
    // save original total power prior to normalization only once
    totalPowerOrig = totalPower;
  }
  
  totalPowerInv = 1.0f/float(totalPower);
  return;
}

// Compute CDF from the luminance, luminance has to be set
bool
CEnvironmentMap::ComputeCDF()
{
  STATUS << "Starting to compute CUM image " << std::endl;
  cumlums = new double[height * width];
  assert(cumlums);

  // as in PBRT book
  assert(cumcols == 0);
  cumcols = new double[width+2];
  assert(cumcols);
  // The first rows are summed up in horizontal direction,
  // each individually, this is the first row

  // center of the pixel in the first column and row
  float mult = sin((0.5f+(float)0)*float(M_PI)/((float)height));
  cumlums[0] = lums[0] * mult;
  // computing first column over all height
  for (int h = 1; h < height; h++) {
    mult = sin((0.5f+(float)h)*float(M_PI)/((float)height));
    int hi = AddrCum(0, h);
    int hi2 = AddrCum(0, h-1);
    int li = AddrLum(0, h);
    cumlums[hi] = cumlums[hi2] + lums[li] * mult;
  } // w
  // the first value to the vertical direction
  int hi = AddrCum(0, height-1);
  cumcols[0] = cumlums[hi];
  assert(cumcols[0] >= 0.f);

  // all other rows of an environment image
  for (int w = 1; w < width; w++) {
    int wi = AddrCum(w, 0);
    int li = AddrLum(w, 0);
    mult = sin((0.5f+(float)0)*float(M_PI)/((float)height));
    cumlums[wi] = lums[li] * mult;
    // a single row summing up
    for (int h = 1; h < height; h++) {
      // for the center of the pixel
      mult = sin((0.5f+(float)h)*float(M_PI)/((float)height));
      // this is conditional probability for the first dimension
      wi = AddrCum(w, h);
      int wi2 = AddrCum(w, h-1);
      li = AddrLum(w, h);
      cumlums[wi] = cumlums[wi2] + mult * lums[li];
    } // w
    // This corresponds to joint probability for the second dimension
    wi = AddrCum(w, height-1);
    cumcols[w] = cumcols[w-1] + cumlums[wi];
    assert(cumcols[w] >= 0.f);
  } // h
  //STATS << "cumcols[width-1]= " << cumcols[width-1] << endl;

  // finally, compute total power from EM
  ComputeTotalPower();
  return false;
}


bool
SingleBinSearch(double list[], int n, float target, // input
		int &nindex, float &alpha) // output
{
  if (target < list[0]) {
    nindex = 0;
    alpha = target/float(list[0]+1e-6);
    return false; // finished
  }
  if (target > list[n-1]) {
    return true; // cannot be found
  }
  //declare low, mid, and high
  int low = 0, mid = 0, high = n-1;
  for (int i = low; i < high; i++) {
    if (list[i] > list[i+1]) {
      FATAL << "A The array for binary search is not sorted" << endl;
      FATAL_ABORT;
    }
  }
  //binary search loop
  while(low <= high) {
    //compute mid
    mid = (low + high) / 2;
    //check target against list[mid]
    if (target < list[mid]) {
      //if target is below list[mid] reset high
      high = mid-1;
    }
    else if (target > list[mid]) {
      //if target is below list[mid]) reset low
      low = mid+1;
    }
    else {
      //if target is found set low to exit the loop
      low = mid;
      high = mid-1;
    }
  } // while
  // This condition below is necessary
  if (list[mid] > target)
    mid--;
  
  assert(0 <= mid);
  assert(mid < n);
  assert(list[mid] <= target);
  assert(target <= list[mid+1]);
  if (mid < n-1) {
    nindex = mid;
    alpha = float((target - list[mid])/(list[mid+1] - list[mid]));
    assert(alpha >= 0);
    assert(alpha <= 1.f);
    return false; // OK
  }

  nindex = n-2;
  alpha = 1;
  return false; // OK
}

bool
DoubleBinSearch(double list1[], double list2[], // input two arrays with float values
  float alpha, int n, float target, // input alpha for mixing, number of values, search value
  int &nindex, float &beta) // output
{
  float valueMin = float(list1[0]*(1.f-alpha)+list2[0]*alpha);
  if (target < valueMin) {
    nindex = 0;
    alpha = target/(valueMin+1e-6f);
    return false; // OK
  }
  float valueMax = float(list1[n-1]*(1.f-alpha)+list2[n-1]*alpha);
  if (valueMax < target) {
    WARNING << "A target = " << target << " valMax = " << valueMax << endl;
    return true; // cannot be found
  }

  //declare low, mid, and high
  int low = 0, mid = 0, high = n-1;
 
  // Debugging code for input array
  for (int i = low; i < high; i++) {
    if (list1[i] > list1[i+1]) {
      FATAL << "B the array for binary search is not sorted" << endl;
      FATAL_ABORT;
    }
    if (list2[i] > list2[i+1]) {
      FATAL << "C the array for binary search is not sorted" << endl;
      FATAL_ABORT;
    }
  }
  
  //binary search loop
  while (low <= high) {
    //compute mid
    mid = (low + high) / 2;
    //check target against list[mid]
    float value = float(list1[mid]*(1.f-alpha)+list2[mid]*alpha);
    if (target < value)
      //if target is below list[mid] reset high
      high = mid-1;
    else if (target > value)
      //if target is below list[mid]) reset low
      low = mid+1;
    else {
      //if target is found set low to exit the loop
      low = mid;
      high = mid-1;
    }
  } // while
  // This condition below is necessary
  if (list1[mid]*(1.f-alpha)+list2[mid]*alpha > target)
    mid--;
  
  float value = float(list1[mid]*(1.f-alpha)+list2[mid]*alpha);
  float value2 = float(list1[mid+1]*(1.f-alpha)+list2[mid+1]*alpha);
  assert(0 <= mid);
  assert(mid < n);
  assert(value <= target);

  if (mid < n-1) {
    nindex = mid;
    assert(target <= value2);
    beta = (target - value)/(value2-value+1e-30f);
    assert(beta >= 0.f);
    assert(beta <= 1.f);
    return false; // OK
  }
  
  nindex = n-2;
  beta = 1.f;
  return false; // OK
}

// get value RGB for a particular theta and phi, when we assume
// that the HDR image is the environment map mapped to a sphere
bool
CEnvironmentMap::ImportanceSamplingXY(float rnd1, float rnd2, // input
				      float &X, float &Y, // output position in the image in pixels
				      CColor &RGB) const
{
  assert(0 <= rnd1); assert(rnd1 <= 1);
  assert(0 <= rnd2); assert(rnd2 <= 1);

  // First search to determine X
  // first we search along phi
  // theta = 0 .. north pole
  // theta = PI .. south pole
  float valueSearch = float(rnd1 * cumcols[width-1]);
  int x = 0; // index in range 0 ... height-2
  float alphaX = 0.f; // value in range 0 ... 1
  bool res1 =
    SingleBinSearch(cumcols, width, valueSearch, x, alphaX);
  assert(res1 == false);
  X = (float)(x+alphaX);
  int x2 = (x+1)%width;
  assert(0 <= x);
  assert(x < width);
  // Then search in two particular columns to determine Y
  float valueSearch2 = float(rnd2 * (cumlums[AddrCum(x, height-1)]*(1.f-alphaX) + 
			       cumlums[AddrCum(x2, height-1)]*alphaX));
  int y = 0; // index in range 0 ... height-2
  float betaY = 0.f; // value in range 0 ... 1

  bool res2 =
    DoubleBinSearch(&(cumlums[AddrCum(x, 0)]), &(cumlums[AddrCum(x2,0)]),
		    alphaX, height, valueSearch2, y, betaY);
  assert(res2 == false);
  assert(0 <= y);
  assert(y < height-1);
  Y = (float)y + betaY;
  assert(Y >= 0.f);
  assert(Y < height);
  int y2 = (y+1);
  if (y2 >= height) y2 = height;

  // The colour is taken from the particular pixel
  // by interpolation from the 4 neighboring pixels
  
  for (int i = 0; i < 3; i++) {
    RGB[i] = cols[(width*y + x)*3 +i]*(1.f-betaY)*(1.f-alphaX)
      + cols[(width*y + x2)*3 + i]*(1.f-betaY)*alphaX
      + cols[(width*y2 + x)*3 + i]*betaY*(1.f - alphaX)
      + cols[(width*y2 + x2)*3 + i]*betaY*alphaX;
  } // for i

  float lum = RGB.Luminance()+1e-20f;
  assert(lum > 0.f);
  float norm = float(1.0f/lum * totalPower);
  for (int i = 0; i < 3; i++) {
    RGB[i] *= norm;
  }

  // now the color is of unit importance(=luminance)
  return false;
}

bool
CEnvironmentMap::SaveDebugHDR(const string &fileName, int mode, float mult)
{
  CHDRImage img;
  img.Allocate(width, height);

  if (mode == 0) {
    assert(cols);
    int nmax = 3*width*height;
    // saving color image
    int ii=0;
    for (int h = 0; h < height; h++) {
      ii = width * h * 3;
      for (int w = 0; w < width; w++) {
	float red = mult * cols[ii]; ii++;
	float green = mult * cols[ii]; ii++;
	assert(ii  < nmax);
	float blue = mult * cols[ii]; ii++;
	img.SetRGB_XY(w, h, red, green, blue);
      } // w
    } // h
    img.SaveHDR(fileName);
    return false;
  }
  if (mode == 1) {
    assert(lums);
    // saving luminance image
    int ii=0;
    for (int h = 0; h < height; h++) {
      for (int w = 0; w < width; w++) {
	float lum = mult * lums[ii++]; 
	img.SetRGB_XY(w, h, lum, lum, lum);
      } // w
    } // h
    img.SaveHDR(fileName);
    return false;
  }
  if (mode == 2) {
    assert(cumlums);
    // saving cum sum of luminance image
    int ii=0;
    for (int h = 0; h < height; h++) {
      for (int w = 0; w < width; w++) {
	float val = float(mult * cumlums[ii++]); 
	img.SetRGB_XY(w, h, val, val, val);
      } // w
    } // h
    img.SaveHDR(fileName);
    return false;
  }
  FATAL << "ERROR: unknown mode for saving image" << std::endl;
  return true;
}

// get value RGB for a particular theta and phi, when we assume
// that the HDR image is the environment map mapped to a sphere
bool
CEnvironmentMap::ImportanceSamplingThetaPhi(float rnd1, float rnd2, // input
					    float &theta, float &phi, // output
					    CColor &RGB, float &prob) const
{
  float X = 0.f, Y = 0.f;
  // sets X, Y, and RGB
  bool res = ImportanceSamplingXY(rnd1, rnd2, X, Y, RGB);
  assert(res == false);
  // now convert X/Y of the image to theta/phi
  phi = XtoPhi(X); // 0 to 2*PI
  assert(0.f <= phi);
  assert(phi <= 2.f*M_PI);
  theta = YtoTheta(Y); // 0 to PI

  prob = ComputeProb(X, Y, theta, RGB);
  return false; // OK
}

// precompute cnt lights
void
CEnvironmentMap::PrecomputeLights(int cnt)
{
  if (cnt > cntAllocatedLights) {
    DeallocatePrecomputedLights();
    AllocatePrecomputedLights(cnt);
  }
  cntLights = cnt;
  
  // Precomputed lights on the fly and store them to the array
  float sumProb = 0.f;
  static CColor RGB;
  static CVector3D XYZ;
  // Let us normalize to the unit power from the whole sphere
  for(int i=0; i < cntLights; i++) {
    float rnd1 = RandomValue();
    float rnd2 = RandomValue();
    float theta, phi, prob; // X, Y
    ImportanceSamplingThetaPhi(rnd1, rnd2, theta, phi, RGB, prob);
    ConvertThetaPhiToXYZ(theta, phi, XYZ);
    //environMap[em].ImportanceSamplingXY(rnd1, rnd2, X, Y, RGB);	  
    //environMap[em].MakeMark(X, Y, EE_BRDFSamples, 10, 100.f);    
    // Positions
#ifdef XYZCOORSYSTEM
    // Z-axis is in the direction of north-pole
    // plane XY corresponds to theta=PI/2 and phi<0,2PI)
    envLightPositionsBackup[i][0] = envLightPositions[i][0] = XYZ[0];
    envLightPositionsBackup[i][1] = envLightPositions[i][1] = XYZ[1];
    envLightPositionsBackup[i][2] = envLightPositions[i][2] = XYZ[2];
#else
    // Y-axis is in the direction of north-pole
    // plane XZ corresponds to theta=PI/2 and phi<0,2PI)
    envLightPositionsBackup[i][0] = envLightPositions[i][0] = XYZ[0]; // X axis remains
    envLightPositionsBackup[i][1] = envLightPositions[i][1] = XYZ[2]; // Z axis become Y
    envLightPositionsBackup[i][2] = envLightPositions[i][2] = XYZ[1]; // Y axis becomes X
#endif    
    // Colors
    envLightColours[i][0] = RGB[0];
    envLightColours[i][1] = RGB[1];
    envLightColours[i][2] = RGB[2];
    // Probabality density for the light
    envLightProbs[i] = prob;
    // Make a backup of colours
    envLightColoursBackup[i][0] = envLightColours[i][0];
    envLightColoursBackup[i][1] = envLightColours[i][1];
    envLightColoursBackup[i][2] = envLightColours[i][2];
    
    sumProb += prob; // adding the probabilities
  } // no of lights
  STATS << "sumProb = " << sumProb/float(cntLights) << endl;

  //exit(0);
  STATUS << "The directional lights were computed." << endl;

  return;
}

void
CEnvironmentMap::MakeLighter(int i, float c1, float c2)
{
  float lum = Luminance(envLightColours[i]);
  for (int j = 0; j < 3; j++)
    envLightColours[i][j] = lum * c1 + envLightColoursBackup[i][j] * c2;
}

// It rotates only the directional lights, not the image!
void 
CEnvironmentMap::RotateEnvironmentLights(float angle)
{
  STATUS << "Rotate EL angle = " << angle << endl;
  
  float sv[2];
  // ... rotation of whole environment lights by angle
  float sinAngle = sin(angle);
  float cosAngle = cos(angle);
  // here we access direction directly to arrays, no randomization applies
  for(int i=0; i < cntLights; i++) {
#ifdef XYZCOORSYSTEM
    // Z-axis is in the direction of north-pole
    // plane XY corresponds to theta=PI/2 and phi<0,2PI)
    const int Xi=0, Yi = 1;
#else
    // Y-axis is in the direction of north-pole
    // plane XZ corresponds to theta=PI/2 and phi<0,2PI)
    const int Xi=0, Yi = 2;
#endif
    sv[0] = cosAngle * envLightPositionsBackup[i][Xi] - sin(angle) * envLightPositionsBackup[i][Yi];
    sv[1] = sinAngle * envLightPositionsBackup[i][Xi] + cos(angle) * envLightPositionsBackup[i][Yi];
    envLightPositions[i][Xi] = sv[0];
    envLightPositions[i][Yi] = sv[1];
  } // for i
  return;
}//--- rotateEnvironmentLights ---------------------------------------------

// get the color when indexed as image
inline bool
CEnvironmentMap::SetLUM_XY(int x, int y, float val)
{
  if ((x < 0)||(x >= width)) return true; // error
  if ((y < 0)||(y >= height)) return true; // error
  int itt = (width*y+x);
  lums[itt] = val;
  return false;
}

// make mark in luminance channel
void
CEnvironmentMap::MakeMarkLUM(int mx, int my, EMarkType typeMark,
			     int marksize, float mult)
{
  int x, y;
  float val;
  if (typeMark == EE_BRDFSamples) {
    val = 255.0; // white
  }
  else {
    // ENVMAP samples
    val = 255.0;
  }
  // Make mark with correct color
  val *= mult;
  if (typeMark == EE_BRDFSamples) {
    // the horizontal direction
    if ((my >= 0) && (my < height)) {
      for (x = mx - marksize; x <= mx + marksize; x++) {
	if ((x < 0) || (x >= width))
	  continue;
	SetLUM_XY(x, my, val);
      }
    }
    if ((mx >= 0) && (mx < width)) {
      for (y = my - marksize; y <= my + marksize; y++) {
	if ((y < 0) || (y >= height))
	  continue;
	SetLUM_XY(mx, y, val);
      }
    }
  }
  else {
    int t;
    // from left bottom to right top corner
    for (t = - marksize; t <= marksize; t++) {
      x = mx + t;
      if ((x < 0) || (x >= width))
	continue;
      y = my + t;
      if ((y < 0) || (y >= height))
	continue;
      SetLUM_XY(x, y, val);
    } // for t
    // from left top to right bottom corner
    for (t = - marksize; t <= marksize; t++) {
      x = mx + t;
      if ((x < 0) || (x >= width))
	continue;
      y = my - t;
      if ((y < 0) || (y >= height))
	continue;
      SetLUM_XY(x, y, val);
    } // for t
  }

  return; // mark finished
}

void
CEnvironmentMap::SetFunctionCosineTheta(float r, float g, float b)
// for the whole sphere!
{
  // Copy the data from PNG representation
  for (int y=0; y < height; y++) {
    float theta = YtoTheta(float(y));
    float cosTheta = fabs(cos(theta)); // absolute value
    for (int x=0; x < width; x++) {
      int ww = (width * y + x)*3;
      // set it to the output format as required by RGBE_WRITE
      cols[ww+0] = r*cosTheta;
      cols[ww+1] = g*cosTheta;
      cols[ww+2] = b*cosTheta;
    } // for jcol
  } // for irow
  return;
}

void
CEnvironmentMap::SetFunctionCosineThetaNorthHemisphere(float r, float g, float b)
{
  // Copy the data from PNG representation
  for (int y=0; y < height; y++) {
    float theta = YtoTheta(float(y));
    float cosTheta = fabs(cos(theta))+1e-6f;
    if (theta <= M_PI/2.f) {
      assert(cosTheta >= 0.f);
      for (int x=0; x < width; x++) {
	int ww = (width * y + x)*3;
	// set it to the output format as required by RGBE_WRITE
	cols[ww+0] = r*cosTheta;
	cols[ww+1] = g*cosTheta;
	cols[ww+2] = b*cosTheta;
      } // for jcol
    } // if north hemisphere
    else {
      // South hemisphere set to almost zero value
      const float almostZero = 1e-20f;
      for (int x=0; x < width; x++) {
	int ww = (width * y + x)*3;
	// set it to the output format as required by RGBE_WRITE
	cols[ww+0] = almostZero*cosTheta;
	cols[ww+1] = almostZero*cosTheta;
	cols[ww+2] = almostZero*cosTheta;
      } // for x
    } // if south hemisphere
  } // for y
  return;
}

void
CEnvironmentMap::SetFunctionCosineThetaPowNorthHemisphere(float r, float g, float b, float p)
{
  // Copy the data from PNG representation
  for (int y=0; y < height; y++) {
    float theta = YtoTheta(float(y));
    float cosTheta = fabs(cos(theta))+1e-6f;
    if (theta <= M_PI/2.f) {
      assert(cosTheta >= 0.f);
      for (int x=0; x < width; x++) {
	int ww = (width * y + x)*3;
	float v = powf(cosTheta, p);
	cols[ww+0] = r * v;
	cols[ww+1] = g * v;
	cols[ww+2] = b * v;
      } // for jcol
    } // if north hemisphere
    else {
      // South hemisphere set to almost zero value
      const float almostZero = 1e-20f;
      for (int x=0; x < width; x++) {
	int ww = (width * y + x)*3;
	cols[ww+0] = almostZero;
	cols[ww+1] = almostZero;
	cols[ww+2] = almostZero;
      } // for x
    } // if south hemisphere
  } // for y
  return;
}

void
CEnvironmentMap::SetFunctionCosineThetaPowSphere(float r, float g, float b, float p)
{
  // Copy the data from PNG representation
  for (int y=0; y < height; y++) {
    float theta = YtoTheta(float(y));
    float cosTheta = fabs(cos(theta))+1e-6f;
    assert(cosTheta >= 0.f);
    for (int x=0; x < width; x++) {
      int ww = (width * y + x)*3;
      float v = powf(cosTheta, p);
      cols[ww+0] = r * v;
      cols[ww+1] = g * v;
      cols[ww+2] = b * v;
    } // for jcol
  } // for y
  return;
}

void
CEnvironmentMap::SetFunction2(float r, float g, float b)
{
  // Copy the data from PNG representation
  for (int y=0; y < height; y++) {
    float theta = YtoTheta(float(y));
    float value = fabs(sin(theta))+1e-6f;
    for (int x=0; x < width; x++) {
      int ww = (width * y + x)*3;
      // set it to the output format as required by RGBE_WRITE
      cols[ww+0] = r*value;
      cols[ww+1] = g*value;
      cols[ww+2] = b*value;
    } // for jcol
  } // for irow
}

void
CEnvironmentMap::SetFunction3(float r, float g, float b)
{
  // Copy the data from PNG representation
  for (int y=0; y < height; y++) {
    float theta = YtoTheta(float(y));
    float value = fabs(sin(theta))+1e-6f;
    for (int x=0; x < width; x++) {
      float v1 = fabs(sin(20*float(x)/width));
      float v2 = fabs(sin(21*float(x)/width));
      float v3 = fabs(sin(theta+x/width*14.f)+0.1f);
      int ww = (width * y + x)*3;
      // set it to the output format as required by RGBE_WRITE
      cols[ww+0] = r*value*v1;
      cols[ww+1] = g*value*v2;
      cols[ww+2] = b*value*v3;
    } // for jcol
  } // for irow
}

float
CEnvironmentMap::ComputeProb(float x, float y, float theta, CColor &/*RGB*/) const
{
  // assumes correct value of RGB set by a previously called function
  float sinTheta = Max(fabs(sin(theta)), 1e-20f); // this small constant must be here to avoid division by zero
  // as in PBRT - the columns are integrated first after multiplication by sin(theta)
  // the integrated columns are added to a single row
  int iy = (int)y;
  int iy2 = iy+1;
  if (iy2 >= height-1) iy2=height-1;
  float betaY = y-(float)iy;
  int ix = (int)x;
  int ix2 = (ix+1)%width;
  float alphaX = x-(float)ix;
  float fprob[2];
  // in X - interpolation between two columns from cumcols - this is easy
  fprob[0] = float(width)*float(cumlums[AddrCum(ix, height-1)]*(1.f-alphaX) + cumlums[AddrCum(ix2, height-1)]*alphaX)
    / float(cumcols[width-1]);

  // in Y - interpolation between two columns - marginal probability interpolated
  fprob[1] = float(height) *
    float((lums[iy*width+ix]*(1.f-alphaX) + lums[iy*width+ix2]*alphaX)*(1.f-betaY) +
     (lums[iy2*width+ix]*(1.f-alphaX) + lums[iy2*width+ix2]*alphaX)*betaY)
    / float(cumlums[AddrCum(ix,height-1)]*(1.f-alphaX) + cumlums[AddrCum(ix2,height-1)]*alphaX);
  //  float sinTheta = sin(theta)+1e-31;
  assert(sinTheta > 0.f);
  float prob = fprob[0]*fprob[1]*sinTheta/float(M_PI);
  return prob;
}

// get value RGB for a particular theta and phi, when we assume
// that the HDR image is the environment map
bool 
CEnvironmentMap::GetRGB_ThetaPhi(float theta, float phi, CColor &RGB) const
{
  assert(0.f <= phi);
  assert(phi <= 2.f*float(M_PI));
  assert(0.f <= theta);
  assert(theta <= float(M_PI));
  float X, Y;
  X = PhiToX(phi);
  Y = ThetaToY(theta);
  int xx = int(X);
  int xx2 = (xx+1)%width;
  float alpha = X - (float)xx;
  int yy = int(Y);
  assert(yy < height);
  int yy2 = yy+1;
  if (yy2 >= height) yy2 = yy;
  float beta = Y - (float)yy;
  // interpolated value for all 3 channels
  for (int isp = 0; isp < 3; isp++) {
    RGB[isp] = GetValueChannel(xx, yy, isp) * (1.f-alpha) * (1.f-beta) +
      GetValueChannel(xx2, yy, isp) * alpha * (1.f-beta) +
      GetValueChannel(xx, yy2, isp) * (1.f-alpha) * beta +
      GetValueChannel(xx2, yy2, isp) * alpha * beta;
  } // for isp
  return false;
}

// get value RGB for a particular theta and phi, when we assume
// that the HDR image is the environment map
float
CEnvironmentMap::GetRGBandProb_ThetaPhi(float theta, float phi, CColor &RGB) const
{
  // Sets X,Y, and RGB
  GetRGB_ThetaPhi(theta, phi, RGB);
  float X, Y;
  X = PhiToX(phi);
  Y = ThetaToY(theta);
  float prob = ComputeProb(X, Y, theta, RGB);
  return prob;
}

float
CEnvironmentMap::GetProb_ThetaPhi(float theta, float phi) const
{
  // Sets X,Y, and RGB
  CColor RGB;
  GetRGB_ThetaPhi(theta, phi, RGB);
  float X, Y;
  X = PhiToX(phi);
  Y = ThetaToY(theta);
  float prob = ComputeProb(X, Y, theta, RGB);
  return prob;
}

__END_GOLEM_SOURCE

