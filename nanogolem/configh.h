// ================================================================
//
// configh.h
//      Global configuration things for header files.
//
// This file must be first included into each headerfile.h !!!!!!!!!
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, 2000.

#ifndef __CONFIGH_H__
#define __CONFIGH_H__

#ifndef USE_GOLEM_NAMESPACE
// without namespaces
#define  __BEGIN_GOLEM_HEADER
#define  __END_GOLEM_HEADER
#else
// with namespace 
#define NAMESPACEDEF     GOLEMLIB
// use of namespace commands inside the library sources
#define  __BEGIN_GOLEM_HEADER namespace NAMESPACEDEF { using namespace NAMESPACEDEF;
#define  __END_GOLEM_HEADER
#endif

__BEGIN_GOLEM_HEADER

// definition of numeric data types in the library

// floating point type (min. 4 bytes, IEEE754 single)
typedef float float4;
// floating point type (min.  8 bytes, IEEE754 double)
typedef double float8;
// floating point type (min. 12 bytes, IEEE854 extended)
typedef long double float12;

// unsigned byte type (exactly 1 byte)
typedef unsigned char byte;
// unsigned integer type (min. 1 byte)
typedef unsigned char uint1;
// signed integer type (min. 1 byte)
typedef signed char int1;

// unsigned integer type (min. 2 bytes)
typedef unsigned short int uint2;
// signed integer type (min. 2 bytes)
typedef signed short int int2;

// unsigned integer type (min. 4 bytes)
typedef unsigned int uint4;
// signed integer type (min. 4 bytes)
typedef signed int int4;

// signed integer type (min. 8 bytes)
// -- see file int8.h

#ifdef _MSC_VER
#ifdef min
#undef min
#endif
#ifdef max
#undef max
#endif
#ifdef ERROR
#undef ERROR
#endif
#endif // _MSC_VER
 
#ifndef _EXPORT_
#if defined(_MSC_VER)
#define _EXPORT_  __declspec(dllexport)
#else
#define _EXPORT_ 
#endif
#endif

// For 64-bit architectures
#define _IA64

__END_GOLEM_HEADER

#endif // __CONFIGH_H__

