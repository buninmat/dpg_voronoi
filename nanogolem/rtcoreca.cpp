// ===================================================================
//
// rtcoreca.cpp
//             The core of ray tracer algorithms with camera
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, 1999

// standard headers
#include <iomanip>
#include <cstdio>
#include <chrono>

// nanoGOLEM headers
#include "configg.h"
#include "basstr.h"
#include "colorfs.h"
#include "dotcount.h"
#include "errorg.h"
#include "rtcoreca.h"
#include "scene.h"
#include "world.h"

__BEGIN_GOLEM_SOURCE

// constructor
CRT_CoreWithCamera_App::CRT_CoreWithCamera_App() :
  CRT_Core_App(), _image(0), _imageVis(0)
{

}

CRT_CoreWithCamera_App::~CRT_CoreWithCamera_App()
{
  CleanUp();
}

bool
CRT_CoreWithCamera_App::Init(CWorld *world, string &outputName)
{
  if (CRT_Core_App::Init(world, outputName))
    return true; // some error

  bool initImage;
  _environment->GetBool("RTCore.initImage", initImage);
  if (initImage) {
    int iwidth, iheight;
    _environment->GetInt("SetCamera.imageWidth", iwidth);
    _environment->GetInt("SetCamera.imageHeight", iheight);
    _image = new CHDRImage();
    assert(_image);
    _image->Allocate(iwidth, iheight);
    _imageVis = new CHDRImage();
    assert(_imageVis);
    _imageVis->Allocate(iwidth, iheight);
  }

  // Set the camera
  this->_camera = world->_camera;

  bool setCameraAutomatically;
  _environment->GetBool("SetCamera.automaticView", setCameraAutomatically);
  if ((!_camera.wasSet) && (setCameraAutomatically)) {
    // Change of a camera setting to get a reasonable image
    SBBox bb = _asds->BBox();
    CVector3D lookAt;
    bb.ComputeCentroid(lookAt); // where we look - center of the scene
    _camera.loc = bb.Max() + bb.Diagonal(); // go aside from the scene
    _camera.dir = Normalize(lookAt - _camera.loc);
    _camera.up = CVector3D(0, 1, 0);
    CVector3D right = CrossProd(_camera.dir, _camera.up);
    right.Normalize();
    _camera.up = CrossProd(right, _camera.dir);
    _camera.up.Normalize();
    // Field of view angle 50 degrees
    float vv_ang = 50.0f * float(M_PI) / 180.0f;
    float vh_ang = vv_ang;
    _camera.FOV = float(M_PI) / 180.f*sqrt(sqr(vv_ang) + sqr(vh_ang));

    if (1) {
      STATUS << "Changing unset camera to" << endl;
      STATUS << "camera.loc = " << _camera.loc << endl;
      STATUS << "camera.dir = " << _camera.dir << endl;
      STATUS << "camera.up = " << _camera.up << endl;
      STATUS << "camera.FOV = " << _camera.FOV*180.f / M_PI << endl << flush;
      STATUS << "BoundingBox = " << _asds->BBox() << endl;
    }
    _camera.wasSet = true;
  }

  // --------------------------------------------------------------  
  return false; // ok
}

// initialized during the computation or within init
bool
CRT_CoreWithCamera_App::CleanUp()
{
  if (_image) delete _image;
  _image = 0;

  if (_imageVis) delete _imageVis;
  _imageVis = 0;

  // remove also the ancestor application
  return CRT_Core_App::CleanUp();
}

// Creates the camera, computes the image, records the statistics data,
// and saves the image, possibly tonemapped and/or HDR.
bool
CRT_CoreWithCamera_App::RunCommon()
{
  // prepare the data structures etc.
  if (RunCommonPrepare())
    return true; // failure

  // compute the image itself
  if (RunCommonCompute())
    return true; // problem

  // Save the image to the file and deallocate the dynamic variables
  if (RunCommonFinish())
    return true; // problem

  return false; // OK
}

bool
CRT_CoreWithCamera_App::RunCommonPrepare()
{
  return false; // OK
}

bool
CRT_CoreWithCamera_App::RunCommonCompute()
{
  // reseting the timer
  _timer.Reset();

  // rendering of the image by ray tracing itself
  int xmin, ymin, xmax, ymax;
  int dotCounterMethod;
  _environment->GetInt("SetCamera.dotCounterMethod", dotCounterMethod);
  _environment->GetInt("SetCamera.scanColumn.start", xmin);
  _environment->GetInt("SetCamera.scanLine.start", ymin);
  xmax = _image->GetWidth();
  ymax = _image->GetHeight();
  if (_environment->OptionPresent("SetCamera.scanColumn.end"))
    _environment->GetInt("SetCamera.scanColumn.end", xmax);
  if (_environment->OptionPresent("SetCamera.scanLine.end"))
    _environment->GetInt("SetCamera.scanLine.end", ymax);
  bool olb = false;
  _environment->GetBool("SetCamera.originLeftBottom", olb);

  int y, x;
  CVector3D dir = Normalize(_camera.dir);
  CVector3D up = _camera.up;
  up.Normalize();
  CVector3D right = CrossProd(dir, up);
  right.Normalize();
  up = CrossProd(right, dir);
  up.Normalize();

  CRay ray;
  CHitPointInfo hinfo;
  float width = float(_image->GetWidth());
  float xcenter = width * 0.5f;
  float height = float(_image->GetHeight());
  float ycenter = height * 0.5f;
  float alpha = atan(height / width);
  float fovy = sin(alpha) * _camera.FOV;
  float fovx = cos(alpha) * _camera.FOV;
  CVector3D right_px = ((2 * tan(fovx / 2)) / width) * right;
  CVector3D up_px = ((2 * tan(fovy / 2)) / height) * up;

  if (olb) {
    up_px = -up_px;
  }

  CColor color;
  if (1) {
    STATUS << "camera.loc = " << _camera.loc << endl;
    STATUS << "camera.dir = " << _camera.dir << endl;
    STATUS << "camera.up = " << _camera.up << endl;
    STATUS << "camera.FOV = " << _camera.FOV*180.f / M_PI << endl << flush;
    STATUS << "BoundingBox = " << _asds->BBox() << endl;
  }
  // -------------------------------------------------------
  this->primHit = 0;
  this->primMiss = 0;
  //float minwh = Min(width, height);

  const int cntMaxPixels = (ymax - ymin)*(xmax - xmin);
  const int cntDots = 80;
  CDotsCounter *dotCount = 0;
  if (dotCounterMethod == 1)
    dotCount = new CDotsPrintCounter(cntMaxPixels, cntDots, cout);
  if (dotCounterMethod == 2)
    dotCount = new CPercentagePrintCounter(cntMaxPixels, cntDots, cout);
  statsAVG.Reset();

  //xmin = 498;			// debugging
  //xmax = 500;
  //ymin = 274;
  //ymin = 500;

  const float eps = 1e-12f;
  for (y = ymin; y != ymax; y++) {
    for (x = xmin; x < xmax; x++) {
      //cout << x << " " << y << endl;
      CVector3D raydir = dir;
      CVector3D rX = (x - xcenter) * right_px;
      CVector3D rY = (y - ycenter) * up_px;
      raydir += rX + rY;
      // Normalize afterwards
      raydir.Normalize();
      if (fabs(raydir.x) < eps)
	raydir.x = eps;
      if (fabs(raydir.y) < eps)
	raydir.y = eps;
      if (fabs(raydir.z) < eps)
	raydir.z = eps;
      ray.Init(_camera.loc, raydir);
      hinfo.SetForFindNearest(ray);
      bool hit = false;
      if (_asds->FindNearest(ray, hinfo)) {
	primHit++;
	hit = true;
      }
      else
	primMiss++;
      // Compute the color
      ComputeColor(ray, hinfo, color);
      _image->SetRGB_XY(x, y, color[0], color[1], color[2]);
      if (_imageVis) {
	// Black for visualization image
	_imageVis->SetRGB_XY(x, y, 0, 0, 0);
	if (hit) {
	  // Statistics only for non-backround pixels
	  statsAVG.Update(evalValueVIS);
	  // Remap value to pseudocolor
	  int alg = 6; // rainbow mapping
	  CColor color;
	  float minval = 0.f;
	  float maxval = 1.f;
	  GetPseudoColor(alg, evalValueVIS, minval, maxval, // input values
			 color); // output color
	  _imageVis->SetRGB_XY(x, y, color[0], color[1], color[2]);
	} // if
      }
      dotCount->Inc();
    } // for x
  } // for y

  delete dotCount;

  _timer.Stop();

  STATUS << "PrimHits= " << primHit << " primMis= " << primMiss
	 << " primTotal= " << primHit + primMiss << endl;
  STATUS << "statsAVG.mean= " << statsAVG.mean << " statsAVG.var= " << statsAVG.Evaluate() << endl;

  return false; // OK
}

// Finish, possibly deallocate the variables
bool
CRT_CoreWithCamera_App::RunCommonFinish(bool /*makeToneMapping*/)
{
  return false; // OK  
}

// reports the statistics result of the specific application
bool
CRT_CoreWithCamera_App::DoReport(const CWorld &world, ostream &app)
{
  CRT_Core_App::DoReport(world, app);
  if (_image) {
    app << "Image of size = " << _image->GetWidth() << " x " << _image->GetHeight() << endl;
  }
  return false;
}

// -----------------------------------------------------------------------
// Initialize all parts of camera

bool
CRT_CoreWithCamera_App::InitCamera()
{
  return false; // OK
}

// Save HDR image without tonemapping and other images
bool
CRT_CoreWithCamera_App::SaveImage(bool hdrFlag)
{
  string ffname, out;
  bool ret = false;

  if (_environment->OptionPresent("OutputFileName")) {
    // Explicitly specifying output filename
    _environment->GetString("OutputFileName", out);
    if (hdrFlag) {
      ret = _image->SaveHDR(out.c_str());
    }
    else {
      ret = _image->SavePNG(out.c_str());
    }

    if (_imageVis) {
      string outVis;
      PrefixExtensionByName(out, string(".vis"), outVis);
      STATUS << "Saving visualization image " << outVis << endl;
      if (!_imageVis->SaveHDR(outVis)) {
	WARNING << " - saving of visualization HDR image failed." << std::endl;
      }
    }
    return ret;
  }

  // Deriving output filename from the input scene filename
  _environment->GetString("InputFileName", ffname);
  if (hdrFlag) {
    out = ChangeFilenameExtension(ffname, string("hdr"));
    ret = _image->SaveHDR(out.c_str());
  }
  else {
    out = ChangeFilenameExtension(ffname, string("png"));
    ret = _image->SavePNG(out.c_str());
  }

  if (_imageVis) {
    string outVis;
    PrefixExtensionByName(out, string(".vis"), outVis);
    STATUS << "Saving visualization image " << outVis << endl;
    if (!_imageVis->SaveHDR(outVis)) {
      WARNING << " - saving of visualization HDR image failed." << std::endl;
    }
  }

  return ret;
}

// since some ray casting application do not compute color and thus the image,
// this is the default setting of the method to compute color
void
CRT_CoreWithCamera_App::ComputeColor(const CRay &, CHitPointInfo &,
				     CColor &/*color*/)
{
  FATAL << "CRT_Core_App::ComputeColor was called - abstract class!\n";
  FATAL_ABORT;
}

__END_GOLEM_SOURCE

