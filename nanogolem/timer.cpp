// ===================================================================
//
// timer.cpp
//     Timing for running process
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, 2000

#include "timer.h"

// Static variable
bool
CTimer::initTimingCalled = false;

void 
CTimer::_initTiming()
{
  initTimingCalled = true;
}

void
CTimer::Reset()
{
  // Real times in seconds
  time_t beg;
  time(&beg);
  lastRealTime = (double)beg;

  realTime   = 0.0;
  countStop = 0;
  running = false;
}

void
CTimer::_start() const
{
  if (running)
    return; // timer is already running

  time_t beg;
  time(&beg);
  lastRealTime = (double)beg;

  // Begin trace.
  running = true;
}

void
CTimer::_stop() const
{
  if (!running)
    return; // timer is not running
  
  time_t end;
  time(&end);
  realTime += (double)end - lastRealTime;

  running = false;
  countStop++;
}

/// default constructor 
CTimer::CTimer()
{
  Reset();
  if (!initTimingCalled)
    _initTiming();
}

// returns the real time measured by timer in seconds
double
CTimer::RealTime() const
{
  if (running) {
    _stop();
    _start();
  }
  return realTime;
}

