// ===================================================================
//
// dotcount.h
//     Header file for printing dots upon computation of an image
//
// Class: CDotsCounter
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, 1999.
//

#ifndef __DOTCOUNT_H__
#define __DOTCOUNT_H__

// nanoGOLEM header files
#include "configh.h"

__BEGIN_GOLEM_HEADER

// forward declarations

// -------------------------------------------------------------------------
// the indication of the portion of the image already computed, the number
// of dots printed when the whole image is computed.
const int numDots = 240;

// --------------------------------------------------------------------------
// This class implements the counter
// --------------------------------------------------------------------------
class CDotsCounter
{
protected:
  long max; // the value to be reached
  long counter; // the counter itself
  long nextCounterValue; // the next counter value to be signalized
  float portion; // the ratio of the one dot for the whole
  long  numDotsPrinted; // the number of the dots already printed

public:
  CDotsCounter(long _max, long numDots);
  virtual ~CDotsCounter() {}

  // initialize the counter
  void Reset() {
    numDotsPrinted = 0;
    counter = 0;
    if (portion > 1.0)
      nextCounterValue = (long) ((float)(numDotsPrinted + 1) * portion);
    else
      nextCounterValue = max;
  }
    
  enum EType {
    EE_dummy = 0,
    EE_dots = 1,
    EE_percentage = 2
  };

  virtual EType GetType() const { return EE_dummy;}

  virtual void Inc(int number = 1);

  long GetValue() { return counter; }

  bool GetCompare(long int numPixels) {
#ifdef __USE_VHMPILIB
    numPixels = numPixels;
    return false; // it always matches
#else // __USE_VHMPILIB
    return (counter != numPixels); // return true, if it does not match
#endif // __USE_VHMPILIB    
  }
};


// --------------------------------------------------------------------------
// This class implements the counter which prints the dot to ostream
// if some specific part of the counter was already computed.
// --------------------------------------------------------------------------
class CDotsPrintCounter:
  public CDotsCounter
{
protected:
  ostream &app; // the ostream for dots used
  // if to flush the stream or not when dot is printed
  bool _flushStream;
public:
  CDotsPrintCounter(long _max, long numDots, ostream &_app,
		    bool flushStream = true):
    CDotsCounter(_max, numDots), app(_app), _flushStream(flushStream) {}  
  virtual ~CDotsPrintCounter() {}

  virtual EType GetType() const { return EE_dots;}

  virtual void Inc(int number = 1);
};

// --------------------------------------------------------------------------
// This class implements the counter which prints the dot to ostream
// if some specific part of the counter was already computed.
// --------------------------------------------------------------------------
class CPercentagePrintCounter:
  public CDotsPrintCounter
{
  int _precision;
public:
  CPercentagePrintCounter(long _max, long numDots, ostream &_app,
			  bool flushStream = true, int precision = 2):
    CDotsPrintCounter(_max, numDots, _app, flushStream),
    _precision(precision) { }
  virtual ~CPercentagePrintCounter() {}

  virtual EType GetType() const { return EE_percentage;}
  
  virtual void Inc(int number = 1);
};

__END_GOLEM_HEADER

#endif // __DOTCOUNT_H__
