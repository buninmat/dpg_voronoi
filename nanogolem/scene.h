// ===================================================================
//
// scene.h
//     Header file for CScene class.
//
// Class: CScene
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, 2000

#ifndef __SCENE_H__
#define __SCENE_H__

// nanoGOLEM headers
#include "configh.h"
#include "color.h"
#include "light.h"
#include "triangle.h"

__BEGIN_GOLEM_HEADER

// forward declarations

// ---------------------------------------------------------
// CScene
//  Contains all known information about the scene
//  Scene is set by a parser.
// ---------------------------------------------------------
class CScene
{
public:
  // Object list of all objects
  CObjectList  objects;
  CLightList   lights;

  // -----------------------------------------------------------------------
  // Background color .. when the primary rays does not hit any object
  // then this object should be used to define the color
  CColor       bgColor;

  // -------------------------------------------------------
  // Methods
  // -------------------------------------------------------

  // Constructor and destructor
  CScene();
  ~CScene();

  // Sets background objects.  Pixels where no object is
  // intersected will be assigned this color.
  void SetBGcolor(CColor *newBGcolor);

  void AddObject(CObject3D *obj) {
    objects.push_back(obj);
  }
  void AddLight(CLight *obj) {
    lights.push_back(obj);
  }
  
  // removes all the allocated data structures inside the scene.
  void CleanUp();
};

__END_GOLEM_HEADER

#endif // __SCENE_H__
