// ===================================================================
//
// rtcore.cpp
//             The core of ray tracer algorithms
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, 2000

// standard headers
#include <iomanip>
#include <cstdio>

// nanoGOLEM headers
#include "configg.h"
#include "errorg.h"
#include "rtcore.h"
#include "scene.h"
#include "triangle.h"
#include "tdbvh.h"
#include "world.h"

#include "asds_voronoi.h"

__BEGIN_GOLEM_SOURCE

// constructor
CRT_Core_App::CRT_Core_App() :
  _asds(0)
{

}

CRT_Core_App::~CRT_Core_App()
{
  CleanUp();
}


bool
CRT_Core_App::Init(CWorld *world, string &outputName)
{
  // init more parent class
  if (CSynthesisApp::Init(world, outputName))
    return true; // some error

  // OBJECTS initialization

  // correct attributes are assigned to each object
  InitObjects(*_objects);

  // Initialization of ASDS for scene objects
  if (InitASDS())
    return true;

  // --------------------------------------------------------------  
  return false; // ok
}

// ASDS initialization  
bool
CRT_Core_App::InitASDS()
{
  bool buildUpASDS;
  _environment->GetBool("RTCore.buildASDS", buildUpASDS);

  // construct the accelerating spatial data structure to accelerate
  // the computation of ray shooting queries
  if (buildUpASDS) {
    // try to build up ASDS
    if (BuildUpASDS())
      return true; // some error when building ASDS
    // the info about ASDS before it is used to console stream
    _asds->ProvideID(STATS);
  }
  // the statistics about the ASDS is printed    
  else {
    WARNING << "Skipping building up ASDS in RTCore\n";
  }

  // --------------------------------------------------------------  
  return false; // ok
}

// correct attributes are assigned to each object
void
CRT_Core_App::InitObjects(CObjectList &/*objects*/)
{
  return;
}

// removes all the auxiliary data structures used by this application
// initialized during the computation or within init
bool
CRT_Core_App::CleanUp()
{
  // Remove the data structure
  if (_asds)
    _asds->Remove();
  delete _asds;
  _asds = 0;
  // remove also the ancestor application
  return CSynthesisApp::CleanUp();
}

// Builds up an ASDS by calling ASDS construction method
// or reads the ASDS from a file.
bool
CRT_Core_App::BuildUpASDS()
{
  STATUS << "\nBuilding up ASDS." << endl;

  _timer.Reset(); // reseting and starting timer
  _timer.Start();

  string asdsType;
  _environment->GetString("RTCore.dataStruct", asdsType);
  if (asdsType.compare("TDBVH") == 0) {
    _asds = new CTDBVH();
  }
  else if (asdsType.compare("Naive") == 0) {
    _asds = new CNaiveRSA();
  }
  else if (asdsType.compare("Voronoi") == 0) {
      _asds = new CASDS_Voronoi();
  }

  if (_asds)
    _asds->BuildUp(*_objects);  // builds up auxiliary data structures

  _timer.Stop();
  STATUS << "\nBuilding up ASDS is finished, real time = "
	 << _timer.RealTime() << endl;

  return false; // OK
}

// reports the statistics result of the specific application
bool
CRT_Core_App::DoReport(const CWorld &/*world*/, ostream &app)
{
  if (_asds) {
    app << "The ASDS built up, ID = " << _asds->GetID() << endl;
  }
  return false;
}

__END_GOLEM_SOURCE

