// ===================================================================
//
// environ.h
//     Header file for environment operations, i.e.. reading
//     environment file, reading command line parameters etc.
//
// Class: CEnvironment
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
/// Initial coding by Vlastimil Havran, 1996

#ifndef __ENVIRON_H__
#define __ENVIRON_H__

// nanoGOLEM headers
#include "configh.h"

// standard headers
#include <string>

__BEGIN_GOLEM_HEADER

// forward declarations
class CVector3D;

// ---------------------------------------------------------------------------
/// Enumeration type used to distinguish the type of the environment variable
enum EOptType
{
  /// the integer variable
  optInt,
  /// the float variable
  optFloat,
  /// the bool variable
  optBool,
  /// the vector variable
  optVector,
  /// the string variable
  optString
};

// ---------------------------------------------------------------------------
/** CEnvironment class.
  This class is used for reading command line, reading environment file and
  managing options. Each recognizable option must be registered in the
  constructor of this class.
 */
class CEnvironment
{
  /**@name Variables */
  //@{
private:
  /// Number of options registered in the environment at most
  int _maxOptions;
  /// Number of columns in the parameter table.
  int numParams;
  /// Number of rows in the parameter table.
  int paramRows;
  /// String with prefixes with non-optional parameters.
  char *optionalParams;
  /// Current row in the parameter table.
  int curRow;
  /** Parameter table itself.
    The organization of this table is two dimensional array of character
    strings. First column are parameters, all the other columns are options
    prefixed by char passed to function GetCmdlineParams and paired with
    corresponding parameters.
   */
  char ***params;

  /// Number of registered options.
  int numOptions;
  /** Options table itself.
    This table is two dimensional array of character strings with four
    columns. First column contains name of the option, second it's value,
    third eventual abbreviation, and finally the fourth contains the default
    value specified when registering the option.
    */
  char ***options;
  /** Options types.
    This is auxiliary table to the table "options". It contains the type of
    each option as specified when registering.
    */
  EOptType *optTypes;
  //@}

  /**@name Methods */
  //@{
  /** Auxiliary method for parsing environment file.
    Function gets one string from buffer, skipping leading whitespaces, and
    appends it at the end of the string parameter. Returns pointer to the next
    string in the buffer with trailing whitespaces skipped. If no string in
    the buffer can't be found, returns NULL.
    @param buffer CInput line for parsing.
    @param string Buffer to append gathered string to.
    @return Pointer to next string or NULL if not succesful.
    */
  char* ParseString(char *buffer, char *string) const; 
  /** Method for registering new option.
    Using this method is possible to register new option with it's name, type,
    abbreviation and default value.
    @param name Name of the option.
    @param type The type of the variable.
    @param abbrev If not NULL, alias usable as a command line option.
    @param defValue Implicit value used if not specified explicitly.
    */
  void RegisterOption(const char *name, const EOptType type,
		      const char *abbrev, const char *defValue = NULL);
  /** Method for checking variable type.
    This method is used to check if the value of the variable can be taken
    as the specified type.
    @param value Tested value of the variable.
    @param type The type of the variable.
    @return true if the variable is of the specified type, false elsewhere.
    */
  bool CheckType(const char *value, const EOptType type) const;
  //@}

  /** Allocate the necessary data for constructor
   */
  void AllocateData(int maxOptions);
  
public:  
  /**@name Methods */
  //@{
  /// Constructor of the CEnvironment class.
  CEnvironment();
  /// Destructor of the CEnvironment class.
  ~CEnvironment();

  /// This method is used as a help on available command line options.
  void PrintUsage(const char *str) const;

  /** Check if the switch "-h" reserved for help is present in the
      command line. Sets the output string to XXX for switch -hXXX
    The only use of this switch is for help. It can be used anytime.
    @param  argc Number of command line arguments.
    @param  argv CArray of command line arguments.
    @param  str  The returned string.
    @return true if found, false elsewhere.
  */
  bool CheckForHelpSwitch(const int argc, const char *argv[],
			  string &str) const;

  /** Check if the specified switch is present in the command line.
    Primary use of this function is to check for presence of some special
    switch. It can be used anytime.
    @param argc Number of command line arguments.
    @param argv CArray of command line arguments.
    @param swtch Switch we are checking for.
    @return true if found, false elsewhere.
   */
  bool CheckForSwitch(const int argc, const char *argv[],
		      const char swtch) const;
  /** First pass of parsing command line.
    This function parses command line and gets from there all non-optional
    parameters (i.e. not prefixed by the dash) and builds a table of
    parameters. According to param optParams, it also writes some optional
    parameters to this table and pair them with corresponding non-optional
    parameters.
    @param argc Number of command line arguments.
    @param argv CArray of command line arguments.
    @param optParams String consisting of prefixes to non-optional parameters.
                     All prefixes must be only one character wide !
   */
  void ReadCmdlineParams(const int argc, const char *argv[],
			 const char *optParams);
  /** Reading of the environment file.
    This function reads the environment file.
    @param filename The name of the environment file to read.
    @return true if OK, false in case of error.
    */
  bool ReadEnvFile(const char *filename);
  /** Second pass of the command line parsing.
    Performs parsing of the command line ignoring all parameters and options
    read in the first pass. Builds table of options.
    @param argc Number of command line arguments.
    @param argv CArray of command line arguments.
    @param index SIndex of non-optional parameter, of which options should
                 be read in.
   */
  void ParseCmdline(const int argc, const char *argv[], const int index);

  /** Clears the environment data structures.
    This function is intended to clear all data structures corresponding to
    particular non-optional parameter to allow options for the next
    non-optional parameter to be read. After this clearing, function
    ReadEnvFile() can be called again to obtain next load of options.
    */
  void ClearEnvironment();
  /** Parameters number query function.
    This function returns number of non-optional parameters specified on
    the command line.
    */
  int GetParamNum() const { return paramRows; }

  /** This function returns number of non-optional parameters specified on
    the command line - they are not preceeded by '-' char
  */
  static int GetParamNum(const int argc, const char *argv[]);
  
  /** Returns the indexed parameter or corresponding optional parameter.
   This function is used for queries on individual non-optional parameters.
   The table must be constructed to allow this function to operate (i.e. the
   first pass of command line parsing must be done).
   @param name If space (' '), returns the non-optional parameter, else returns
               the corresponding optional parameter.
   @param index SIndex of the non-optional parameter to which all other options
                corresponds.
   @param value Return value of the queried parameter.
   @return true if OK, false in case of error or if the parameter wasn't
           specified.
   */
  bool GetParam(const char name, const int index, string &value) const;

  /**@name Options query functions.
   These functions are used for queries on individual options. The table must
   be constructed to allow these functions to operate (i.e. the environment
   file must be read and the second pass of command line parsing be done).
   @param name Name of the option to retrieve.
   @param value Return value of the queried option.
   @param isFatal Boolean value specifying, if the function should exit or
                  return false in case of error.
   @return true if OK, false in case of error.
   */
  //@{
  /// returns true, if the specified option has been specified
  bool OptionPresent(const char *name) const;
  bool OptionPresent(const string &name) const
  { return OptionPresent(name.c_str()); }
  /// returns named option as a integral value.
  bool GetInt(const char *name, int &value,
	      const bool isFatal = true) const;
  bool GetInt(const string &name, int &value,
	      const bool isFatal = true) const
  { return GetInt(name.c_str(), value, isFatal); }
  int GetInt(const string& name) const
  { int v; GetInt(name,v,true); return v;}

  /// returns named option as a double floating point value.
  bool GetFloat(const char *name, double &value,
		const bool isFatal = true) const;
  bool GetFloat(const string& name, double &value,
		const bool isFatal = true) const
  { return GetFloat(name.c_str(), value, isFatal); }

  /// returns named option as a single floating point value.
  bool GetFloat(const char *name, float &value,
		const bool isFatal = true) const
  { double v; bool b=GetFloat(name,v,isFatal); value=float(v); return b; }
  bool GetFloat(const string& name, float &value,
		const bool isFatal = true) const
  { return GetFloat(name.c_str(), value, isFatal); }
  float GetFloat(const string& name) const
  { float v; GetFloat(name,v,true); return v;}

  /// returns named option as a boolean value.
  bool GetBool(const char *name, bool &value,
	       const bool isFatal = true) const;
  bool GetBool(const string &name, bool &value,
	       const bool isFatal = true) const
  { return GetBool(name.c_str(), value, isFatal); }
  bool GetBool(const string& name) const
  { bool b; GetBool(name,b,true); return b;}

  /// returns named option as a 3D vector.
  bool GetVector(const char *name, float &x, float &y, float &z,
		 const bool isFatal = true) const;
  bool GetVector(const string& name, float &x, float &y, float &z,
		 const bool isFatal = true) const
  { return GetVector(name.c_str(), x,y,z, isFatal); }

  /// returns named option as a character string.
  bool GetString(const char *name, string &value,
		 const bool isFatal = true) const;
  bool GetString(const string &name, string &value,
		 const bool isFatal = true) const
  { return GetString(name.c_str(), value, isFatal); }
  string GetString(const string& name) const
  { string v; GetString(name,v,true); return v;}

  //@}
  /**@name Options set functions.
   These functions are used for setting individual options.
   @param name Name of the option to set.
   @param value Value to set the option to.
   */
  //@{
  /// sets named option as a integral value.
  void SetInt(const char *name, const int value);
  void SetInt(const string &name, const int value)
  { SetInt(name.c_str(), value); }
  /// sets named option as a floating point value.
  void SetFloat(const char *name, const float value);
  void SetFloat(const string &name, const float value)
  { SetFloat(name.c_str(), value); }
  /// sets named option as a boolean value.
  void SetBool(const char *name, const bool value);
  void SetBool(const string &name, const bool value)
  { SetBool(name.c_str(), value); }
  /// sets named option as a 3D vector.
  void SetVector(const char *name,
		 const float x, const float y, const float z);
  void SetVector(const string &name,
		 const float x, const float y, const float z)
  { SetVector(name.c_str(), x,y,z ); }
  /// sets named option as a character string.
  void SetString(const char *name, const char *value);
  void SetString(const string &name, const char *value)
  { SetString(name.c_str(), value); }
  void SetString(const char *name, const string &value);
  void SetString(const string &name, const string &value)
  { SetString(name.c_str(), value); }
  //@}
  //@}
};

__END_GOLEM_HEADER

#endif // __ENVIRON_H__

