// ===================================================================
//
// brdfmodel.h
//     Header file for BRDF class in nanoGOLEM.
//
// Class: BRDF, BRDF_Phong
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, 2000

#ifndef _BRDFMODEL_H__
#define _BRDFMODEL_H__

#include <cassert>
#include <cmath>
#include <cstring>
#include <string.h>

// nanoGOLEM headers
#include "configh.h"
#include "color.h"
#include "vector3.h"

__BEGIN_GOLEM_HEADER

// Common interface to BRDF used in this project
class BRDF
{
public:
  BRDF() {}
  virtual ~BRDF() { }

  // returns cosine corrected value of BRDF given the illumination
  // and viewer angle in RGB, returns false on success
  virtual bool GetBRDFValue(float u, float v, // texture position
			    float theta_view, float phi_view, // viewer direction as two angles in radians
			    float theta_il, float phi_il, // illum. direction as two angles in radians
			    CColor &RGB) const = 0; // output value
  
  // Get albedo for viewing direction
  virtual float GetAlbedo(float u, float v, // texture position
			  // viewer direction as two angles in radians
			  float theta_view, float phi_view) const = 0;

  // Make stochastic sampling, where Z-axis is the normal of a surface
  virtual bool SpawnStochasticRay(float u, float v,
				  float rnd1, float rnd2, // input
				  float thetaV, float phiV, // the direction of a viewer
				  CVector3D &XYZ, // output direction
				  CColor &RGB, // corresponding color
				  float &prob) const = 0;
  
  // As default BRDF functions are considered isotropic .. returns false here
  virtual bool IsAnisotropic() const {return false;}

  // As default BRDF functions are considered non spatially varying
  virtual bool isSpatiallyVarying() const {return false;}

  // Different BRDF models
  enum {
    EE_Phong = 1,
  };

  // returns probability of sampling in a given viewer direction
  // The user has to provide also RGB[3] since it is computed from
  // the value. Typically in MIS, this function is called after
  // the call GetBRDFValue() that RGB is computed. RGB is not changed.
  virtual float GetProbOfSampling(float u, float v,
				  float theta_view, float phiV,
				  float theta_i, float phi_i,
				  const CColor &RGB) const = 0;
  
  virtual int GetID() const = 0; // abstract class
  virtual void GetString(string &outstr) const = 0; // abstract class
};

// -------------------------------------------------------------------
// Implementation using Phong model - white color only
class BRDF_Phong: public BRDF
{
protected:
  // The specification of the Phong's model that is white color
  mutable float rho_diff;
  mutable float rho_spec;
  mutable float spec_index;
public:
  BRDF_Phong(float rho_diffuse, float rho_specular, float spec_index);
  virtual ~BRDF_Phong() {}

  virtual int GetID() const { return EE_Phong;}
  virtual void GetString(string &outstr) const;
  
  // returns cosine corrected value of BRDF given the illumination
  // and viewer angle in RGB, returns false on success
  virtual bool GetBRDFValue(float u, float v,
			    float theta_view, float phi_view,
			    float theta_il, float phi_il,
			    CColor &RGB) const;
  // returns albedo - value between 0.0 and 1.0
  virtual float GetAlbedo(float u, float v, // texture position
			  // viewer direction as two angles in radians
			  float theta_view, float phi_view) const;
  // Make stochastic sampling 
  virtual bool SpawnStochasticRay(float u, float v,
				  float rnd1, float rnd2, // input
				  float thetaV, float phiV, // the direction of a viewer
				  CVector3D &XYZ, // output direction
				  CColor &RGB, // corresponding color
				  float &prob) const; // and probability

  // get probability of sampling for particular direction of the function BRDF.cos(theta),
  //RGB has to be provided as BRDF.cos(theta)
  virtual float GetProbOfSampling(float u, float v,
				  float theta_view, float phiV,
				  float theta_i, float phi_i,
				  const CColor &RGB) const;
};

__END_GOLEM_HEADER

#endif // _BRDFMODEL_H__
