// ===================================================================
//
// dotcount.cpp
//     CCamera/viewing frustrum for the rendering.
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, 1999.

// standard C++ headers
#include <cstdio>
#include <iomanip>

// nanoGOLEM headers
#include "configg.h"
#include "dotcount.h"

__BEGIN_GOLEM_SOURCE

// implementation of counters for pixels

// ------------------------------------------------------------------------
// class CDotsCounter
// ------------------------------------------------------------------------
CDotsCounter::CDotsCounter(long _max, long numDots):
  max(_max), counter(0L)
{
  portion = (float)max/(float)numDots;
  Reset();
}

void
CDotsCounter::Inc(int number)
{
  //  if (counter >= max)
  //    return; // all the dots were already printed
  
  counter += number;
}

// ------------------------------------------------------------------------
// class CDotsPrintCounter
// ------------------------------------------------------------------------
void
CDotsPrintCounter::Inc(int number)
{
  counter += number;

  if (counter >= nextCounterValue) {
    while (counter >= nextCounterValue)
    {
      app << ".";
      numDotsPrinted++;
      nextCounterValue = (long)((float)(numDotsPrinted + 1) * portion);
    }
    if (_flushStream)
      app << flush;
  }
}

// ------------------------------------------------------------------------
// class CPercentagePrintCounter
// ------------------------------------------------------------------------
void
CPercentagePrintCounter::Inc(int number)
{
  counter += number;

  if (counter >= nextCounterValue) {
    while (counter >= nextCounterValue)
    {
      app << "|"
	  << setprecision(_precision) << (100.0f * nextCounterValue/max) ;
      numDotsPrinted++;
      nextCounterValue = (long)((float)(numDotsPrinted + 1) * portion);
    }
    if (_flushStream)
      app << flush;
  }
}

__END_GOLEM_SOURCE

