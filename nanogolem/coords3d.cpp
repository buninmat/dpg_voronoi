// ===================================================================
//
// coords3d.cpp
//             Various coordinates conversions
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, 2010

// standard headers
#include <cstdlib>
#include <fstream>
#include <cmath>
#include <cassert>

// nanoGOLEM headers
#include "configg.h"
#include "coords3d.h"

__BEGIN_GOLEM_SOURCE

// input theta and phi in radians, output xyz is the unit vector
void
ConvertThetaPhiToXYZ(float theta, float phi, CVector3D &xyz)
{
  // Here we convert the angles to 3D vector
  xyz.x = cos(phi)*sin(theta);
  xyz.y = sin(phi)*sin(theta);
  xyz.z = cos(theta);
}

// The method that converts the directional vector on the hemisphere to the
// angle theta (from zenith) and phi angle (the azimuth)
void
ConvertXYZtoThetaPhi(const CVector3D &xyzV, // input
                     float &theta, // output
                     float &phi)
{
  CVector3D xyz = xyzV;
  xyz.Normalize();

  // Standard conversion
  theta = acos(xyz[2]);
  if (theta > float(M_PI))
    theta = float(M_PI);
  phi = atan2(xyz[1], xyz[0]);

  if (phi < 0.f)
    phi += 2.f*float(M_PI);
  if (phi > 2.f * float(M_PI))
    phi -= 2.f*float(M_PI);

  assert( (phi >= 0.f) && (phi <= 2.f * float(M_PI)));
  return;
}

__END_GOLEM_SOURCE

