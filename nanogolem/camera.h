// ===================================================================
//
// camera.h
//     Header file for CCamera class.
//
// Class: CCamera
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, 2000

#ifndef __CAMERA_H__
#define __CAMERA_H__

// nanoGOLEM headers
#include "configh.h"
#include "vector3.h"

__BEGIN_GOLEM_HEADER

// The definition of the perspective camera to be used
struct CCamera
{
  CVector3D  loc; // at vector
  CVector3D  dir; // dir vector
  CVector3D  up; // up vector
  float      FOV; // camera field of view - diagonal
  bool       wasSet; // flag if camera was set

  CCamera(): loc(0,0,0), dir(0,0,1), up(0,1,0),
	     FOV(50.f*float(M_PI)/180.f), wasSet(false) { }
};

__END_GOLEM_HEADER

#endif // __CAMERA_H__

