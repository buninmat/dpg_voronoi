// ===================================================================
//
// triangle.h
//     Header file for Triangle class in nanoGOLEM.
//
// Class: Triangle
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, 2000

#ifndef __TRIANGLE_H__
#define __TRIANGLE_H__

// Standard C++ headers
#include <vector>

// nanoGOLEM headers
#include "configh.h"
#include "color.h"
#include "hminfo.h"
#include "sbbox.h"
#include "triangle.h"
#include "vector2.h"
#include "vector3.h"

__BEGIN_GOLEM_HEADER

// Forward declarations
class CRay;

// Declaration of a general 3D primitive
class CObject3D
{
protected:
  static int countID;
  // local variables
  int objID; // unique ID of each geometric primitive
public:
  static void SetCountID(int objsID);
  void SetUniqueID(int newID) {objID = newID;}
  int UniqueID() const { return objID;}
  // Only a flag for all possible primitives
  static bool twoSidedPlanars;
  enum EType {EE_Object3D=0, EE_Triangle=1, EE_TriangleNorm=2, EE_TriangleInt=3};
  
  // Minimalistic interface for ray tracing
  CObject3D() {}
  virtual ~CObject3D() {}
  virtual EType ID() const { return EE_Object3D;}
  virtual SBBox GetBox() const = 0;
  virtual bool NearestInt(CRay &ray, CHitPointInfo &info) const = 0;
  virtual void FindNormal(CHitPointInfo &info) const = 0;
  virtual void GetUVvectors(CHitPointInfo &/*info*/) const {
    WARNING << "GetUVvectors Not implemented" << flush;
    //FATAL_ABORT;
  }
  virtual void GetUV(CHitPointInfo &/*info*/) const {
    WARNING << "GetUV Not implemented" << flush;
    //FATAL_ABORT;
  }
};

// -----------------------------------------------------------
// Just a container of triangles/general 3D shapes
class CObjectList :
  public vector<CObject3D* >
{
public:
  CObjectList() { }
};

// -----------------------------------------------------------
// A triangle represented by 3 vertices and one color
// for the whole triangle
class CTriangle:
  public CObject3D
{
protected:
  CColor color;
  unsigned int boundingBoxIndices;
public:
	CVector3D vertices[3];
  // Constructor  
  CTriangle() {}
  CTriangle(const CVector3D newVerts[3]) {
    vertices[0] = newVerts[0];
    vertices[1] = newVerts[1];
    vertices[2] = newVerts[2];
    //vertices[1] = newVerts[2];
    //vertices[2] = newVerts[1];
    ComputeBoundingBoxIndices();
  }
  virtual EType ID() const { return EE_Triangle;}

  bool Check() const { return true; } // triangle OK .. false

  void ComputeBoundingBoxIndices() {
    int axis;
    boundingBoxIndices = 0;
    for (axis=2; axis >=0; axis--) {
      int i = 0;
      if (vertices[1][axis] > vertices[i][axis])
	i = 1;
      if (vertices[2][axis] > vertices[i][axis])
	i = 2;
      boundingBoxIndices = (boundingBoxIndices<<2) + i;
    }    
    for (axis=2; axis >=0; axis--) {
      int i = 0;
      if (vertices[1][axis] < vertices[i][axis])
	i = 1;
      if (vertices[2][axis] < vertices[i][axis])
	i = 2;
      boundingBoxIndices = (boundingBoxIndices<<2) + i;
    }
  }
  
  CVector3D GetMin() const {
    return CVector3D(GetMin(0), GetMin(1), GetMin(2));
  }

  CVector3D GetMax() const {
    return CVector3D(GetMax(0), GetMax(1), GetMax(2));
  }
  
  float GetMin(const int axis) const {
    //return vertices[(boundingBoxIndices>>(axis<<1))&3][axis];
    return Min(Min(vertices[0][axis], vertices[1][axis]), vertices[2][axis]);
  }
  
  float GetMax(const int axis) const {
    //return vertices[(boundingBoxIndices>>(6+(axis<<1)))&3][axis];
    return Max(Max(vertices[0][axis], vertices[1][axis]), vertices[2][axis]);
  }

  float GetCenter(const int axis) {
    return (vertices[0][axis] + vertices[1][axis] + vertices[2][axis])/3.0f;
  }

  virtual SBBox GetBox() const {
    /*CVector3D minb = GetMin();
    CVector3D maxb = GetMin();
    const float eps = 2e-2f;
    float add = eps * Magnitude(maxb-minb);
    return SBBox(minb-CVector3D(add,add,add), GetMax()+CVector3D(add,add,add));*/
	return SBBox(GetMin(), GetMax());
  }
  
  virtual bool NearestInt(CRay &ray, CHitPointInfo &info) const;
  virtual void FindNormal(CHitPointInfo &info) const {
    info.SetNormal(Normalize(CrossProd(vertices[1] - vertices[0], vertices[2] - vertices[0])));
  }
};

// -----------------------------------------------------------
// A triangle represented by 3 vertices with normals and tangents
// for the whole triangle
class CTriangleNorm:
  public CTriangle
{
protected:
  // The triangles interpolated value for the vertices
  CVector3D normals[3];
public:  
  // Constructor
  CTriangleNorm() {}
  CTriangleNorm(const CVector3D newVertices[], const CVector3D &newNormal);
  CTriangleNorm(const CVector3D newVertices[], const CVector3D newNormals[]);
  virtual EType ID() const { return EE_TriangleNorm;}
  CVector3D GetNormalForVertex(int i) const {assert(i>=0 && i < 3); return normals[i];}
  virtual void FindNormal(CHitPointInfo &info) const;
};

// -----------------------------------------------------------
// A triangle represented by 3 vertices with normals and tangents
// for the whole triangle
class CTriangleInt:
  public CTriangleNorm
{
protected:
public:  
  // The triangles interpolated value for the vertices
  CVector2D uv[3];
  CVector3D vectorU[3];
  CVector3D vectorV[3];

  // Constructor
  CTriangleInt(){
    for (int i = 0; i < 3; i++) {vectorU[i].Set(0.f); vectorU[i].Set(0.f);}
  }
  CTriangleInt(const CVector3D newVertices[], const CVector3D newNormals[],
	       const CVector2D newUV[]);
  virtual EType ID() const { return EE_TriangleInt;}
  void SetTangent(int i, const CVector3D &vec) {assert(i>=0 && i < 3); vectorU[i] = vec;}
  void SetBiTangent(int i, const CVector3D &vec) {assert(i>=0 && i < 3); vectorV[i] = vec;}
  virtual void GetUVvectors(CHitPointInfo &info) const;
  virtual void GetUV(CHitPointInfo &info) const;
};

__END_GOLEM_HEADER

#endif //  __TRIANGLE_H__

