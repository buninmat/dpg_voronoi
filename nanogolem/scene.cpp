// ===================================================================
//
// scene.cpp
//     CScene and input related routines in ray tracer.
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, 2000

// nanoGOLEM headers
#include "configg.h"
#include "basmacr.h"
#include "scene.h"

__BEGIN_GOLEM_SOURCE

// ---------------------------------------------------------------------
// Class CScene

// CScene constructor
CScene::CScene()
{
  bgColor = CColor(0,0,0); // initially black
}

// CScene destructor
CScene::~CScene()
{
  CleanUp();
}

void
CScene::CleanUp()
{
  // iterate the list of objects and free them from memory
  for (CObjectList::iterator io = objects.begin();
       io != objects.end(); io++) {
    delete (*io);
  }
  // remove the list of objects
  objects.erase(objects.begin(), objects.end());
}

__END_GOLEM_SOURCE

