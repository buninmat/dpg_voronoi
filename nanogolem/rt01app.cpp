// ===================================================================
//
// rt01app.cpp
//             Sampling from environment map
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, 2015

// standard headers
#include <cstdlib>
#include <fstream>

// nanoGOLEM headers
#include "configg.h"
#include "brdfmodel.h"
#include "colorfs.h"
#include "environ.h"
#include "rnd.h"
#include "rt01app.h"
#include "scene.h"
#include "vector3.h"

__BEGIN_GOLEM_SOURCE

// Compute angles for BRDF evaluation
void
CRT_01_App::GenerateAngles(const CVector3D &dir, float dotProd,
			   CVector3D &tangent, CVector3D &bitangent,
			   float &theta, float &phi)
{
  // compute theta angle for viewer direction for spherical parametrization
  if (dotProd >= 1.0f) dotProd = 1.0f;
  if (dotProd <= -1.0f) dotProd = -1.0f;
  assert(dotProd >= -1.00001f); 
  assert(dotProd <= 1.00001f); 
  theta = acos(dotProd);
  CVector3D ndir = Normalize(dir);
  float u = DotProd(tangent, ndir); // negative values are needed
  float v = DotProd(bitangent, ndir);
  phi = atan2(v, u);
  while (phi < 0.f)
    phi += float(2.0*M_PI);
  while (phi > float(2.0*M_PI))
    phi -= float(2.0*M_PI);
  // We have theta_v and phi_v in local coordinate system
  if (isnan(phi)) {
    FATAL << "ERROR: PHI = INF";
    phi = 0.f;
  }
}

void
CRT_01_App::ComputeColor(const CRay &ray, CHitPointInfo &info,
			 CColor &color)
{
  CVector3D dirViewer = Normalize(-ray.GetDir());
  // just set the black color if no object was hit
  if (info.GetObject3D() == NULL) {
    float theta, phi;
    ConvertXYZtoThetaPhi(dirViewer, theta, phi);
    em.GetRGB_ThetaPhi(theta, phi, color);
    color *= _imgHdrMult;
    return;
  }
  statsSamplingEMtrack.Reset();
  
  // the diffuse BRDF of the surface 
  info.Extrap(); // Compute the point of intersection
    
  CVector3D N = info.GetNormal();
  float dotProd = DotProd(N, dirViewer);
  if (isnan_local(dotProd)) {
    return;
  }
  CVector3D tangent, bitangent2;
  info.GetDifferentials(tangent, bitangent2);
  float u,v;
  info.GetUV(u,v);
  CVector3D bitangent = CrossProd(N, tangent);
  float theta_v, phi_v;
  GenerateAngles(dirViewer, dotProd,
		 tangent, bitangent, theta_v, phi_v);
  CColor brdfRGB, entryRGB;
  color = 0.f; // initiate the output value
  // Sample according to EM
  for (int l = 0; l < cntSamples; l++) {
    em.GenerateNewRandomLightPrecomputed();
    CVector3D dirLight = em.GetDirectionPrecomputedLight(l);
    dirLight.Normalize();
    float dotProdLight = DotProd(N, dirLight);
    if (dotProdLight < 0.0f) {
      // The sample invisible
      statsSamplingEMtrack.Update(0.f);
      continue; 
    }
    
    float theta_i, phi_i;
    GenerateAngles(dirLight, dotProdLight,
		   tangent, bitangent, theta_i, phi_i);
    brdf->GetBRDFValue(u, v, theta_v, phi_v, theta_i, phi_i, brdfRGB);
    for(int isp = 0; isp < 3; isp++) {
      entryRGB[isp] = brdfRGB[isp] * em.GetColorPrecomputedLight(l, isp);
      color[isp] += entryRGB[isp];
    } // for isp
    statsSamplingEMtrack.Update(entryRGB.Luminance() * (float)cntSamples);
  } // for l

  // multiplying the intensity by predefined constant
  color *= _imgHdrMult/(float)cntSamples;

  // And those in postprocessing when reweighting
  SetVariableForVisualizationAfter(visModeVariable, brdf, u, v, 0.0f, 0.0f,
                                   (float)statsSamplingEMtrack.Evaluate(),
				   (float)statsSamplingEMtrack.Evaluate(),
                                   (float)statsSamplingEMtrack.Evaluate());

  return;
}

void
CRT_01_App::SetData()
{
  _environment->GetInt("SetRayTracing.visModeVariable", visModeVariable);
  STATUS << "VisModeVariable = " << visModeVariable << endl;

  _environment->GetInt("SetRayTracing.cntSamples", cntSamples);
  STATUS << "SamplesPerPixel = " << cntSamples << endl;

  string envfilename;
  _environment->GetString("Scene.envFileName", envfilename);
  int envMapCnt;
  _environment->GetInt("SetRayTracing.envCntSamples", envMapCnt);
  
  // Prepare environment map for sampling
  if (!em.LoadHDR(envfilename)) {
    FATAL << "The environment file " << envfilename << " cannot be read" << endl;
    FATAL_ABORT;
  }
  em.ComputeLuminance();
  em.NormalizePower();
  em.ComputeLuminance();
  em.ComputeCDF();
  em.PrecomputeLights(envMapCnt);
  // Each pixel has different set of lights
  em.SetModeRandomLights(true);

  string type;
  _environment->GetString("BRDF.type", type);
  // Create the BRDF model
  if (type.compare("Phong") == 0) {
    float kd, ks, ns_exp;
    _environment->GetFloat("BRDF.Phong.kd", kd);
    _environment->GetFloat("BRDF.Phong.ks", ks);
    _environment->GetFloat("BRDF.Phong.ns", ns_exp);
    if (kd + ks > 1.0) {
      WARNING << "The albedo greater than zero!!!" << endl;
    }    
    // Prepare BRDF for evaluation
    brdf = new BRDF_Phong(kd, ks, ns_exp);
  }
  else if (type.compare("Phongkd") == 0) {
    // kd is read and ks = 1.0-kd
    float kd, ks, ns_exp;
    _environment->GetFloat("BRDF.Phong.kd", kd);
    ks = 1.0f - kd;
    _environment->GetFloat("BRDF.Phong.ns", ns_exp);
    // Prepare BRDF for evaluation
    brdf = new BRDF_Phong(kd, ks, ns_exp);
  }
  if (!brdf) {
    FATAL << "BRDF was not specified - exiting" << endl;
    FATAL_ABORT;
  }
  
  return;
}

// Set the visualization of the sampling after the sampling
bool
CRT_01_App::SetVariableForVisualizationAfter(int whatToShow,
					     BRDF *brdf, float u, float v,
					     float ratioBRDFtoAll,
					     float prodBRDFvar, float prodEMvar,
					     float varProdEstimateOnline,
					     float varProdEstimatePostprocessing)
{
  brdf = brdf;
  switch (whatToShow) {
    case 0: SetValueST(ratioBRDFtoAll); // float ratioBRDFtoAll
      return true;
    case 1: SetValueST(prodBRDFvar);
      return true;
    case 2: SetValueST(prodEMvar);
      return true;
    case 3: SetValueST(varProdEstimateOnline);
      return true;
    case 4: SetValueST(varProdEstimatePostprocessing);
      return true;
    case 5: SetValueST(u);
      return true;
    case 6: SetValueST(v);
      return true;
    default: {
      WARNING << "Unknown variable to debug after - exiting" << endl;
      return false; // error
    }
  } // switch
}

bool
CRT_01_App::Init(CWorld *world, string &outputName)
{
  // init previous classes
  if (CRT_00_App::Init(world, outputName))
    return true; // error occured

  SetData();
  statsAVG.Reset();
  
  STATUS << "EM sampling" << endl << flush;
  STATUS << "===========================================" << endl << flush;
  return false; // ok
}

__END_GOLEM_SOURCE
