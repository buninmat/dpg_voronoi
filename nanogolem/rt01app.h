// ===================================================================
//
// rt01app.h
//     Header file for the project - EM illumination, sampling EM
//
// Class: CRT_01_App
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, 2015.

#ifndef __RT01APP_H__
#define __RT01APP_H__

// Standard C++ headers
#include <fstream>

// nanoGOLEM headers
#include "configh.h"
#include "basmath.h"
#include "brdfmodel.h"
#include "color.h"
#include "rt00app.h"

__BEGIN_GOLEM_HEADER

class CRT_01_App:
  public CRT_00_App
{
protected:
  // Environment map with sphere
  CEnvironmentMap em;
  // BRDF to be used for the whole model
  BRDF* brdf;
  // number of samples taken for integration of a single pixel
  int   cntSamples;
  // statistics when sampling from EM for a single pixel
  SVAR  statsSamplingEMtrack;
  // visualization variable
  int   visModeVariable;
  void  SetValueST(float vv) { evalValueVIS = vv; }

  // Set the visualization of the sampling after the sampling
  bool SetVariableForVisualizationAfter(int whatToShow,
					BRDF *brdf, float u, float v,
					float ratioBRDFtoAll,
					float prodBRDFvar, float prodEMvar,
					float varProdEstimateOnline,
					float varProdEstimatePostprocessing);

  // function to return the color for primary rays to be passed to
  // frame sampling scheme .. it is used by CColorMapper in the camera.
  virtual void ComputeColor(const CRay &ray, CHitPointInfo &info, CColor &color);
  void GenerateAngles(const CVector3D &dir, float dotProd,
		      CVector3D &tangent, CVector3D &bitangent,
		      float &theta, float &phi);
  // Set EM and BRDF
  void SetData();
public:
  // constructor
  CRT_01_App(): CRT_00_App(),brdf(0) {}
  // destructor
  virtual ~CRT_01_App() {}

  virtual bool Init(CWorld *world, string &outputName);
  
  // deletes all the auxiliary data structures
  virtual bool CleanUp() {
    delete brdf; brdf = 0;
    CRT_00_App::CleanUp();
    return false;
  }
};

__END_GOLEM_HEADER

#endif // __RT01APP_H__
