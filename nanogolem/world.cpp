// ===================================================================
//
// world.cpp
//     CWorld-related routines in ray tracer.
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, 2000.

// standard headers
#include <string>

// nanoGOLEM headers
#include "configg.h"
#include "appcore.h"
#include "asds.h"
#include "assimpparse.h"
#include "basstr.h"
#include "rtcoreca.h"
#include "scene.h"
#include "timer.h"
#include "viewpars.h"
#include "world.h"

__BEGIN_GOLEM_SOURCE

// Static variables and initial settings
float
CLimits::Threshold = 0.01f;
float
CLimits::Small = 0.01f;
float
CLimits::Infinity = 1e10f;

// -----------------------------------------------------------
// class CWorld

// CWorld constructor.
CWorld::CWorld(const int argc, const char *argv[]):
  _application(0), _scene(0)
{
  string strhelp;
  // First, check for request for help
  if (_environment.CheckForHelpSwitch(argc, argv, strhelp)) {
    _environment.PrintUsage(strhelp.c_str());
    return;
  }

  // Read the names of the scene, environment and output files
  _environment.ReadCmdlineParams(argc, argv, "E");

  // just parse the arguments before first scene is parsed
  _environment.ParseCmdline(argc, argv, 0);  
}

// CWorld destructor.
CWorld::~CWorld()
{
  CleanUp();
}

// clean all data allocated inside the world
void
CWorld::CleanUp()
{
  if (_application)
    delete _application;  
  _application = 0;
  _environment.ClearEnvironment();

  if (_scene) {
    _scene->CleanUp();
    delete _scene;
    _scene = 0;
  }

  return;
}

bool
CWorld::Init(const string &inputFileName)
{
  SetDefaultValues();

  if (_environment.OptionPresent("OutputFileName")) {
    // the output name was specified explicitely
    _environment.GetString("OutputFileName", outputFileName);
  }
  else {
    // compute the output filename from input one
    _application->GetFileName(outputFileName, inputFileName, _environment);
    // and set it to the environment file
    _environment.SetString("OutputFileName", outputFileName.c_str());
  }

  // Set the output file type from the output filename
  // the dot in the output file name from backward
  unsigned int pos = static_cast<unsigned>(outputFileName.find_last_of('.'));
  if (pos < outputFileName.length()) {// concrete output type set
    string ext(outputFileName, pos+1, outputFileName.length() - pos - 1);
    _environment.SetString("OutputFileType", ext.c_str());
  }

  if (_scene) {
    STATUS << "Reassigning objects IDs" << endl;
    CASDS::ReassignUniqueIDs(_scene->objects);
  }

  return false; // OK
}

void
CWorld::SetDefaultValues()
{  
  // DEBUG << "CWorld::SetDefaultValues() is called" << endl;

  // sets the variable, that can be important for parser as well
  _environment.GetFloat("Limits.threshold", CLimits::Threshold);
  _environment.GetFloat("Limits.small", CLimits::Small);
  _environment.GetFloat("Limits.infinity", CLimits::Infinity);
  
  int seed;
  // the seed of pseudorandom generator
  _environment.GetInt("SeedValue", seed);
  SetRandomSeed(seed);

  CVector3D v;
  _environment.GetVector("Scene.backgroundColor", v.x, v.y, v.z);
  _scene->bgColor[0] = v.x;
  _scene->bgColor[1] = v.y;
  _scene->bgColor[2] = v.z;
  STATS << "BgColor = " << _scene->bgColor << endl;
  
  return;
}

// parse the scene, returns false if everything OK
bool
CWorld::ParseScene(const string &sceneFilename)
{
  assert(_scene);

  // the timer used when reading the scene
  CTimer timer;

  string ext = ExtractExtension(sceneFilename.c_str());
  string newSceneFilename = sceneFilename;
  if (ext.compare("view") == 0) {
    // Get the camera setting from a file
    bool ok =
      CParserVIEWRun(sceneFilename, newSceneFilename, *this);
    if (!ok)
      STATUS << "Camera parsed, new scene filename = " << newSceneFilename << endl;
    else
      STATUS << "Cannot parse the scene = " << newSceneFilename << endl;
  }  
  
  // starting the timer
  timer.Start();

  bool twoSidedPlanars = false;  
  if (_environment.OptionPresent("Scene.twoSidedPlanars"))
    _environment.GetBool("Scene.twoSidedPlanars", twoSidedPlanars);  
  if (twoSidedPlanars)
    STATUS << "Two sided planar primitives will be created.\n";
  CObject3D::twoSidedPlanars = twoSidedPlanars;

  // ASSIMP based parser
  CParserASSIMP parser;
  bool result = parser.Parse(newSceneFilename.c_str(), _environment, *_scene);

  timer.Stop();
  DEBUG << "Parsing time= " << timer.RealTime() << endl;
  return result;
}

bool
CWorld::CreateApplication(void)
{
  DeleteApplication();

  // which application should be run ?
  string applicationName;
  _environment.GetString("Application.type", applicationName);

  _application = CSynthesisApp::CreateApplication(applicationName);
  
  // here is the place to add some other applications
  if (_application == NULL) {
    FATAL << " unknown application type for nanoGOLEM\n";
    FATAL << " Application type was used: " << applicationName << endl;
    _application = 0;
    return true;  // error
  }

  return false; // ok
}

void
CWorld::DeleteApplication(void)
{
  if (_application) {
    _application->CleanUp();
    delete _application;
    _application = 0;
  }  
}

// Function for running the algorithm for one scene, including
// setting up the application, running application, cleaning application,
// and running statistics and reference statistics.
bool
CWorld::Run()
{
  // Initializating some application
  // --------------------------------------------------------------
  // Anything can happen inside the application initialization,
  // and moreover, this initialization must be enabled
  bool initApplication;
  _environment.GetBool("Init", initApplication);

  if (initApplication) {
    STATUS << "Application - initialization" << endl;
    if (_application->Init(this, outputFileName)) {
      return true; // some error occured during initialization
    }
  }

  string baseOutputName;
  _environment.GetString("OutputFileName", baseOutputName);

  // Running some application
  // --------------------------------------------------------------
  // anything can happen inside the application run, and it must be enabled
  bool runApplication;
  _environment.GetBool("Run", runApplication);

  if (runApplication) {
    STATUS << "Running the application" << endl;
    if (!initApplication)
      WARNING << "The application was not initialized for run" << endl;
    
    // running application is enabled and Init was performed
    if (_application->Run()) {
      return true; // some error occured when running master code
    }
  }

  // CleanUp the data
  // ---------------------------------------------------------------------
  bool cleanUpApplicationData;
  _environment.GetBool("CleanUp", cleanUpApplicationData);

  if (cleanUpApplicationData) {
    STATUS << "Application - cleaning up the data structures" << endl;
    if (_application->CleanUp())
      return true; // some error occured when deleting the application
  }
  
  return false; // everything correct
}

__END_GOLEM_SOURCE

