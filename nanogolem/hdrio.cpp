// ================================================================
//
// hdrio.cpp
//     This is the support to report messages and errors/
//       and debug bugs in nanoGOLEM.
//
//  The design of these streams definition was motivated when reading PovRay manual.
//
// Class: CHDRImage
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran and Jiri Filip, February 2002

// Standard C++ headers
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <vector>
#include <algorithm>

// 3rd party library headers
#include "png.h"

// nanoGOLEM headers
#include "configg.h"
#include "basstr.h"
#include "hdrimg.h"
#include "talloc.h"

// workarounds for compatibility with libpng 1.4 and above
#if PNG_LIBPNG_VER > 10400
#define png_infopp_NULL nullptr
#define int_p_NULL nullptr
#define png_bytepp_NULL nullptr
#define png_set_gray_1_2_4_to_8 png_set_expand_gray_1_2_4_to_8
#endif

__BEGIN_GOLEM_SOURCE

// HDR file format reading and writing
typedef unsigned char RGBE[4];
#define R  0
#define G  1
#define B  2
#define E  3

#define  MINELEN  8	  // minimum scanline length for encoding
#define  MAXELEN  0x7fff  // maximum scanline length for encoding

float
ConvertComponent(int expo, int val)
{
  float v = val / 256.0f;
  float d = (float) pow(2, expo);
  return v * d;
}

void
WorkOnRGBE(RGBE *scan, int len, float *cols)
{
  while (len-- > 0) {
    int expo = scan[0][E] - 128;
    float s = 0.f;
    cols[0] = ConvertComponent(expo, scan[0][R]);
    s += cols[0];
    cols[1] = ConvertComponent(expo, scan[0][G]);
    s += cols[1];
    cols[2] = ConvertComponent(expo, scan[0][B]);
    s += cols[2];
    // summing to floats requires that non-zero
    // values are used as luminances. Completely
    // black value does not allow to use binary search
    // when doing importance sampling
    const float threshold = 3e-4f;
    if (s < threshold) {
      const float minValue = 0.3333f*threshold;
      cols[0] = minValue;
      cols[1] = minValue;
      cols[2] = minValue;
    }
    cols += 3;
    scan++;
  }
  return;
}

bool
OldDecrunch(RGBE *scanline, int len, FILE *file)
{
  int i;
  int rshift = 0;
  
  while (len > 0) {
    scanline[0][R] = fgetc(file);
    scanline[0][G] = fgetc(file);
    scanline[0][B] = fgetc(file);
    scanline[0][E] = fgetc(file);
    if (feof(file))
      return false;
    
    if (scanline[0][R] == 1 &&
	scanline[0][G] == 1 &&
	scanline[0][B] == 1) {
      for (i = scanline[0][E] << rshift; i > 0; i--) {
	memcpy(&scanline[0][0], &scanline[-1][0], 4);
	scanline++;
	len--;
      }
      rshift += 8;
    }
    else {
      scanline++;
      len--;
      rshift = 0;
    }
  }
  return true;
}

bool
decrunch(RGBE *scanline, int len, FILE *file)
{
  int  i, j;
  
  if (len < MINELEN || len > MAXELEN)
    return OldDecrunch(scanline, len, file);
  
  i = fgetc(file);
  if (i != 2) {
    fseek(file, -1, SEEK_CUR);
    return OldDecrunch(scanline, len, file);
  }
  
  scanline[0][G] = fgetc(file);
  scanline[0][B] = fgetc(file);
  i = fgetc(file);
  
  if (scanline[0][G] != 2 || scanline[0][B] & 128) {
    scanline[0][R] = 2;
    scanline[0][E] = i;
    return OldDecrunch(scanline + 1, len - 1, file);
  }

  // read each component
  for (i = 0; i < 4; i++) {
    for (j = 0; j < len; ) {
      unsigned char code = fgetc(file);
      if (code > 128) { // run
	code &= 127;
	unsigned char val = fgetc(file);
	while (code--)
	  scanline[j++][i] = val;
      }
      else  {	// non-run
	while(code--)
	  scanline[j++][i] = fgetc(file);
      }
    }
  }

  return feof(file) ? false : true;
}


/* offsets to red, green, and blue components in a data (float) pixel */
#define RGBE_DATA_RED    0
#define RGBE_DATA_GREEN  1
#define RGBE_DATA_BLUE   2
/* number of floats per pixel */
#define RGBE_DATA_SIZE   3
/* flags indicating which fields in an rgbe_header_info are valid */
#define RGBE_VALID_PROGRAMTYPE 0x01
#define RGBE_VALID_GAMMA       0x02
#define RGBE_VALID_EXPOSURE    0x04
/* return codes for rgbe routines */
#define RGBE_RETURN_SUCCESS  1
#define RGBE_RETURN_FAILURE -1

typedef struct {
  int valid;            /* indicate which fields are valid */
  char programtype[16]; /* listed at beginning of file to identify it 
                         * after "#?".  defaults to "RGBE" */ 
  float gamma;          /* image has already been gamma corrected with 
                         * given gamma.  defaults to 1.0 (no correction) */
  float exposure;       /* a value of 1.0 in an image corresponds to
			 * <exposure> watts/steradian/m^2. 
			 * defaults to 1.0 */
} rgbe_header_info;

enum rgbe_error_codes {
  rgbe_read_error,
  rgbe_write_error,
  rgbe_format_error,
  rgbe_memory_error,
};

/* default error routine.  change this to change error handling */
static int
RGBE_error(int rgbe_error_code, const char *msg)
{
  switch (rgbe_error_code) {
  case rgbe_read_error:
    perror("RGBE read error");
    break;
  case rgbe_write_error:
    perror("RGBE write error");
    break;
  case rgbe_format_error:
    fprintf(stderr,"RGBE bad file format: %s\n",msg);
    break;
  default:
  case rgbe_memory_error:
    fprintf(stderr,"RGBE error: %s\n",msg);
  }
  return RGBE_RETURN_FAILURE;
}//--- rgbe_error --------------------------------------------------

/* default minimal header. modify if you want more information in header */
int
RGBE_WriteHeader(FILE *fp, int width, int height, rgbe_header_info *info)
{
  const char *programtype = "RADIANCE";

  if (info && (info->valid & RGBE_VALID_PROGRAMTYPE))
    programtype = info->programtype;
  if (fprintf(fp,"#?%s\n",programtype) < 0)
    return RGBE_error(rgbe_write_error,NULL);
  /* The #? is to identify file type, the programtype is optional. */
  if (info && (info->valid & RGBE_VALID_GAMMA)) {
    if (fprintf(fp,"GAMMA=%g\n",info->gamma) < 0)
      return RGBE_error(rgbe_write_error,NULL);
  }
  if (info && (info->valid & RGBE_VALID_EXPOSURE)) {
    if (fprintf(fp,"EXPOSURE=%g\n",info->exposure) < 0)
      return RGBE_error(rgbe_write_error,NULL);
  }
  if (fprintf(fp,"FORMAT=32-bit_rle_rgbe\n\n") < 0)
    return RGBE_error(rgbe_write_error,NULL);
  if (fprintf(fp, "-Y %d +X %d\n", height, width) < 0)
    return RGBE_error(rgbe_write_error,NULL);
  return RGBE_RETURN_SUCCESS;

}//--- RGBE_WriteHeader ------------------------------------------------------

/* standard conversion from float pixels to rgbe pixels */
/* note: you can remove the "inline"s if your compiler complains about it */
void 
float2rgbe(unsigned char rgbe[4], float red, float green, float blue)
{
  float v;
  int e;

  v = red;
  if (green > v) v = green;
  if (blue > v) v = blue;
  if (v < 1e-32) {
    rgbe[0] = rgbe[1] = rgbe[2] = rgbe[3] = 0;
  }
  else {
    v = (float)(frexp(v,&e) * 256.0 / v);
    rgbe[0] = (unsigned char) (red * v);
    rgbe[1] = (unsigned char) (green * v);
    rgbe[2] = (unsigned char) (blue * v);
    rgbe[3] = (unsigned char) (e + 128);
  }
}//--- float2rgbe ------------------------------------------------

/* simple write routine that does not use run length encoding */
/* These routines can be made faster by allocating a larger buffer and
   fread-ing and fwrite-ing the data in larger chunks */
int
RGBE_WritePixels(FILE *fp, float *data, int numpixels)
{
  unsigned char rgbe[4];

  while (numpixels-- > 0) {
    float2rgbe(rgbe,data[RGBE_DATA_RED],
	       data[RGBE_DATA_GREEN],data[RGBE_DATA_BLUE]);
    data += RGBE_DATA_SIZE;
    if (fwrite(rgbe, sizeof(rgbe), 1, fp) < 1)
      return RGBE_error(rgbe_write_error,NULL);
  }
  return RGBE_RETURN_SUCCESS;
}//--- RGBE_WritePixels ----------------------------------------------------

/* The code below is only needed for the run-length encoded files. */
/* Run length encoding adds considerable complexity but does */
/* save some space.  For each scanline, each channel (r,g,b,e) is */
/* encoded separately for better compression. */

static int
RGBE_WriteBytes_RLE(FILE *fp, unsigned char *data, int numbytes)
{
#define MINRUNLENGTH 4
  int cur, beg_run, run_count, old_run_count, nonrun_count;
  unsigned char buf[2];

  cur = 0;
  while(cur < numbytes) {
    beg_run = cur;
    /* find next run of length at least 4 if one exists */
    run_count = old_run_count = 0;
    while((run_count < MINRUNLENGTH) && (beg_run < numbytes)) {
      beg_run += run_count;
      old_run_count = run_count;
      run_count = 1;
      while( (beg_run + run_count < numbytes) && (run_count < 127)
             && (data[beg_run] == data[beg_run + run_count]))
	run_count++;
    }
    /* if data before next big run is a short run then write it as such */
    if ((old_run_count > 1)&&(old_run_count == beg_run - cur)) {
      buf[0] = 128 + old_run_count;   /*write short run*/
      buf[1] = data[cur];
      if (fwrite(buf,sizeof(buf[0])*2,1,fp) < 1)
	return RGBE_error(rgbe_write_error,NULL);
      cur = beg_run;
    }
    /* write out bytes until we reach the start of the next run */
    while(cur < beg_run) {
      nonrun_count = beg_run - cur;
      if (nonrun_count > 128) 
	nonrun_count = 128;
      buf[0] = nonrun_count;
      if (fwrite(buf,sizeof(buf[0]),1,fp) < 1)
	return RGBE_error(rgbe_write_error,NULL);
      if (fwrite(&data[cur],sizeof(data[0])*nonrun_count,1,fp) < 1)
	return RGBE_error(rgbe_write_error,NULL);
      cur += nonrun_count;
    }
    /* write out next run if one was found */
    if (run_count >= MINRUNLENGTH) {
      buf[0] = 128 + run_count;
      buf[1] = data[beg_run];
      if (fwrite(buf,sizeof(buf[0])*2,1,fp) < 1)
	return RGBE_error(rgbe_write_error,NULL);
      cur += run_count;
    }
  }
  return RGBE_RETURN_SUCCESS;
#undef MINRUNLENGTH
}//--- RGBE_WriteBytes_RLE -----------------------------------------------------------

int
RGBE_WritePixels_RLE(FILE *fp, float *data, int scanline_width,
		     int num_scanlines)
{
  unsigned char rgbe[4];
  unsigned char *buffer;
  int i, err;

  if ((scanline_width < 8)||(scanline_width > 0x7fff))
    /* run length encoding is not allowed so write flat*/
    return RGBE_WritePixels(fp,data,scanline_width*num_scanlines);
  buffer = (unsigned char *)malloc(sizeof(unsigned char)*4*scanline_width);
  if (buffer == NULL) 
    /* no buffer space so write flat */
    return RGBE_WritePixels(fp,data,scanline_width*num_scanlines);
  while(num_scanlines-- > 0) {
    rgbe[0] = 2;
    rgbe[1] = 2;
    rgbe[2] = scanline_width >> 8;
    rgbe[3] = scanline_width & 0xFF;
    if (fwrite(rgbe, sizeof(rgbe), 1, fp) < 1) {
      free(buffer);
      return RGBE_error(rgbe_write_error,NULL);
    }
    for(i=0;i<scanline_width;i++) {
      float2rgbe(rgbe,data[RGBE_DATA_RED],
		 data[RGBE_DATA_GREEN],data[RGBE_DATA_BLUE]);
      buffer[i] = rgbe[0];
      buffer[i+scanline_width] = rgbe[1];
      buffer[i+2*scanline_width] = rgbe[2];
      buffer[i+3*scanline_width] = rgbe[3];
      data += RGBE_DATA_SIZE;
    }
    /* write out each of the four channels separately run length encoded */
    /* first red, then green, then blue, then exponent */
    for(i=0;i<4;i++) {
      if ((err = RGBE_WriteBytes_RLE(fp,&buffer[i*scanline_width],
				     scanline_width)) != RGBE_RETURN_SUCCESS) {
	free(buffer);
	return err;
      }
    }
  }
  free(buffer);
  return RGBE_RETURN_SUCCESS;
}//--- RGBE_WritePixels_RLE -----------------------------------------------------

int
Write_RGBE_file(const char *filename, float ***arr, int planes,
		int nr, int nc)
{
  FILE *outfp;

  if (!(outfp = fopen(filename, "wb")))
    {
      printf("Error when writing file %s !!!\n",filename);
      return -2; // error
    }

  RGBE_WriteHeader(outfp, nc, nr, NULL);

  float *imgdata = allocation1(0,planes*nc*nr);

  for(int irow=0;irow<nr;irow++)
    for(int jcol=0;jcol<nc;jcol++)
      for(int isp=0;isp<planes;isp++)
        //        imgdata[planes*((nr-irow-1)*nc + jcol)+isp] = arr[isp][irow][jcol];
        imgdata[planes*(irow*nc + jcol)+isp] = arr[isp][irow][jcol];

  int writeStatus = RGBE_WritePixels_RLE(outfp, imgdata, nc, nr);
  fclose(outfp);

  freemem1(imgdata, 0,planes*nc*nr);

  return writeStatus;
}//--- write_RGBE_file ----------------------------------------------------------



#define PI 3.14159265358979323846

/************************************************************************
 from/for Pres.cpp -> image IO definitions: -----------------------------
 ************************************************************************/
#define IMGIO_FILE_NOT_OPENED -1
#define IMGIO_NOT_A_PNG -2
#define IMGIO_ALLOCATION_PROBLEM -3
#define IMGIO_FILE_READ_PROBLEM -4
#define IMGIO_FILE_WRITE_PROBLEM -5
#define IMGIO_FILE_CONTENTS_PROBLEM -6
#define IMGIO_WRONG_PARAMETER -7

#define PNG_BYTES_TO_CHECK 4

int
ReadPNGheader(const string &filename, int *planes, int *nr, int *nc)
{
   /*! the procedure only reads basic image information from the 'filename' PNG file */

   char buf[PNG_BYTES_TO_CHECK];
   png_uint_32 width, height;
   int bit_depth, color_type, interlace_type;
   png_structp png_ptr;
   png_infop info_ptr /*, endinfo_ptr*/;
   FILE *fp = fopen(filename.c_str(), "rb");
   if (!fp)
   {
       /*printf("File cannot be opened.");*/
       return IMGIO_FILE_NOT_OPENED;
   }
   if (fread(buf, 1, PNG_BYTES_TO_CHECK, fp) != PNG_BYTES_TO_CHECK) return IMGIO_FILE_NOT_OPENED;
   if (png_sig_cmp((png_bytep)buf, (png_size_t)0, PNG_BYTES_TO_CHECK))
   {
       /*printf("Not a PNG file.");*/
       return IMGIO_NOT_A_PNG;
   }

   /* Create and initialize the png_struct with the desired error handler
    * functions.  If you want to use the default stderr and longjump method,
    * you can supply NULL for the last three parameters.  We also supply the
    * the compiler header file version, so that we know if the application
    * was compiled with a compatible version of the library.  REQUIRED
    */
   png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, (png_voidp)NULL, NULL, NULL);
   if (!png_ptr)
   {
       /*printf("Error creating PNG structures.");*/
       return IMGIO_ALLOCATION_PROBLEM;
   }

   /* Allocate/initialize the memory for image information.  REQUIRED. */
   info_ptr = png_create_info_struct(png_ptr);
   if (!info_ptr)
   {
       png_destroy_read_struct(&png_ptr,
          (png_infopp)NULL, (png_infopp)NULL);
       /*printf("Error creating PNG structures.");*/
       return IMGIO_ALLOCATION_PROBLEM;
   }
    
   /* Set error handling if you are using the setjmp/longjmp method (this is
    * the normal method of doing things with libpng).  REQUIRED unless you
    * set up your own error handlers in the png_create_read_struct() earlier.
    */
   if (setjmp(png_jmpbuf(png_ptr)))
   {
      /* Free all of the memory associated with the png_ptr and info_ptr */
      png_destroy_read_struct(&png_ptr, &info_ptr, png_infopp_NULL);
      fclose(fp);
      /* If we get here, we had a problem reading the file */
      return IMGIO_FILE_READ_PROBLEM;
   }
   
   /* Set up the input control if you are using standard C streams */
   png_init_io(png_ptr, fp);

   png_set_sig_bytes(png_ptr,PNG_BYTES_TO_CHECK);
   
   /* The call to png_read_info() gives us all of the information from the
    * PNG file before the first IDAT (image data chunk).  REQUIRED
    */
   png_read_info(png_ptr, info_ptr);
   png_get_IHDR(png_ptr, info_ptr, &width, &height, &bit_depth, &color_type, &interlace_type, int_p_NULL, int_p_NULL);
   *nc=width;
   *nr=height;
       
   if((color_type == PNG_COLOR_TYPE_GRAY) || (color_type == PNG_COLOR_TYPE_GRAY_ALPHA)) *planes=1;
   else
   if((color_type == PNG_COLOR_TYPE_RGB) || (color_type == PNG_COLOR_TYPE_RGB_ALPHA) || (color_type == PNG_COLOR_TYPE_PALETTE)) *planes=3;
   else return -1; /* not a usable color_type */

   /* Free all of the memory associated with the png_ptr and info_ptr */
   png_destroy_read_struct(&png_ptr, &info_ptr, png_infopp_NULL);
   fclose(fp);
   return 0;
} /* readPNGheader ------------------------------------------------------ */

int
ReadPNGfile3D(const string &filename,float ***arr, int planes,
	      int nr, int nc, int oneplane)
{
  /*! \param 'planes' should be 1 for gray-scale or 3 for RGB */
  /*! \note this filter will automatically convert between rgb and grayscale
    images in case of need, depending on the user-specified 'planes' value */
  
  /*! \warning Gamma information is currently ignored !
    \warning Alpha information is currently ignored ! (black background substituted) */
  
  char buf[PNG_BYTES_TO_CHECK];
  png_uint_32 width, height;
  int bit_depth, color_type, interlace_type;
  int number_passes; /* for interlaced images */
  int x,y,xi;
  png_structp png_ptr;
  png_infop info_ptr /*, endinfo_ptr*/;
  png_bytep row;
  png_bytepp rows;
  float **arr2 = 0;
  unsigned short byte1, byte2;

  //union {unsigned short ipix; char cpix[2];} bitconv;
  FILE *fp = fopen(filename.c_str(), "rb");

  if(planes==1) {/* grayscale image */
    if(oneplane<0) arr2=(float**)arr; else arr2 = arr[oneplane];
  }

  if (!fp) {
    /*printf("File cannot be opened.");*/
    return IMGIO_FILE_NOT_OPENED;
  }
  if (fread(buf, 1, PNG_BYTES_TO_CHECK, fp) != PNG_BYTES_TO_CHECK)
    return IMGIO_FILE_NOT_OPENED;
  if (png_sig_cmp((png_bytep)buf, (png_size_t)0, PNG_BYTES_TO_CHECK)) {
    /*printf("Not a PNG file.");*/
    return IMGIO_NOT_A_PNG;
  }

  /* Create and initialize the png_struct with the desired error handler
   * functions.  If you want to use the default stderr and longjump method,
   * you can supply NULL for the last three parameters.  We also supply the
   * the compiler header file version, so that we know if the application
   * was compiled with a compatible version of the library.  REQUIRED
   */
  png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING,
				   (png_voidp)NULL, NULL, NULL);
  if (!png_ptr) {
    /*printf("Error creating PNG structures.");*/
    return IMGIO_ALLOCATION_PROBLEM;
  }
  
  /* Allocate/initialize the memory for image information.  REQUIRED. */
  info_ptr = png_create_info_struct(png_ptr);
  if (!info_ptr) {
    png_destroy_read_struct(&png_ptr,
			    (png_infopp)NULL, (png_infopp)NULL);
    /*printf("Error creating PNG structures.");*/
    return IMGIO_ALLOCATION_PROBLEM;
  }

  /* Set error handling if you are using the setjmp/longjmp method (this is
   * the normal method of doing things with libpng).  REQUIRED unless you
   * set up your own error handlers in the png_create_read_struct() earlier.
   */
  if (setjmp(png_jmpbuf(png_ptr))) {
    /* Free all of the memory associated with the png_ptr and info_ptr */
    png_destroy_read_struct(&png_ptr, &info_ptr, png_infopp_NULL);
    fclose(fp);
    /* If we get here, we had a problem reading the file */
    return IMGIO_FILE_READ_PROBLEM;
  }

  /* Set up the input control if you are using standard C streams */
  png_init_io(png_ptr, fp);

  png_set_sig_bytes(png_ptr,PNG_BYTES_TO_CHECK);
  
  /* The call to png_read_info() gives us all of the information from the
   * PNG file before the first IDAT (image data chunk).  REQUIRED
   */
  png_read_info(png_ptr, info_ptr);
  png_get_IHDR(png_ptr, info_ptr, &width, &height, &bit_depth, &color_type,
	       &interlace_type, int_p_NULL, int_p_NULL);
  
  /* tell libpng to strip 16 bit/color files down to 8 bits/color */
  /*png_set_strip_16(png_ptr);*/
  
  /* this would simply remove the alpha channel, if present.
     Currently the alpha-data removal is postponed to the time of raw
     data transformation to ***arr. This needs more memory (by 33%), but
     allows further alpha processing if needed. */
  /*if (color_type & PNG_COLOR_MASK_ALPHA) png_set_strip_alpha(png_ptr);*/
  
  /* Extract multiple pixels with bit depths of 1, 2, and 4 from a single
   * byte into separate bytes (useful for paletted and grayscale images).
   */
  png_set_packing(png_ptr);
  
  /* in case the png file is grayscale and the user requests a rgb image,
     transform automatically the image data */
  if ((planes==3)&&(color_type == PNG_COLOR_TYPE_GRAY ||
		    color_type == PNG_COLOR_TYPE_GRAY_ALPHA))
    png_set_gray_to_rgb(png_ptr);

  /* in case the png file is rgb and the user requests a grayscale image,
     transform automatically the image data */
  if ((planes==1)&&(color_type == PNG_COLOR_TYPE_RGB ||
		    color_type == PNG_COLOR_TYPE_RGB_ALPHA))
    png_set_rgb_to_gray_fixed(png_ptr, 1 /*error_action=silent*/, -1 /*red_weight=default*/, -1 /*green_weight=default*/);

  if((width!=(unsigned int)nc)||(height!=(unsigned int)nr)) {
    /* Free all of the memory associated with the png_ptr and info_ptr */
    png_destroy_read_struct(&png_ptr, &info_ptr, png_infopp_NULL);
    fclose(fp);
    /* some file properties are different from the expected */
    return IMGIO_FILE_CONTENTS_PROBLEM;
  }

  /* Expand paletted colors into true RGB triplets */
  if ((planes==3)&&(color_type == PNG_COLOR_TYPE_PALETTE)) /* \warning (planes==3) added by PS, otherwise gray images become corrupted during input*/
    png_set_palette_to_rgb(png_ptr);

  /* Expand grayscale images to the full 8 bits from 1, 2, or 4 bits/pixel */
  if (color_type == PNG_COLOR_TYPE_GRAY && bit_depth < 8)
    png_set_gray_1_2_4_to_8(png_ptr);
  
  /* Turn on interlace handling.  REQUIRED if you are not using
   * png_read_image().  To see how to handle interlacing passes,
   * see the png_read_row() method below:
   */
  number_passes = png_set_interlace_handling(png_ptr);
  
  /* Optional call to gamma correct and add the background to the palette
   * and update info structure.  REQUIRED if you are expecting libpng to
   * update the palette for you (ie you selected such a transform above).
   */
  png_read_update_info(png_ptr, info_ptr);

  if(number_passes==1) { /* one row at a time - less memory needed */
    /* Allocate the memory to hold one row at a time using the fields of info_ptr. */
    row = (png_byte *)png_malloc(png_ptr, png_get_rowbytes(png_ptr,info_ptr));
    if (row==NULL) {
      /* Free all of the memory associated with the png_ptr and info_ptr */
      png_destroy_read_struct(&png_ptr, &info_ptr, png_infopp_NULL);
      fclose(fp);
      return IMGIO_ALLOCATION_PROBLEM;
    }

    for (y = 0; y < (int)height; y++) {
      png_read_rows(png_ptr, &row, png_bytepp_NULL, 1);
      
      /* copying to arr */
      if (planes==1) { /* one plane */
	xi=0;
	for(x=0;x<(int)width;x++)
	  if (bit_depth==16) {
	    byte1 = row[xi++];
	    byte2 = row[xi++];
	    byte2 <<= 8;
	    arr2[y][x]=(float)(byte1+byte2);
	       
	    /* currently the (possible) alpha channel is ignored, i.e. the
	       resulting image data looks like combined with black backgroung */
	    if(color_type & PNG_COLOR_MASK_ALPHA) xi+=2;
	  }
	  else {
	    arr2[y][x]=(float)row[xi++];
	    /* currently the (possible) alpha channel is ignored, i.e. the
	       resulting image data looks like combined with black backgroung */
	    if(color_type & PNG_COLOR_MASK_ALPHA) xi++;
	  }
      }
      else { /* 3 planes */
	xi=0;
	for (x=0;x<(int)width;x++)
	  if (bit_depth==16) {
	    byte1 = row[xi++];
	    byte2 = row[xi++];
	    byte2 <<= 8;
	    arr[0][y][x]=(float)(byte1+byte2);
	    byte1 = row[xi++];
	    byte2 = row[xi++];
	    byte2 <<= 8;
	    arr[1][y][x]=(float)(byte1+byte2);
	    byte1 = row[xi++];
	    byte2 = row[xi++];
	    byte2 <<= 8;
	    arr[2][y][x]=(float)(byte1+byte2);
	    
	    /*
	      bitconv.cpix[0]=row[xi++];
	      bitconv.cpix[1]=row[xi++];
	      arr[0][y][x]=(float)bitconv.ipix;
	      bitconv.cpix[0]=row[xi++];
	      bitconv.cpix[1]=row[xi++];
	      arr[1][y][x]=(float)bitconv.ipix;
	      bitconv.cpix[0]=row[xi++];
	      bitconv.cpix[1]=row[xi++];
	      arr[2][y][x]=(float)bitconv.ipix;
	    */
	    /* currently the (possible) alpha channel is ignored, i.e. the
	       resulting image data looks like combined with black backgroung */
	       if(color_type & PNG_COLOR_MASK_ALPHA) xi+=2;
	  } else {
	    arr[0][y][x]=(float)row[xi++];
	    arr[1][y][x]=(float)row[xi++];
	    arr[2][y][x]=(float)row[xi++];
	    /* currently the (possible) alpha channel is ignored, i.e. the
	       resulting image data looks like combined with black backgroung */
	    if(color_type & PNG_COLOR_MASK_ALPHA) xi++;
	  }
      }
    }
    png_free(png_ptr,row);
  }
  else { /* interlaced images must be read all at once */
    rows = (png_byte **)png_malloc(png_ptr, height*sizeof(png_bytep));
    if (rows==NULL) {
      /* Free all of the memory associated with the png_ptr and info_ptr */
      png_destroy_read_struct(&png_ptr, &info_ptr, png_infopp_NULL);
      fclose(fp);
      return IMGIO_ALLOCATION_PROBLEM;
    }
    for (y = 0; y < (int)height; y++) {
      rows[y] = (png_byte *)png_malloc(png_ptr, png_get_rowbytes(png_ptr,info_ptr));
      if (rows[y]==NULL) {
	/* Free all of the memory associated with the png_ptr and info_ptr */
	png_destroy_read_struct(&png_ptr, &info_ptr, png_infopp_NULL);
	fclose(fp);
	return IMGIO_ALLOCATION_PROBLEM;
      }
    }
     
    png_read_image(png_ptr, rows);
     
    /* copying to arr */
    if (planes==1) { /* one plane */
      for(y=0;y<(int)height;y++) {
	xi=0;
	for(x=0;x<(int)width;x++)
	  if(bit_depth==16) {
	    byte1 = rows[y][xi++];
	    byte2 = rows[y][xi++];
	    byte2 <<= 8;
	    arr2[y][x]=(float)(byte1+byte2);
	    /*
	      bitconv.cpix[0]=rows[y][xi++];
	      bitconv.cpix[1]=rows[y][xi++];
	      arr2[y][x]=(float)bitconv.ipix;
	    */
	    /* currently the (possible) alpha channel is ignored, i.e. the
	       resulting image data looks like combined with black backgroung */
	    if(color_type & PNG_COLOR_MASK_ALPHA) xi+=2;
	  }
	  else {
	    arr2[y][x]=(float)rows[y][xi++];
	    /* currently the (possible) alpha channel is ignored, i.e. the
	       resulting image data looks like combined with black backgroung */
	    if(color_type & PNG_COLOR_MASK_ALPHA) xi++;
	  }
      }
    }
    else { /* 3 planes */
      for(y=0;y<(int)height;y++) {
	xi = 0;
	for(x=0;x<(int)width;x++)
	  if (bit_depth==16) {
	    byte1 = rows[y][xi++];
	    byte2 = rows[y][xi++];
	    byte2 <<= 8;
	    arr[0][y][x]=(float)(byte1+byte2);
	    byte1 = rows[y][xi++];
	    byte2 = rows[y][xi++];
	    byte2 <<= 8;
	    arr[1][y][x]=(float)(byte1+byte2);
	    byte1 = rows[y][xi++];
	    byte2 = rows[y][xi++];
	    byte2 <<= 8;
	    arr[2][y][x]=(float)(byte1+byte2);
	    /*
	      bitconv.cpix[0]=rows[y][xi++];
	      bitconv.cpix[1]=rows[y][xi++];
	      arr[0][y][x]=(float)bitconv.ipix;
	      bitconv.cpix[0]=rows[y][xi++];
	      bitconv.cpix[1]=rows[y][xi++];
	      arr[1][y][x]=(float)bitconv.ipix;
	      bitconv.cpix[0]=rows[y][xi++];
	      bitconv.cpix[1]=rows[y][xi++];
	      arr[2][y][x]=(float)bitconv.ipix;
	    */
	    /* currently the (possible) alpha channel is ignored, i.e. the
	       resulting image data looks like combined with black backgroung */
	     if(color_type & PNG_COLOR_MASK_ALPHA) xi+=2;
	  }
	  else {
	    arr[0][y][x]=(float)rows[y][xi++];
	    arr[1][y][x]=(float)rows[y][xi++];
	    arr[2][y][x]=(float)rows[y][xi++];
	    /* currently the (possible) alpha channel is ignored, i.e. the
	       resulting image data looks like combined with black backgroung */
	    if(color_type & PNG_COLOR_MASK_ALPHA) xi++;
	  }
      }
    }

    for (y = 0; y < (int)height; y++)
      png_free(png_ptr,rows[y]);
    png_free(png_ptr,rows);
  }

  /* read rest of file, and get additional chunks in info_ptr - REQUIRED */
  png_read_end(png_ptr, info_ptr);
  
  /* At this point you have read the entire image */
  /* clean up after the read, and free any memory allocated - REQUIRED */
  png_destroy_read_struct(&png_ptr, &info_ptr, png_infopp_NULL);
  
  fclose(fp);
  return 1;
} /* ReadPNGfile3D ------------------------------------------------------ */

/**************************************************************************/
int
WritePNGfile(const char *filename,float ***arr,int planes, int nr,
	     int nc, int oneplane, int sixteenbit)
{
  /*! \param 'planes' should be 1 for gray-scale or 3 for RGB */
  FILE *fp;
  png_structp png_ptr;
  png_infop info_ptr;
  /*png_colorp palette;*/
  png_color_8 sig_bit;
  png_bytep row;
  int x,y,xi;
  float **arr2 = 0;
  int fileplanes = 0;
  int true_bit_depth = 0;
  //   union {unsigned short ipix; char cpix[2];} bitconv;
  unsigned short byte1,byte2;
  
  if (sixteenbit) true_bit_depth=16; else true_bit_depth=8;
  if (planes==1) { /* grayscale image */
    if(oneplane<0) arr2=(float**)arr; else arr2 = arr[oneplane];
    fileplanes=1;
  } else fileplanes=3;

  if(planes<0) return IMGIO_WRONG_PARAMETER;
  /* if((planes!=1)&&(planes!=3)) return IMGIO_WRONG_PARAMETER; */
  /* \warning PS changed the behaviour so that for nonstandard planes an RGB image is stored
     containing simply the first 3 planes (2 in case of planes==2) */
  
  /* open the file */
  fp = fopen(filename, "wb");
  if (fp == NULL) return IMGIO_FILE_NOT_OPENED;
  
  /* Create and initialize the png_struct with the desired error handler
   * functions.  If you want to use the default stderr and longjump method,
   * you can supply NULL for the last three parameters.  We also check that
   * the library version is compatible with the one used at compile time,
   * in case we are using dynamically linked libraries.  REQUIRED.
   */
  png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, (png_voidp)NULL, NULL, NULL);
  if (png_ptr == NULL) {
    fclose(fp);
    return IMGIO_ALLOCATION_PROBLEM;
  }

  /* Allocate/initialize the image information data.  REQUIRED */
  info_ptr = png_create_info_struct(png_ptr);
  if (info_ptr == NULL) {
    fclose(fp);
    png_destroy_write_struct(&png_ptr,  png_infopp_NULL);
    return IMGIO_ALLOCATION_PROBLEM;
  }

  /* Set error handling.  REQUIRED if you aren't supplying your own
   * error handling functions in the png_create_write_struct() call.
   */
  if (setjmp(png_jmpbuf(png_ptr))) {
    /* If we get here, we had a problem reading the file */
    fclose(fp);
    png_destroy_write_struct(&png_ptr, &info_ptr);
    return IMGIO_FILE_WRITE_PROBLEM;
  }

  /* set up the output control if you are using standard C streams */
  png_init_io(png_ptr, fp);
  
  /* Set the image information here.  Width and height are up to 2^31,
   * bit_depth is one of 1, 2, 4, 8, or 16, but valid values also depend on
   * the color_type selected. color_type is one of PNG_COLOR_TYPE_GRAY,
   * PNG_COLOR_TYPE_GRAY_ALPHA, PNG_COLOR_TYPE_PALETTE, PNG_COLOR_TYPE_RGB,
   * or PNG_COLOR_TYPE_RGB_ALPHA.  interlace is either PNG_INTERLACE_NONE or
   * PNG_INTERLACE_ADAM7, and the compression_type and filter_type MUST
   * currently be PNG_COMPRESSION_TYPE_BASE and PNG_FILTER_TYPE_BASE. REQUIRED
   */
  if(planes==1)
    png_set_IHDR(png_ptr, info_ptr, nc, nr, true_bit_depth, PNG_COLOR_TYPE_GRAY,
		 PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE,
		 PNG_FILTER_TYPE_BASE);
  else
    png_set_IHDR(png_ptr, info_ptr, nc, nr, true_bit_depth, PNG_COLOR_TYPE_RGB,
		 PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE,
		 PNG_FILTER_TYPE_BASE);
  
  /*! \warning - not finished ??.. */

  sig_bit.red = true_bit_depth; /*true_red_bit_depth;*/
  sig_bit.green = true_bit_depth; /*true_green_bit_depth;*/
  sig_bit.blue = true_bit_depth; /*true_blue_bit_depth;*/
  sig_bit.gray = true_bit_depth; /*true_gray_bit_depth;*/
  png_set_sBIT(png_ptr, info_ptr, &sig_bit);
  
  /* Optional gamma chunk is strongly suggested if you have any guess
   * as to the correct gamma of the image.
   */
  /*png_set_gAMA(png_ptr, info_ptr, gamma);*/
  

  /* Write the file header information.  REQUIRED */
  png_write_info(png_ptr, info_ptr);

  /* Once we write out the header, the compression type on the text
   * chunks gets changed to PNG_TEXT_COMPRESSION_NONE_WR or
   * PNG_TEXT_COMPRESSION_zTXt_WR, so it doesn't get written out again
   * at the end.
   */


  /* The easiest way to write the image (you may have a different memory
   * layout, however, so choose what fits your needs best).  You need to
   * use the first method if you aren't handling interlacing yourself.
   */
  if(sixteenbit) row = (png_bytep)png_malloc(png_ptr, (long)nc*(long)fileplanes*(long)2);
  else row = (png_bytep)png_malloc(png_ptr, (long)nc*(long)fileplanes);
  if (row==NULL) {
    /* Free all of the memory associated with the png_ptr and info_ptr */
    png_destroy_write_struct(&png_ptr, &info_ptr);
    fclose(fp);
    return IMGIO_ALLOCATION_PROBLEM;
  }
  /* Write 1 row at a time. */
  for (y = 0; y < nr; y++) {
    if(planes==1) {
      for(x=0;x<nc;x++)
	if (sixteenbit) {
	  byte1 = byte2 = (unsigned short)arr2[y][x];
	  byte1 >>= 8;
	  byte2 <<= 8;
	  byte2 >>= 8;
	  row[2*x] = (unsigned char)(byte2);
	  row[2*x+1] = (unsigned char)(byte1);
	  /*     			
	     bitconv.ipix=(unsigned short)arr2[y][x];
      	     row[2*x]=bitconv.cpix[0];
      	     row[2*x+1]=bitconv.cpix[1];
	  */
	} else row[x]=(unsigned char)arr2[y][x];
      }
    else {
      xi=0;
      for(x=0;x<nc;x++)
	if(sixteenbit) {
	  byte1 = byte2 = (unsigned short)arr[0][y][x];
	  byte1 >>= 8;
	  byte2 <<= 8;
	  byte2 >>= 8;
	  row[xi++] = (unsigned char)(byte2);
	  row[xi++] = (unsigned char)(byte1);
	  byte1 = byte2 = (unsigned short)arr[1][y][x];
	  byte1 >>= 8;
	  byte2 <<= 8;
	  byte2 >>= 8;
	  row[xi++] = (unsigned char)(byte2);
	  row[xi++] = (unsigned char)(byte1);
	  
	  /*
	    bitconv.ipix=(unsigned short)arr[0][y][x];
	    row[xi++]=bitconv.cpix[0];
	    row[xi++]=bitconv.cpix[1];
	    bitconv.ipix=(unsigned short)arr[1][y][x];
	    row[xi++]=bitconv.cpix[0];
	    row[xi++]=bitconv.cpix[1];
	  */
	  if (planes>=3) {
            byte1 = byte2 = (unsigned short)arr[2][y][x];
	    byte1 >>= 8;
            byte2 <<= 8;
            byte2 >>= 8;
	    row[xi++] = (unsigned char)(byte2);
	    row[xi++] = (unsigned char)(byte1);
	    /*
	      bitconv.ipix=(unsigned short)arr[2][y][x];
	      row[xi++]=bitconv.cpix[0];
	      row[xi++]=bitconv.cpix[1];
	    */
	  } else {row[xi++]=255; row[xi++]=255;}
	} else {
	  row[xi++]=(unsigned char)arr[0][y][x];
	  row[xi++]=(unsigned char)arr[1][y][x];
	  if(planes>=3) row[xi++]=(unsigned char)arr[2][y][x];
	  else row[xi++]=255;
	}
    }
    png_write_rows(png_ptr, &row, 1);
  }
  png_free(png_ptr,row);

  /* You can write optional chunks like tEXt, zTXt, and tIME at the end
   * as well.  Shouldn't be necessary in 1.1.0 and up as all the public
   * chunks are supported and you can use png_set_unknown_chunks() to
   * register unknown chunks into the info structure to be written out.
   */

  /* It is REQUIRED to call this to finish writing the rest of the file */
  png_write_end(png_ptr, info_ptr);
  
  
  /*! \warning - to be checked ... */
  /* Similarly, if you png_malloced any data that you passed in with
     png_set_something(), such as a hist or trans array, free it here,
     when you can be sure that libpng is through with it. */
  /*png_free(png_ptr, trans);*/
  /*trans=NULL;*/
  
  /* clean up after the write, and free any memory allocated */
  png_destroy_write_struct(&png_ptr, &info_ptr);
  
  /* close the file */
  fclose(fp);
  return 1;
} /* writePNGfile ---------------------------------------------------- */

bool
CHDRImage::LoadPNG(const string &fileName)
{
  // First, read header
  int planesCnt;
  int readStatus = ReadPNGheader(fileName.c_str(), &planesCnt, &height, &width);
  if (readStatus < 0) {
    width = height = 0;
    FATAL << "Wrong environment map file " << fileName << endl;
    FATAL << "!!! Exiting ...\n" << endl;
    return false; // error
  }

  // for PNG
  float ***imagefloat = 0; 
  // allocation of PNG env. image
  imagefloat = allocation3(0, planesCnt, 0, height, 0, width);
  assert(imagefloat);
  assert(planesCnt == 3);
  
  // reading PNG env. map file
  readStatus = ReadPNGfile3D(fileName, imagefloat, planesCnt, height, width, 1);
  STATUS << "Reading PNG image " << fileName << " " << readStatus << endl;
  STATUS << "PNG loader width = " << width << " height = " << height << endl;
  Allocate(width, height);
  assert(cols);

  // Copy the data from PNG representation
  for (int y=0; y < height; y++) {
    for (int x=0; x < width; x++) {
      int ww = (width * y + x)*3;
      float s = 0.f;
      for (int isp=0; isp < planesCnt; isp++) {
	float val = imagefloat[isp][y][x];
	cols[ww+isp] = val;
	s += val;
      }
      // summing to floats requires that non-zero
      // values are used as luminances. Completely
      // black value does not allow to use binary search
      // when doing importance sampling
      const float threshold = 1e-4f;
      if (s < threshold) {
	const float minValue = 0.3333f*threshold;
	cols[ww+0] = minValue;
	cols[ww+1] = minValue;
	cols[ww+2] = minValue;
      }
    } // for jcol
  } // for irow

  // deallocate the data as read by PNG reader
  freemem3(imagefloat, 0, planesCnt, 0, height, 0, width);

  gname = ExtractFileName(fileName);
  return true; // OK
}

bool
CHDRImage::SavePNG(const string &fileName)
{
  int planesCnt = 3;
  float ***imagefloat = allocation3(0, planesCnt, 0, height, 0, width);

  // Copy the data from PNG representation
  for (int y=0; y < height; y++) {
    for (int x=0; x < width; x++) {
      int ww = (width * y + x)*3;
      for (int isp=0; isp < planesCnt; isp++) {
	// set it to the output format as required by RGBE_WRITE
	imagefloat[isp][y][x] = cols[ww+isp];
      }
    } // for jcol
  } // for irow
  
  int statusWrite = WritePNGfile(fileName.c_str(), imagefloat,
				 planesCnt, height, width, 0, 1);
  STATUS << "HDR write " << fileName << " " << statusWrite << endl;
  if (statusWrite < 0) {
    return true; // ERROR
  }

  // deallocate the data needed for writing
  freemem3(imagefloat, 0, planesCnt, 0, height, 0, width);

  return false; // OK
}

bool
CHDRImage::LoadHDR(const string &fileName)
{
  int i;
  char str[200];
  FILE *file;

  file = fopen(fileName.c_str(), "rb");
  if (!file)
    return false; // error

  size_t tmp = fread(str, 10, 1, file);
  tmp = tmp;
  //char *tt = fgets(str, 14, file);
  if (!((memcmp(str, "#?RADIANCE", 10)==0)||(memcmp(str, "#?RGBE", 6)==0))) {
    fclose(file);
    return false; // error
  }

  fseek(file, 1, SEEK_CUR);

  //char cmd[200];
  i = 0;
  char c = 0, oldc;
  while(true) {
    oldc = c;
    c = fgetc(file);
    if (c == 0xa && oldc == 0xa)
      break;
    //cmd[i++] = c;
  }

  char reso[200];
  i = 0;
  while(true) {
    c = fgetc(file);
    reso[i++] = c;
    if (c == 0xa)
      break;
  }

  int w = 0, h = 0;
  if (sscanf(reso, "-Y %d +X %d", &h, &w) != 2) {
    fclose(file);
    return false; // error
  }
  
  this->width = w;
  this->height = h;  
  //STATUS << "Reading HDR image " << fileName << endl;
  //STATUS << "HDR loader width = " << width << " height = " << height << endl;
  Allocate(width, height);
  assert(this->cols);
  float *localcols = cols;
  
  RGBE *scanline = new RGBE[w * 4];
  assert(scanline);
  if (!scanline) {
    fclose(file);
    return false; // error
  }
  
  // convert image 
  for (int y = h - 1; y >= 0; y--) {
    if (decrunch(scanline, w, file) == false)
      break;
    WorkOnRGBE(scanline, w, localcols);
    localcols += w * 3;
  }
  
  delete [] scanline;
  fclose(file);

  gname = ExtractFileName(fileName);

  return true; // OK
}

bool
CHDRImage::SaveHDR(const string &fileName, bool /*verbose*/)
{
  string fileNameReal;
  // saving in HDR format by Greg Ward
  fileNameReal = ChangeFilenameExtension(string(fileName), string("hdr"));
  int planesCnt = 3;
  float ***imagefloat = allocation3(0, planesCnt, 0, height, 0, width);
  for (int y=0; y < height; y++) {
    for (int x=0; x < width; x++) {
      int ww = (width * y + x)*3;
      for (int isp=0; isp < planesCnt; isp++) {
	// set it to the output format as required by RGBE_WRITE
	imagefloat[isp][y][x] = cols[ww+isp];
      }
    } // for jcol
  } // for irow
  
  int statusWrite = Write_RGBE_file(fileNameReal.c_str(), imagefloat,
				    planesCnt, height, width);
  printf("Writing to HDR file %s: %d\n", fileNameReal.c_str(), statusWrite);
  if (statusWrite < 0) {
    return true; // ERROR
  }
  // deallocate the data needed for writing
  freemem3(imagefloat, 0, planesCnt, 0, height, 0, width);
  return false; // OK
}

__END_GOLEM_SOURCE

