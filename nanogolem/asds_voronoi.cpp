#include "asds_voronoi.h"
#include <vector>
#include <array>

void CASDS_Voronoi::ProvideID(std::ostream& app) {

}

void CASDS_Voronoi::TraversalReset() {

}


void CASDS_Voronoi::BuildUp(const CObjectList& objlist) {
	vector<CVector3D> particles(objlist.size());
	for (auto& o : objlist) {
		CVector3D c;
		o->GetBox().ComputeCentroid(c);
		particles.push_back(c);
	}
	CreateBoundingSimplex(particles);
	CVector3D pcentroid = { 0,0,0 };
	centroid = { 0,0,0 };
	for (const auto& p : boundingSimplex) {
		centroid += p;
	}
	centroid /= boundingSimplex.size();
	centralVertex = Vertex(boundingSimplex);
}

float dot(const CVector3D& vec1, const CVector3D& vec2) {
	return vec1.x * vec2.x + vec1.y * vec2.y + vec1.z * vec2.z;
}

bool gem3d(array<CVector3D, 3> A, CVector3D b, CVector3D &x) {
	int perm[3] = {0,1,2};
	for (int i = 0; i < 3; ++i) {
		float absmax = -1.f;
		int jmax;
		float max;
		for (int j = i; j < 3; ++j) {
			float v = A[perm[j]][i];
			float a = fabs(v);
			if (a > absmax) {
				max = v; absmax = a; jmax = j;
			}
		}
		if (absmax < FLT_EPSILON) return false;
		int t = perm[i]; perm[i] = jmax; perm[jmax] = t;
		for (int j = i + 1; j < 3; ++j) {
			float scal = A[perm[j]][i] / max;
			A[perm[j]] -= A[jmax] * scal;
			b[perm[j]] -= b[jmax] * scal;
		}
	}
	for (int i = 2; i >= 0; --i) {
		for (int j = 2; j > i; --j) {
			b[perm[i]] -= A[perm[i]][j] * x[j];
		}
		x[i] = b[perm[i]] / A[perm[i]][i];
	}
	return true;
}

CVector3D testGem() {
	array<CVector3D, 3> A = { CVector3D{1,-1,0}, {1,1,-1}, {1,1,1} };
	CVector3D b = { 1,2,3 };
	CVector3D x;
	gem3d(A, b, x);
	return x; // 1.75, 0.75, 0.5
}

void CASDS_Voronoi::CreateBoundingSimplex(const vector<CVector3D> &particles) {
	array<CVector3D, 4> normals = {
		CVector3D{1.f,1.f,1.f},
		CVector3D{-1.f, 1.f, 1.f},
		CVector3D{0.f, -1.f, 1.f}, 
		CVector3D{0.f, 0.f, -1.f} 
	};
	normals[0].Normalize(), normals[1].Normalize(),
		normals[2].Normalize(), normals[3].Normalize();
	vector<float> dmin(4);
	vector<float> dmax(4);
	bool first = true;
	for (int i = 0; i < 4; ++i) {
		for (auto& p : particles) {
			float dist = dot(p, normals[i]);
			if (first || dist > dmax[i]) dmax[i] = dist;
			if (first || dist < dmax[i]) dmin[i] = dist;
		}
		first = false;
	}
	for (int i = 0; i < 4; ++i) { // some extra offset 
		dmax[i] += (dmax[i] - dmin[i]) * 0.5f;
	}
	array<CVector3D, 4> A;
	for (int i = 0; i < 4; ++i) {
		array<CVector3D, 3> A;
		CVector3D b;
		for (int j = 0; j < 3; ++j) {
			int id = (i + j) % 4;
			A[j] = normals[id];
			b[j] = dmax[id];
		}
		gem3d(A, b, boundingSimplex[i]);
	}
}

CASDS_Voronoi::Vertex::Vertex(array<CVector3D, 4> simplex) {

}

void CASDS_Voronoi::AddRealParticle(const Particle& p) {

}

const CObject3D* CASDS_Voronoi::FindNearestI(CRay& ray, CHitPointInfo& info) {
	return nullptr;
}