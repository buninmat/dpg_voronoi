// ================================================================
//
// basstr.h
//     Basic operations/functions with strings
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, 2000.

#ifndef __BASSTR_H__
#define __BASSTR_H__

// nanoGOLEM headers
#include "configh.h"

// Standard headers
#include <string>

__BEGIN_GOLEM_HEADER

// forward declarations

// ----------------------------------------------------------------------
// String functions for filenames

/// Puts the specified number of spaces out to given stream.
/** 
    Used for indentation during CObject3D::Describe().
*/
void
Indent(ostream &app, int ind);

/// Prefix filename by a given directory name. 
/**
   E.g. "aa.out","ref" makes "ref/aa.out", but "stats/aa.out",
   "ref" makes "stats/ref/aa.out".
 */
void
PrefixNameByDir(const string &filename, const string &dirname,
		string &outputName);

/// Prefix filename extension by a given extPrefix.
/**
   For example the filename "data/tetra.rgb" and extPrefix "0102" makes
   the output filename "data/tetra0102.rgb"
   If no extension is found, then extPrefix is simply appended to the
   file name, e.g. for "balls" makes "balls0102"
*/
void
PrefixExtensionByName(const string &filename, const string &extPrefix,
		      string &outputName);

/// A version of PrefixExtensionByName which returns the result by value.
inline string 
PrefixExtensionByName(const string &filename, const string &extPrefix)
{
  string outputName;
  PrefixExtensionByName(filename,extPrefix,outputName);
  return outputName;
}

/// Adding/replacing the extension of the filename.
/**
   @param filename  (in) filename to be changed, e.g. "tetra.rgb"
   @param newExtension  (in) newExtension added/replaced  (e.g. "vis", "dbg",
                             "rgb" etc.)
   @param outputName  (out) filename with replaced extension 

   (e.g. "tetra.rgb" makes "tetra.dbg" and "aaaaa/bbb" makes "aaaaa/bbb.dbg").
*/
void
ChangeFilenameExtension(const string &filename, const string &newExt,
			string &outputName);

/// A version of ChangeFilenameExtension which returns the result by value.
inline string
ChangeFilenameExtension(const string &filename, const string &newExt)
{
  string outputName;
  ChangeFilenameExtension(filename,newExt,outputName);
  return outputName;
}

/// Removes path from filename,
// for example the filename "data/tetra.rgb" gets "tetra.rgb"
void
RemovePathFromFilename(const string &filename, string &outputName);

// Removes filename from the whole path
// for example the filename "A/data/tetra.rgb" gets "A/data"
void
RemoveFilenameOnly(const string &filename, string &outputDir);

/// Extracts the only filename, excludes the whole path, e.g. "aaaa/bbb.dbg" -> "bbb"
string
ExtractFileName(const string &fileName);

/// Extracts the only extension, e.g. "aaaa/bbb.dbg" -> "dbg"
string
ExtractExtension(const string &fileName);

/**
  Skip all the rows in a text file starting from the current character,
  when the current character is a 'mark'. All the following lines starting
  with the 'mark' character are also skipped.
*/
void
EatComments(istream &inpf, const char mark);

/// Skip all the whitespaces in the input stream. 
/**
   Whitespaces are the characters ' ','\t','\n','\f','\r','\b' .
*/
void
EatWhitespace(istream &inpf);


// Find the first line from now on that starts with a given keyword.
/**
   Read the keyword and do not read other characters
   Returns 'false' on success (line found), 'true' if not found.
*/
bool
FindLineWithKeyword(istream &inpf, const string &keyword);

/// Converts all the possible characters to lowercase.
void
ToLowerCase(string &str);

__END_GOLEM_HEADER

#endif // __BASSTR_H__

