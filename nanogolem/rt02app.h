// ===================================================================
//
// rt02app.h
//     Header file for the project - BRDF sampling
//
// Class: CRT_02_App
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, 2015.

#ifndef __RT02APP_H__
#define __RT02APP_H__

// Standard C++ headers
#include <fstream>

// nanoGOLEM headers
#include "configh.h"
#include "rt01app.h"

__BEGIN_GOLEM_HEADER

class CRT_02_App:
  public CRT_01_App
{
protected:
  SVAR statsSamplingBRDFtrack;
  // function to return the color for primary rays to be passed to
  // frame sampling scheme .. it is used by CColorMapper in the camera.
  virtual void ComputeColor(const CRay &ray, CHitPointInfo &info,
			    CColor &color);
public:
  // constructor
  CRT_02_App(): CRT_01_App() { }
  // destructor
  virtual ~CRT_02_App() { }
  virtual bool Init(CWorld *world, string &outputName);
  
  // deletes all the auxiliary data structures
  virtual bool CleanUp() {
    CRT_01_App::CleanUp();
    return false;
  }
};

__END_GOLEM_HEADER

#endif // __RT02APP_H__

