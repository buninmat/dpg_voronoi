// ===================================================================
//
// basrnd.cpp
//     Helper routines for random generator
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, 1999.

// nanoGOLEM headers
#include "configg.h"
#include "basrnd.h"
#include "errorg.h"

#ifdef __USE_MERSENNE_TWISTER
#include "mtwister.h"
#endif // __USE_MERSENNE_TWISTER

__BEGIN_GOLEM_SOURCE

// ----------------------------------------------------------------

// Random generator used preferrably
//static
//CMultDimRNDGen_MT1993ar MersenneTwisterGOLEM;
static MTRand MersenneTwisterGOLEM;

void
MersenneTwisterSetSeed(int newSeed)
{
//  MersenneTwisterGOLEM.SetSeed(newSeed);
  MersenneTwisterGOLEM.seed(newSeed);
}

double
MersenneTwisterRand()
{
  const float epsilon = 1e-6f;

  float value = (float)MersenneTwisterGOLEM.randExc();
  if (value < 0.0) {
    value = 0.0;
    //FATAL << "MersenneTwister bad down" << endl;
  }
  if (value >= 1.0) {
    value = 1.0f - epsilon;
    //FATAL << "MersenneTwister bad up" << endl;
  }
  return value;
}

__END_GOLEM_SOURCE

