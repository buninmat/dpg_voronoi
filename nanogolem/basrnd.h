// ================================================================
//
// basrnd.h
//     Basic operations for random generator.
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, 2004.

#ifndef __BASRND_H__
#define __BASRND_H__

// nanoGOLEM headers
#include "configh.h"
#include "basmath.h"

// Standard headers
#include <cstdlib>

__BEGIN_GOLEM_HEADER

// forward declarations

/// Returns a random floating-point value between the two values. 
/**
  Range is inclusive; the function should occasionally return a, 
  but never b. The range is [a; b).
*/
inline float
RandomValue(float a, float b)
{
  float range = Abs(a - b);

// use Mersenne Twister random number generator
#define __USE_MERSENNE_TWISTER

#ifdef __USE_MERSENNE_TWISTER
  // preferred way
  extern double MersenneTwisterRand();
  return float(MersenneTwisterRand()) * range + ((a < b) ? a : b);
#else
#ifdef __UNIX__
  // traditional ANSI generator - the worst case
  return drand48() * range + ((a < b) ? a : b);
#else // __UNIX__
  const double MULT = 1.0 / (RAND_MAX+1);
  return ((float)rand() * MULT) * range + ((a < b) ? a : b);
#endif // __UNIX__

#endif // __USE_MERSENNE_TWISTER
}

/// Returns random value in the range [0; 1)
inline float
RandomValue()
{
#ifdef __USE_MERSENNE_TWISTER
  // preferred way
  extern double MersenneTwisterRand();
  return float(MersenneTwisterRand());
#else
#ifdef __UNIX__
  // traditional ANSI generator - the worst case  
  return drand48();
#else // __UNIX__
  const double MULT = 1.0 / (RAND_MAX + 1);
  return rand() * MULT;
#endif // __UNIX__
#endif // __USE_MERSENNE_TWISTER
}


/// Sets the random seed for all random generators.
inline void
SetRandomSeed(int seed = 0)
{
#ifdef __USE_MERSENNE_TWISTER
  // preferred way
  extern void MersenneTwisterSetSeed(int);
  MersenneTwisterSetSeed(seed);
#endif // #__USE_MERSENNE_TWISTER

  srand(seed);
#ifdef __UNIX__
  srand48(seed);
#endif // __UNIX__
}

__END_GOLEM_HEADER

#endif // __BASRND_H__
