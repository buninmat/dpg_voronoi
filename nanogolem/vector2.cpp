// ===================================================================
//
// vector2.cpp
//     CVector2D class implements an 2D vector
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran

// nanoGOLEM headers
#include "configg.h"
#include "vector2.h"

__BEGIN_GOLEM_SOURCE

// Precompute which.  which is 0 if the normal is dominant
// in the X direction, 1 if the Y direction.
int
CVector2D::DominantAxis()
{
  return (fabs(x()) > fabs(y())) ?  0 : 1;
}

void
CVector2D::ExtractVerts(float *p, int dominant) const
{
  if (dominant == 0)
    *p = y();
  else
    *p = x();
}

float
CVector2D::Normalize()
{
  float s = Size();

  if (s != 0.0) {
    xx /= s;
    yy /= s;
  }
  return s;
}

int
CVector2D::Equal(const CVector2D &u, float trash) const
{
 return ( (fabs(x() - u.x()) < trash) && (fabs(y() - u.y()) < trash) ); 
}


// get the angle between two vectors in range 0 - PI

float
CVector2D::Angle(const CVector2D &v) const
{
  float cosine;
  cosine = float(DotProd(*this, v)) / (Size() * v.Size());
  return acos(cosine);
}

float
CVector2D::Cosine(const CVector2D &v) const
{
  return float(DotProd(*this, v))  /  (Size() * v.Size());
}

// cosine assuming that this vector is normalized
float
CVector2D::CosineN(const CVector2D &v) const
{
  return float(DotProd(*this, v)) / v.Size();
}

// if a given vector is smaller in one of coordinates than this, update
CVector2D&
CVector2D::UpdateMin(const CVector2D &v)
{
  if (x() > v.x())
    SetX(v.x());
  if (y() > v.y())
    SetY(v.y());
  return *this;
}

CVector2D&
CVector2D::UpdateMax(const CVector2D &v)
{
  if ( x() < v.x() )
    SetX(v.x());
  if ( y() < v.y() )
    SetY(v.y());
  return *this;
}

__END_GOLEM_SOURCE

