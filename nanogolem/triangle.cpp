// ===================================================================
//
// triangle.cpp
//     Source file for Triangle class in nanoGOLEM.
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, 2000

// nanoGOLEM headers
#include "configg.h"
#include "ray.h"
#include "triangle.h"

__BEGIN_GOLEM_SOURCE

void
CObject3D::SetCountID(int newCnt)
{
  CObject3D::countID = newCnt;
}

int
CObject3D::countID = 0;

bool
CObject3D::twoSidedPlanars = false;

// Computes a ray-triangle intersection
bool
CTriangle::NearestInt(CRay &ray, CHitPointInfo &info) const
{
  CVector3D e1 = vertices[1] - vertices[0];
  CVector3D e2 = vertices[2] - vertices[0];
  CVector3D p = CrossProd(ray.GetDir(), e2);
  float a = DotProd(e1, p);

  if (fabs(a) < CLimits::Small)
    return false; // perpendicular, not intersected

  float f = 1.0f / a;
  if ((twoSidedPlanars) && (f < 0.f)) {
    f = -f; // both sides of a triangle are visible
  }
  CVector3D s = ray.GetLoc() - vertices[0];
  CVector3D q = CrossProd(s, e1);
  float t = f * DotProd(e2, q);
  if (t < CLimits::Threshold) // ray goes away from triangle
    return false; // not intersected

  const float eps = 3e-5f;

  // Using barycentric coordinates with respect to vertex[0]
  float alpha = f * DotProd(s, p);
  if ((alpha < -eps) ||
      (alpha > 1.0f + eps))
    return false; // not intersected

  float beta = f * DotProd(ray.GetDir(), q);

  if ((beta < -eps) ||
      (alpha + beta > 1.0f + eps))
    return false; // not intersected

  if (t < info.GetMaxT()) {
    // Remember the closest intersection
    info.SetT(t);
    info.SetObject(this);
    info.SetMaxT(t);
    info.alpha = alpha;
    info.beta = beta;
    return true;	// intersected
  }
  return false; // not intersected
}

CTriangleNorm::CTriangleNorm(const CVector3D newVertices[3], const CVector3D newNormals[3]) :
  CTriangle(newVertices)
{
  normals[0] = newNormals[0];
  normals[1] = newNormals[1];
  normals[2] = newNormals[2];
}

CTriangleNorm::CTriangleNorm(const CVector3D newVertices[3], const CVector3D &newNormal) :
  CTriangle(newVertices)
{
  normals[0] = newNormal;
  normals[1] = newNormal;
  normals[2] = newNormal;
}

// -----------------------------------------------------------------
// Triangle with normals specified at vertices
void
CTriangleNorm::FindNormal(CHitPointInfo &info) const
{
  CVector3D N = normals[0] * (1.0f - info.alpha - info.beta) +
    normals[1] * info.alpha + normals[2] * info.beta;
  N.Normalize();
  info.SetNormal(N);
}

// -----------------------------------------------------------------
// Triangle with interpolated UV
CTriangleInt::CTriangleInt(const CVector3D newVertices[3], const CVector3D newNormals[3],
			   const CVector2D newUV[3]) :
  CTriangleNorm(newVertices, newNormals)
{
  for (int i = 0; i < 3; i++) { vectorU[i].Set(0.f); vectorU[i].Set(0.f); }
  uv[0] = newUV[0];
  uv[1] = newUV[1];
  uv[2] = newUV[2];
}

void
CTriangleInt::GetUV(CHitPointInfo &info) const
{
  CVector2D uvI = uv[0] * (1.0f - info.alpha - info.beta) +
    uv[1] * info.alpha + uv[2] * info.beta;
  info.SetUV(uvI.xx, uvI.yy);
}

void
CTriangleInt::GetUVvectors(CHitPointInfo &info) const
{
  FindNormal(info);
  GetUV(info);
  CVector3D U = vectorU[0] * (1.0f - info.alpha - info.beta) +
    vectorU[1] * info.alpha + vectorU[2] * info.beta;
  U.Normalize();
  CVector3D V = vectorV[0] * (1.0f - info.alpha - info.beta) +
    vectorV[1] * info.alpha + vectorV[2] * info.beta;
  V.Normalize();
  info.SetDifferentials(U, V);
}

__END_GOLEM_SOURCE

