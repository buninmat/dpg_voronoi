// ================================================================
//
// configg.h
//      Global configuration things for implementation files.
//
// This file must be as first included into each sourcefile.cpp !!!!!!!!!
//
// Licence: The use of this C++ file is allowed only for the subject
// "Data structures for computer graphics", lectured by Vlastimil Havran,
// at Czech Technical University in Prague. Any other usage and distribution
// to any 3rd party, including its modified versions, requires written signed
// permission from Vlastimil Havran (havran_AT_fel.cvut.cz), see file doc/Licence.txt
//
// Initial coding by Vlastimil Havran, 2000.

#ifndef __CONFIGG_H__
#define __CONFIGG_H__

// This file is intended for global definitions across the whole library,
// which influences the compilation a great deal.
#include <iostream>
#include <string.h>
using namespace std;

#ifndef USE_GOLEM_NAMESPACE
// without namespaces
#define  __BEGIN_GOLEM_SOURCE
#define  __END_GOLEM_SOURCE
#else
// with namespace 
#define NAMESPACEDEF     GOLEMLIB
// use of namespace commands inside the library sources
#define  __BEGIN_GOLEM_SOURCE namespace NAMESPACEDEF { using namespace NAMESPACEDEF;
#define  __END_GOLEM_SOURCE   }
#endif

#endif // __CONFIGG_H__

